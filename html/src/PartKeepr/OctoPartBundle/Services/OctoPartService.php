<?php

namespace PartKeepr\OctoPartBundle\Services;

use Guzzle\Http\Client;
use PartKeepr\OctoPartBundle\Response\OctoPartResponse;
use Symfony\Component\DomCrawler\Crawler;

class OctoPartService
{
    const OCTOPART_ENDPOINT = "http://octopart.com/api/v3/";

    private $apiKey;

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function getPartByUID($uid)
    {
        $debug = false;
        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_URL => 'https://store.comet.bg/en/Catalogue/Product/' . $uid . '/',
            CURLOPT_POST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => 1,
        ]);

        $response = curl_exec($ch);

        $crawler = new Crawler();
        $crawler->addHtmlContent($response);

        $infoDiv = trim($crawler->filter('.info')->html());

        $crawlerInfo = new Crawler();
        $crawlerInfo->addHtmlContent($infoDiv);

        $divs = $crawlerInfo->filter('div');

        foreach ($divs as $div) {
            $infoVars[] = trim($div->nodeValue);
        }

        foreach ($infoVars as $info) {
            if (preg_match("/Product name Comet\:(.*?)$/", $info, $result)) {
                $partInfo['cometId'] = trim($result[1]);
            }
            if (preg_match("/Manufacturer name\:(.*?)$/", $info, $result)) {
                $partInfo['mpn'] = trim($result[1]);
            }
            if (preg_match("/Manufacturer\:(.*?) \((.*?)\)$/", $info, $result)) {
                $partInfo['manufacturer'] = trim($result[1]);
            }
            if (preg_match("/Description\:(.*?)$/", $info, $result)) {
                $partInfo['name'] = trim($result[1]);
            }
        }

        $partInfo['url'] = 'https://store.comet.bg/en/Catalogue/Product/' . $uid . '/';

        $specs = [];

        // Get Specs
        $specsTable = $crawler->filter('.specs_list')->html();

        $tableCrawler = new Crawler();
        $tableCrawler->addHtmlContent($specsTable);

        $specsTr = $tableCrawler->filter('tr');
        foreach ($specsTr as $tr) {
            $value = trim($tr->nodeValue);
            if ($value == 'Specification') {
                continue;
            }

            $value = str_replace("\t","",$value);

            $explode = explode("\n", $value);
            $specs[$explode[0]] = $explode[1];
        }

        // Get Image

        $image = $crawler->filter('.product_gallery img')->getNode(0)->getAttributeNode('src')->value;

        // Get Datasheets
        $datasheets = [];
        $datasheetsEl = $crawler->filter('td.pdf a');
        if (count($datasheetsEl)) {
            for($j=0; $j!=$datasheetsEl; $j++) {
                $datasheets[] = $datasheetsEl->getNode($j)->getAttributeNode('href')->value;
            }
        }

        $partInfo = $this->_setDummyData($partInfo);

        $octopartResponse = new OctoPartResponse();
        $octopartResponse = $octopartResponse->getResponse($uid, $partInfo['cometId'], $partInfo['url'], $partInfo['name'], $partInfo['manufacturer'], $specs, $image, $datasheets);

        if ($debug) {
            dump($partInfo);
            dump($octopartResponse);
            die();
        }
        return $octopartResponse;
    }

    public function getPartyByQuery($query, $start = 0)
    {
        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_URL => 'https://store.comet.bg/ajax/get_result_counts.php',
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => 'language=bg&keywords='.htmlentities($query).'&manufacturer_id=0&package=&page=&sort=&items_per_page=100&currency=2&product=&quantity=&delivery_time=&more_info=&country=&name=&email=&page=&sort=&sync_listing=1&log_stats=1',
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => 1,
        ]);

        $response = curl_exec($ch);

        $response = json_decode($response, true);
        $content = $mainContent = $response['listing']['content'];

        $crawler = new Crawler();
        $crawler->addHtmlContent($content);

        $aMain = $crawler->filter('a');

        $ids = [];
        foreach ($aMain as $a) {
            $href = $a->getAttribute('href');
            if (preg_match("/\/Catalogue\/Product\/(.*?)\//", $href, $result)) {
                if (is_numeric($result[1]) && !in_array($result[1], $ids)) {
                    $ids[] = (int) $result[1];
                }
            }
        }

        $productN = 0;
        $rows = array();
        $tr_elements = $crawler->filter('table tr');
        // iterate over filter results
        foreach ($tr_elements as $i => $content) {
            $tds = array();
            $tdsRaw = array();
            // create crawler instance for result
            $crawler = new Crawler($content);
            $td_elements = $crawler->filter('td');
            //iterate again
            foreach ($td_elements as $i => $node) {
                // extract the value
                $tds[] = $node->nodeValue;
                $tdsRaw[] = $node;
            }

            if (!isset($pos['cometId'])) {
                foreach ($tds as $i => $td) {
                    $td = str_replace("\n", "",trim($td));
                    if ($td == 'Наименование Комет') {
                        $pos['cometId'] = $i;
                    } elseif ($td == 'Наименование производител') {
                        $pos['id'] = $i;
                    } elseif ($td == 'Описание') {
                        $pos['name'] = $i;
                    } elseif ($td == 'Производител') {
                        $pos['manufacturer'] = $i;
                    } elseif ($td == 'НаличностПоказвай само налични') {
                        $pos['qty_first'] = $i;
                    }
                }
                continue;
            }

            $rowInfo['id'] = trim($tds[$pos['cometId']]);
            $rowInfo['id'] = trim($tds[$pos['id']]);
            $rowInfo['name'] = trim($tds[$pos['name']]);
            $rowInfo['manufacturer'] = trim($tds[$pos['manufacturer']]);
            $rowInfo['qty'] = (int) $tds[$pos['qty_first']] + (int) $tds[$pos['qty_first'] + 1] + (int) $tds[$pos['qty_first'] + 2];

            $rowInfo['uid'] = $ids[$productN];
            $rowInfo['url'] = "https://store.comet.bg/Catalogue/Product/{$rowInfo['uid']}/";

            if ($rowInfo['cometId'] == "" || $rowInfo['cometId'] == 'София (магазин)' || $rowInfo['manufacturer'] == 'manufacturer') {
                continue;
            }

            $rowInfo = $this->_setDummyData($rowInfo);

            $rows[] = $rowInfo;
            $productN++;
        }

        if (count($rows) == 0) {
            return [];
        }

        return $rows;
    }

    private function _setDummyData($rowInfo)
    {
        if (empty($rowInfo['id'])) {
            $rowInfo['id'] = $rowInfo['cometId'];
        }

        if (empty($rowInfo['mpn'])) {
            $rowInfo['mpn'] = $rowInfo['cometId'];
        }

        if (empty($rowInfo['name'])) {
            $rowInfo['name'] = '';
        }

        if (empty($rowInfo['manufacturer'])) {
            $rowInfo['manufacturer'] = 'Unknown';
        }

        return $rowInfo;
    }
}
