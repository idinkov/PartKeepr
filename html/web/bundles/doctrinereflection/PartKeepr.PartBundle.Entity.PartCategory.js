Ext.define('PartKeepr.PartBundle.Entity.PartCategory', {
    extend: 'PartKeepr.data.HydraTreeModel',
    alias: 'schema.PartKeepr.PartBundle.Entity.PartCategory',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'lft', type: 'int', validators: []},
                { name: 'rgt', type: 'int', validators: []},
                { name: 'lvl', type: 'int', validators: []},
                { name: 'root', type: 'int', allowNull: true, validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'categoryPath', type: 'string', allowNull: true, validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/part_categories'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.PartBundle.Entity.PartCategory', '');
