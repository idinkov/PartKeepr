Ext.define('PartKeepr.AuthBundle.Entity.UserProvider', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.AuthBundle.Entity.UserProvider',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'type', type: 'string', validators: []},
                { name: 'editable', type: 'boolean', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/user_providers'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.AuthBundle.Entity.UserProvider', '');
