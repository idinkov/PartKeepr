Ext.define('PartKeepr.SiPrefixBundle.Entity.SiPrefix', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.SiPrefixBundle.Entity.SiPrefix',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'prefix', type: 'string', validators: [{"type":"presence","message":"siprefix.prefix.not_blank"}]},
                { name: 'symbol', type: 'string', validators: []},
                { name: 'exponent', type: 'int', validators: []},
                { name: 'base', type: 'int', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/si_prefixes'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.SiPrefixBundle.Entity.SiPrefix', '');
