Ext.define('PartKeepr.StockBundle.Entity.StockEntry', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.StockBundle.Entity.StockEntry',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'stockLevel', type: 'int', validators: []},
                { name: 'price', type: 'number', allowNull: true, validators: []},
                { name: 'dateTime', type: 'date', validators: []},
                { name: 'correction', type: 'boolean', validators: []},
                { name: 'comment', type: 'string', allowNull: true, validators: []}
                            ,
                            { name: 'part',
                reference: 'PartKeepr.PartBundle.Entity.Part',
            allowBlank: true                        },
                            { name: 'user',
                reference: 'PartKeepr.AuthBundle.Entity.User',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/stock_entries'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.StockBundle.Entity.StockEntry', '');
