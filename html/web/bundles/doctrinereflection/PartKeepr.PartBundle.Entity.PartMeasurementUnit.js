Ext.define('PartKeepr.PartBundle.Entity.PartMeasurementUnit', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.PartBundle.Entity.PartMeasurementUnit',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: [{"type":"presence","message":"partMeasurementUnit.name.not_blank"}]},
                { name: 'shortName', type: 'string', validators: [{"type":"presence","message":"partMeasurementUnit.shortName.not_blank"}]},
                { name: 'default', type: 'boolean', validators: []}
                        
    ],

        hasMany: [
            {
        name: 'parts',
        associationKey: 'parts',
        model: 'PartKeepr.PartBundle.Entity.Part'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/part_measurement_units'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.PartBundle.Entity.PartMeasurementUnit', '');
