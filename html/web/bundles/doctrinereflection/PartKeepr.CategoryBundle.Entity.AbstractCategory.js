Ext.define('PartKeepr.CategoryBundle.Entity.AbstractCategory', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.CategoryBundle.Entity.AbstractCategory',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'lft', type: 'int', validators: []},
                { name: 'rgt', type: 'int', validators: []},
                { name: 'lvl', type: 'int', validators: []},
                { name: 'root', type: 'int', allowNull: true, validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.CategoryBundle.Entity.AbstractCategory'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.CategoryBundle.Entity.AbstractCategory', '');
