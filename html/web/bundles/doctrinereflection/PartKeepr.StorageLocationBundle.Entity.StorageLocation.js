Ext.define('PartKeepr.StorageLocationBundle.Entity.StorageLocation', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.StorageLocationBundle.Entity.StorageLocation',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []}
                            ,
                            { name: 'category',
                reference: 'PartKeepr.StorageLocationBundle.Entity.StorageLocationCategory',
            allowBlank: true                        }
                                        ,
                            { name: 'image',
                reference: 'PartKeepr.StorageLocationBundle.Entity.StorageLocationImage'
                }
                    
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/storage_locations'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.StorageLocationBundle.Entity.StorageLocation', '');
