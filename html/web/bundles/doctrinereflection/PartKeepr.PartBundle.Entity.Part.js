Ext.define('PartKeepr.PartBundle.Entity.Part', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.PartBundle.Entity.Part',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: [{"type":"presence","message":"This value should not be blank."}]},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'comment', type: 'string', validators: []},
                { name: 'stockLevel', type: 'int', persist: false, validators: []},
                { name: 'minStockLevel', type: 'int', validators: []},
                { name: 'averagePrice', type: 'number', persist: false, validators: []},
                { name: 'status', type: 'string', allowNull: true, validators: []},
                { name: 'needsReview', type: 'boolean', validators: []},
                { name: 'partCondition', type: 'string', allowNull: true, validators: []},
                { name: 'productionRemarks', type: 'string', allowNull: true, validators: []},
                { name: 'createDate', type: 'date', allowNull: true, persist: false, validators: []},
                { name: 'internalPartNumber', type: 'string', allowNull: true, validators: []},
                { name: 'removals', type: 'boolean', persist: false, validators: []},
                { name: 'lowStock', type: 'boolean', persist: false, validators: []},
                { name: 'metaPart', type: 'boolean', validators: []}
                            ,
                            { name: 'category',
                reference: 'PartKeepr.PartBundle.Entity.PartCategory',
            allowBlank: false                        },
                            { name: 'footprint',
                reference: 'PartKeepr.FootprintBundle.Entity.Footprint',
            allowBlank: true                        },
                            { name: 'partUnit',
                reference: 'PartKeepr.PartBundle.Entity.PartMeasurementUnit',
            allowBlank: true                        },
                            { name: 'storageLocation',
                reference: 'PartKeepr.StorageLocationBundle.Entity.StorageLocation',
            allowBlank: true                        }
                            
    ],

        hasMany: [
            {
        name: 'manufacturers',
        associationKey: 'manufacturers',
        model: 'PartKeepr.PartBundle.Entity.PartManufacturer'
        },
            {
        name: 'distributors',
        associationKey: 'distributors',
        model: 'PartKeepr.PartBundle.Entity.PartDistributor'
        },
            {
        name: 'attachments',
        associationKey: 'attachments',
        model: 'PartKeepr.PartBundle.Entity.PartAttachment'
        },
            {
        name: 'stockLevels',
        associationKey: 'stockLevels',
        model: 'PartKeepr.StockBundle.Entity.StockEntry'
        },
            {
        name: 'parameters',
        associationKey: 'parameters',
        model: 'PartKeepr.PartBundle.Entity.PartParameter'
        },
            {
        name: 'metaPartParameterCriterias',
        associationKey: 'metaPartParameterCriterias',
        model: 'PartKeepr.PartBundle.Entity.MetaPartParameterCriteria'
        },
            {
        name: 'projectParts',
        associationKey: 'projectParts',
        model: 'PartKeepr.ProjectBundle.Entity.ProjectPart'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/parts'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.PartBundle.Entity.Part', '');
