Ext.define('PartKeepr.TipOfTheDayBundle.Entity.TipOfTheDayHistory', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.TipOfTheDayBundle.Entity.TipOfTheDayHistory',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []}
                            ,
                            { name: 'user',
                reference: 'PartKeepr.AuthBundle.Entity.User',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/tip_of_the_day_histories'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.TipOfTheDayBundle.Entity.TipOfTheDayHistory', '');
