Ext.define('PartKeepr.FootprintBundle.Entity.FootprintCategory', {
    extend: 'PartKeepr.data.HydraTreeModel',
    alias: 'schema.PartKeepr.FootprintBundle.Entity.FootprintCategory',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'lft', type: 'int', validators: []},
                { name: 'rgt', type: 'int', validators: []},
                { name: 'lvl', type: 'int', validators: []},
                { name: 'root', type: 'int', allowNull: true, validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'categoryPath', type: 'string', allowNull: true, validators: []}
                        
    ],

        hasMany: [
            {
        name: 'footprints',
        associationKey: 'footprints',
        model: 'PartKeepr.FootprintBundle.Entity.Footprint'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/footprint_categories'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.FootprintBundle.Entity.FootprintCategory', '');
