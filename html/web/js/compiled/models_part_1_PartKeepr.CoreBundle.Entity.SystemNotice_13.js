Ext.define('PartKeepr.CoreBundle.Entity.SystemNotice', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.CoreBundle.Entity.SystemNotice',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'date', type: 'date', validators: []},
                { name: 'title', type: 'string', validators: []},
                { name: 'description', type: 'string', validators: []},
                { name: 'acknowledged', type: 'boolean', validators: []},
                { name: 'type', type: 'string', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/system_notices'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.CoreBundle.Entity.SystemNotice', '');
