Ext.define('PartKeepr.ProjectBundle.Entity.ProjectAttachment', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ProjectBundle.Entity.ProjectAttachment',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'type', type: 'string', validators: []},
                { name: 'filename', type: 'string', validators: []},
                { name: 'originalFilename', type: 'string', allowNull: true, validators: []},
                { name: 'mimetype', type: 'string', validators: []},
                { name: 'size', type: 'int', validators: []},
                { name: 'extension', type: 'string', allowNull: true, validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'created', type: 'date', validators: []}
                            ,
                            { name: 'project',
                reference: 'PartKeepr.ProjectBundle.Entity.Project',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/project_attachments'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ProjectBundle.Entity.ProjectAttachment', '');
