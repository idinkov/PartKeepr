Ext.define('FOS.UserBundle.Model.Group', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.FOS.UserBundle.Model.Group',

    idProperty: "@id",
    fields: [
                { name: 'name', type: 'string', validators: []},
                { name: 'roles', type: 'array', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:FOS.UserBundle.Model.Group'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('FOS.UserBundle.Model.Group', '');
