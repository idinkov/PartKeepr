Ext.define('PartKeepr.StorageLocationBundle.Entity.StorageLocationCategory', {
    extend: 'PartKeepr.data.HydraTreeModel',
    alias: 'schema.PartKeepr.StorageLocationBundle.Entity.StorageLocationCategory',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'lft', type: 'int', validators: []},
                { name: 'rgt', type: 'int', validators: []},
                { name: 'lvl', type: 'int', validators: []},
                { name: 'root', type: 'int', allowNull: true, validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'categoryPath', type: 'string', allowNull: true, validators: []}
                        
    ],

        hasMany: [
            {
        name: 'storageLocations',
        associationKey: 'storageLocations',
        model: 'PartKeepr.StorageLocationBundle.Entity.StorageLocation'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/storage_location_categories'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.StorageLocationBundle.Entity.StorageLocationCategory', '');
