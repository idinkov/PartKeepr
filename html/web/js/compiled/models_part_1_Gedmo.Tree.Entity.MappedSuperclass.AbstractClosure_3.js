Ext.define('Gedmo.Tree.Entity.MappedSuperclass.AbstractClosure', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.Gedmo.Tree.Entity.MappedSuperclass.AbstractClosure',

    idProperty: "@id",
    fields: [
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:Gedmo.Tree.Entity.MappedSuperclass.AbstractClosure'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('Gedmo.Tree.Entity.MappedSuperclass.AbstractClosure', '');
