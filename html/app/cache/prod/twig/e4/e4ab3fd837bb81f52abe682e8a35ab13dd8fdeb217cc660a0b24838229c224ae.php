<?php

/* PartKeeprSetupBundle::authkey.php.twig */
class __TwigTemplate_ed1cb118dddbb62a2c6604a598b0f35a33fcddd89d10fb20363fb46d684cc9e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<?php
/**
 * Your auth key is: ";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["authkey"]) ? $context["authkey"] : null), "html", null, true);
        echo "
 *
 * Copy and paste the auth key in order to proceed with setup
 */
";
    }

    public function getTemplateName()
    {
        return "PartKeeprSetupBundle::authkey.php.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 3,  19 => 1,);
    }
}
/* <?php*/
/* /***/
/*  * Your auth key is: {{ authkey }}*/
/*  **/
/*  * Copy and paste the auth key in order to proceed with setup*/
/*  *//* */
/* */
