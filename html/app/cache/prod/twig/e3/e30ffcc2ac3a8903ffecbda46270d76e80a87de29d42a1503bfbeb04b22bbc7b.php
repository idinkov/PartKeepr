<?php

/* PartKeeprSetupBundle::parameters.php.twig */
class __TwigTemplate_55b3c4fd5840d0dc908f958d8c4a83ba4a38d5fd83f29ce0d498c8228e4230c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<?php
";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["parameters"]) ? $context["parameters"] : null));
        foreach ($context['_seq'] as $context["name"] => $context["value"]) {
            // line 3
            echo "\$container->setParameter('";
            echo twig_escape_filter($this->env, $context["name"], "html", null, true);
            echo "', ";
            echo $context["value"];
            echo ");
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['name'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "PartKeeprSetupBundle::parameters.php.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 3,  22 => 2,  19 => 1,);
    }
}
/* <?php*/
/* {% for name,value in parameters %}*/
/* $container->setParameter('{{ name }}', {{ value|raw }});*/
/* {% endfor %}*/
/* */
