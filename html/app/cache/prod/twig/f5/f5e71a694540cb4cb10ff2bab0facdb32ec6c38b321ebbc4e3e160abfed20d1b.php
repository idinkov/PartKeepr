<?php

/* PartKeeprFrontendBundle::maintenance.html.twig */
class __TwigTemplate_f837396027b56908d342de854e0b9110717fdbd6c461e188403b036dc13ccc4e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"
        \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">
<html>
<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>
    <title>";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["maintenanceTitle"]) ? $context["maintenanceTitle"] : null), "html", null, true);
        echo "</title>
</head>
<body>
    <h1>";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["maintenanceTitle"]) ? $context["maintenanceTitle"] : null), "html", null, true);
        echo "</h1>

    <p>";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["maintenanceMessage"]) ? $context["maintenanceMessage"] : null), "html", null, true);
        echo "</p>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "PartKeeprFrontendBundle::maintenance.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 11,  32 => 9,  26 => 6,  19 => 1,);
    }
}
/* <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"*/
/*         "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">*/
/* <html>*/
/* <head>*/
/*     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>*/
/*     <title>{{ maintenanceTitle }}</title>*/
/* </head>*/
/* <body>*/
/*     <h1>{{ maintenanceTitle }}</h1>*/
/* */
/*     <p>{{ maintenanceMessage }}</p>*/
/* </body>*/
/* </html>*/
/* */
