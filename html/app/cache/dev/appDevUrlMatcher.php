<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/css/ec39aa1')) {
            // _assetic_ec39aa1
            if ($pathinfo === '/css/ec39aa1.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'ec39aa1',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_ec39aa1',);
            }

            if (0 === strpos($pathinfo, '/css/ec39aa1_')) {
                // _assetic_ec39aa1_0
                if ($pathinfo === '/css/ec39aa1_ux-all_1.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'ec39aa1',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_ec39aa1_0',);
                }

                // _assetic_ec39aa1_1
                if ($pathinfo === '/css/ec39aa1_charts-all_2.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'ec39aa1',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_ec39aa1_1',);
                }

                // _assetic_ec39aa1_2
                if ($pathinfo === '/css/ec39aa1_silk-icons-sprite_3.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'ec39aa1',  'pos' => 2,  '_format' => 'css',  '_route' => '_assetic_ec39aa1_2',);
                }

                // _assetic_ec39aa1_3
                if ($pathinfo === '/css/ec39aa1_fugue-16_4.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'ec39aa1',  'pos' => 3,  '_format' => 'css',  '_route' => '_assetic_ec39aa1_3',);
                }

                // _assetic_ec39aa1_4
                if ($pathinfo === '/css/ec39aa1_partkeepr_5.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'ec39aa1',  'pos' => 4,  '_format' => 'css',  '_route' => '_assetic_ec39aa1_4',);
                }

                // _assetic_ec39aa1_5
                if ($pathinfo === '/css/ec39aa1_PartKeepr_6.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'ec39aa1',  'pos' => 5,  '_format' => 'css',  '_route' => '_assetic_ec39aa1_5',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/images/245ec67')) {
            // _assetic_245ec67
            if ($pathinfo === '/images/245ec67.ico') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '245ec67',  'pos' => NULL,  '_format' => 'ico',  '_route' => '_assetic_245ec67',);
            }

            // _assetic_245ec67_0
            if ($pathinfo === '/images/245ec67_favicon_1.ico') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '245ec67',  'pos' => 0,  '_format' => 'ico',  '_route' => '_assetic_245ec67_0',);
            }

        }

        if (0 === strpos($pathinfo, '/js/compiled')) {
            if (0 === strpos($pathinfo, '/js/compiled/extjs')) {
                // _assetic_4c8b3ba
                if ($pathinfo === '/js/compiled/extjs.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '4c8b3ba',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_4c8b3ba',);
                }

                if (0 === strpos($pathinfo, '/js/compiled/extjs_')) {
                    // _assetic_4c8b3ba_0
                    if ($pathinfo === '/js/compiled/extjs_ext-all-debug_1.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '4c8b3ba',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_4c8b3ba_0',);
                    }

                    if (0 === strpos($pathinfo, '/js/compiled/extjs_T')) {
                        // _assetic_4c8b3ba_1
                        if ($pathinfo === '/js/compiled/extjs_TreePicker_2.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '4c8b3ba',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_4c8b3ba_1',);
                        }

                        // _assetic_4c8b3ba_2
                        if ($pathinfo === '/js/compiled/extjs_TabCloseMenu_3.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '4c8b3ba',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_4c8b3ba_2',);
                        }

                    }

                    // _assetic_4c8b3ba_3
                    if ($pathinfo === '/js/compiled/extjs_StatusBar_4.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '4c8b3ba',  'pos' => 3,  '_format' => 'js',  '_route' => '_assetic_4c8b3ba_3',);
                    }

                    // _assetic_4c8b3ba_4
                    if ($pathinfo === '/js/compiled/extjs_IFrame_5.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '4c8b3ba',  'pos' => 4,  '_format' => 'js',  '_route' => '_assetic_4c8b3ba_4',);
                    }

                }

                // _assetic_eddbaea
                if ($pathinfo === '/js/compiled/extjs.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'eddbaea',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_eddbaea',);
                }

                if (0 === strpos($pathinfo, '/js/compiled/extjs_')) {
                    // _assetic_eddbaea_0
                    if ($pathinfo === '/js/compiled/extjs_ext-all_1.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'eddbaea',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_eddbaea_0',);
                    }

                    if (0 === strpos($pathinfo, '/js/compiled/extjs_T')) {
                        // _assetic_eddbaea_1
                        if ($pathinfo === '/js/compiled/extjs_TreePicker_2.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => 'eddbaea',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_eddbaea_1',);
                        }

                        // _assetic_eddbaea_2
                        if ($pathinfo === '/js/compiled/extjs_TabCloseMenu_3.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => 'eddbaea',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_eddbaea_2',);
                        }

                    }

                    // _assetic_eddbaea_3
                    if ($pathinfo === '/js/compiled/extjs_StatusBar_4.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'eddbaea',  'pos' => 3,  '_format' => 'js',  '_route' => '_assetic_eddbaea_3',);
                    }

                    // _assetic_eddbaea_4
                    if ($pathinfo === '/js/compiled/extjs_IFrame_5.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'eddbaea',  'pos' => 4,  '_format' => 'js',  '_route' => '_assetic_eddbaea_4',);
                    }

                }

            }

            if (0 === strpos($pathinfo, '/js/compiled/m')) {
                if (0 === strpos($pathinfo, '/js/compiled/main')) {
                    // _assetic_a68c93c
                    if ($pathinfo === '/js/compiled/main.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'a68c93c',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_a68c93c',);
                    }

                    if (0 === strpos($pathinfo, '/js/compiled/main_')) {
                        // _assetic_a68c93c_0
                        if ($pathinfo === '/js/compiled/main_charts_1.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => 'a68c93c',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_a68c93c_0',);
                        }

                        // _assetic_a68c93c_1
                        if ($pathinfo === '/js/compiled/main_CallActions_2.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => 'a68c93c',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_a68c93c_1',);
                        }

                        // _assetic_a68c93c_2
                        if ($pathinfo === '/js/compiled/main_Array_3.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => 'a68c93c',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_a68c93c_2',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main_Hydra')) {
                            // _assetic_a68c93c_3
                            if ($pathinfo === '/js/compiled/main_HydraModel_4.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => 'a68c93c',  'pos' => 3,  '_format' => 'js',  '_route' => '_assetic_a68c93c_3',);
                            }

                            // _assetic_a68c93c_4
                            if ($pathinfo === '/js/compiled/main_HydraTreeModel_5.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => 'a68c93c',  'pos' => 4,  '_format' => 'js',  '_route' => '_assetic_a68c93c_4',);
                            }

                        }

                        // _assetic_a68c93c_5
                        if ($pathinfo === '/js/compiled/main_ModelStore_6.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => 'a68c93c',  'pos' => 5,  '_format' => 'js',  '_route' => '_assetic_a68c93c_5',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main_Ext.')) {
                            // _assetic_a68c93c_6
                            if ($pathinfo === '/js/compiled/main_Ext.form.field.Checkbox.EXTJS-21886_7.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => 'a68c93c',  'pos' => 6,  '_format' => 'js',  '_route' => '_assetic_a68c93c_6',);
                            }

                            // _assetic_a68c93c_7
                            if ($pathinfo === '/js/compiled/main_Ext.data.field.Date-ISO8601_8.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => 'a68c93c',  'pos' => 7,  '_format' => 'js',  '_route' => '_assetic_a68c93c_7',);
                            }

                        }

                    }

                }

                if (0 === strpos($pathinfo, '/js/compiled/models')) {
                    // _assetic_447d316
                    if ($pathinfo === '/js/compiled/models.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_447d316',);
                    }

                    if (0 === strpos($pathinfo, '/js/compiled/models_part_1_')) {
                        if (0 === strpos($pathinfo, '/js/compiled/models_part_1_FOS.UserBundle.Model.')) {
                            // _assetic_447d316_0
                            if ($pathinfo === '/js/compiled/models_part_1_FOS.UserBundle.Model.Group_1.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_447d316_0',);
                            }

                            // _assetic_447d316_1
                            if ($pathinfo === '/js/compiled/models_part_1_FOS.UserBundle.Model.User_2.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_447d316_1',);
                            }

                        }

                        // _assetic_447d316_2
                        if ($pathinfo === '/js/compiled/models_part_1_Gedmo.Tree.Entity.MappedSuperclass.AbstractClosure_3.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_447d316_2',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.')) {
                            if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.AuthBundle.Entity.')) {
                                // _assetic_447d316_3
                                if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.AuthBundle.Entity.FOSUser_4.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 3,  '_format' => 'js',  '_route' => '_assetic_447d316_3',);
                                }

                                if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.AuthBundle.Entity.User')) {
                                    // _assetic_447d316_4
                                    if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.AuthBundle.Entity.User_5.js') {
                                        return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 4,  '_format' => 'js',  '_route' => '_assetic_447d316_4',);
                                    }

                                    if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.AuthBundle.Entity.UserPr')) {
                                        // _assetic_447d316_5
                                        if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.AuthBundle.Entity.UserPreference_6.js') {
                                            return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 5,  '_format' => 'js',  '_route' => '_assetic_447d316_5',);
                                        }

                                        // _assetic_447d316_6
                                        if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.AuthBundle.Entity.UserProvider_7.js') {
                                            return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 6,  '_format' => 'js',  '_route' => '_assetic_447d316_6',);
                                        }

                                    }

                                }

                            }

                            if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.BatchJobBundle.Entity.BatchJob')) {
                                // _assetic_447d316_7
                                if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.BatchJobBundle.Entity.BatchJob_8.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 7,  '_format' => 'js',  '_route' => '_assetic_447d316_7',);
                                }

                                // _assetic_447d316_8
                                if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.BatchJobBundle.Entity.BatchJobQueryField_9.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 8,  '_format' => 'js',  '_route' => '_assetic_447d316_8',);
                                }

                                // _assetic_447d316_9
                                if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.BatchJobBundle.Entity.BatchJobUpdateField_10.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 9,  '_format' => 'js',  '_route' => '_assetic_447d316_9',);
                                }

                            }

                            if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.C')) {
                                // _assetic_447d316_10
                                if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.CategoryBundle.Entity.AbstractCategory_11.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 10,  '_format' => 'js',  '_route' => '_assetic_447d316_10',);
                                }

                                if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.CoreBundle.Entity.')) {
                                    // _assetic_447d316_11
                                    if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.CoreBundle.Entity.BaseEntity_12.js') {
                                        return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 11,  '_format' => 'js',  '_route' => '_assetic_447d316_11',);
                                    }

                                    // _assetic_447d316_12
                                    if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.CoreBundle.Entity.SystemNotice_13.js') {
                                        return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 12,  '_format' => 'js',  '_route' => '_assetic_447d316_12',);
                                    }

                                }

                                // _assetic_447d316_13
                                if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.CronLoggerBundle.Entity.CronLogger_14.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 13,  '_format' => 'js',  '_route' => '_assetic_447d316_13',);
                                }

                            }

                            // _assetic_447d316_14
                            if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.DistributorBundle.Entity.Distributor_15.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 14,  '_format' => 'js',  '_route' => '_assetic_447d316_14',);
                            }

                            if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.FootprintBundle.Entity.Footprint')) {
                                // _assetic_447d316_15
                                if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.FootprintBundle.Entity.Footprint_16.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 15,  '_format' => 'js',  '_route' => '_assetic_447d316_15',);
                                }

                                // _assetic_447d316_16
                                if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.FootprintBundle.Entity.FootprintAttachment_17.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 16,  '_format' => 'js',  '_route' => '_assetic_447d316_16',);
                                }

                                // _assetic_447d316_17
                                if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.FootprintBundle.Entity.FootprintCategory_18.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 17,  '_format' => 'js',  '_route' => '_assetic_447d316_17',);
                                }

                                // _assetic_447d316_18
                                if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.FootprintBundle.Entity.FootprintImage_19.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 18,  '_format' => 'js',  '_route' => '_assetic_447d316_18',);
                                }

                            }

                            if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.Im')) {
                                if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.ImageBundle.Entity.')) {
                                    // _assetic_447d316_19
                                    if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.ImageBundle.Entity.CachedImage_20.js') {
                                        return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 19,  '_format' => 'js',  '_route' => '_assetic_447d316_19',);
                                    }

                                    // _assetic_447d316_20
                                    if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.ImageBundle.Entity.Image_21.js') {
                                        return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 20,  '_format' => 'js',  '_route' => '_assetic_447d316_20',);
                                    }

                                    // _assetic_447d316_21
                                    if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.ImageBundle.Entity.TempImage_22.js') {
                                        return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 21,  '_format' => 'js',  '_route' => '_assetic_447d316_21',);
                                    }

                                }

                                // _assetic_447d316_22
                                if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.ImportBundle.Entity.ImportPreset_23.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 22,  '_format' => 'js',  '_route' => '_assetic_447d316_22',);
                                }

                            }

                            if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.ManufacturerBundle.Entity.Manufacturer')) {
                                // _assetic_447d316_23
                                if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.ManufacturerBundle.Entity.Manufacturer_24.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 23,  '_format' => 'js',  '_route' => '_assetic_447d316_23',);
                                }

                                // _assetic_447d316_24
                                if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.ManufacturerBundle.Entity.ManufacturerICLogo_25.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 24,  '_format' => 'js',  '_route' => '_assetic_447d316_24',);
                                }

                            }

                            if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.P')) {
                                if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.PartBundle.Entity.')) {
                                    // _assetic_447d316_25
                                    if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.PartBundle.Entity.MetaPartParameterCriteria_26.js') {
                                        return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 25,  '_format' => 'js',  '_route' => '_assetic_447d316_25',);
                                    }

                                    if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.PartBundle.Entity.Part')) {
                                        // _assetic_447d316_26
                                        if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.PartBundle.Entity.Part_27.js') {
                                            return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 26,  '_format' => 'js',  '_route' => '_assetic_447d316_26',);
                                        }

                                        // _assetic_447d316_27
                                        if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.PartBundle.Entity.PartAttachment_28.js') {
                                            return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 27,  '_format' => 'js',  '_route' => '_assetic_447d316_27',);
                                        }

                                        // _assetic_447d316_28
                                        if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.PartBundle.Entity.PartCategory_29.js') {
                                            return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 28,  '_format' => 'js',  '_route' => '_assetic_447d316_28',);
                                        }

                                        // _assetic_447d316_29
                                        if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.PartBundle.Entity.PartDistributor_30.js') {
                                            return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 29,  '_format' => 'js',  '_route' => '_assetic_447d316_29',);
                                        }

                                        // _assetic_447d316_30
                                        if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.PartBundle.Entity.PartImage_31.js') {
                                            return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 30,  '_format' => 'js',  '_route' => '_assetic_447d316_30',);
                                        }

                                        if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.PartBundle.Entity.PartM')) {
                                            // _assetic_447d316_31
                                            if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.PartBundle.Entity.PartManufacturer_32.js') {
                                                return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 31,  '_format' => 'js',  '_route' => '_assetic_447d316_31',);
                                            }

                                            // _assetic_447d316_32
                                            if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.PartBundle.Entity.PartMeasurementUnit_33.js') {
                                                return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 32,  '_format' => 'js',  '_route' => '_assetic_447d316_32',);
                                            }

                                        }

                                        // _assetic_447d316_33
                                        if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.PartBundle.Entity.PartParameter_34.js') {
                                            return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 33,  '_format' => 'js',  '_route' => '_assetic_447d316_33',);
                                        }

                                    }

                                }

                                if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.ProjectBundle.Entity.Project')) {
                                    // _assetic_447d316_34
                                    if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.ProjectBundle.Entity.Project_35.js') {
                                        return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 34,  '_format' => 'js',  '_route' => '_assetic_447d316_34',);
                                    }

                                    // _assetic_447d316_35
                                    if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.ProjectBundle.Entity.ProjectAttachment_36.js') {
                                        return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 35,  '_format' => 'js',  '_route' => '_assetic_447d316_35',);
                                    }

                                    // _assetic_447d316_36
                                    if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.ProjectBundle.Entity.ProjectPart_37.js') {
                                        return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 36,  '_format' => 'js',  '_route' => '_assetic_447d316_36',);
                                    }

                                    if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.ProjectBundle.Entity.ProjectRun')) {
                                        // _assetic_447d316_37
                                        if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.ProjectBundle.Entity.ProjectRun_38.js') {
                                            return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 37,  '_format' => 'js',  '_route' => '_assetic_447d316_37',);
                                        }

                                        // _assetic_447d316_38
                                        if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.ProjectBundle.Entity.ProjectRunPart_39.js') {
                                            return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 38,  '_format' => 'js',  '_route' => '_assetic_447d316_38',);
                                        }

                                    }

                                }

                            }

                            if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.S')) {
                                // _assetic_447d316_39
                                if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.SiPrefixBundle.Entity.SiPrefix_40.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 39,  '_format' => 'js',  '_route' => '_assetic_447d316_39',);
                                }

                                if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.St')) {
                                    if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.StatisticBundle.Entity.StatisticSnapshot')) {
                                        // _assetic_447d316_40
                                        if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.StatisticBundle.Entity.StatisticSnapshot_41.js') {
                                            return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 40,  '_format' => 'js',  '_route' => '_assetic_447d316_40',);
                                        }

                                        // _assetic_447d316_41
                                        if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.StatisticBundle.Entity.StatisticSnapshotUnit_42.js') {
                                            return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 41,  '_format' => 'js',  '_route' => '_assetic_447d316_41',);
                                        }

                                    }

                                    if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.Sto')) {
                                        // _assetic_447d316_42
                                        if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.StockBundle.Entity.StockEntry_43.js') {
                                            return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 42,  '_format' => 'js',  '_route' => '_assetic_447d316_42',);
                                        }

                                        if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.StorageLocationBundle.Entity.StorageLocation')) {
                                            // _assetic_447d316_43
                                            if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.StorageLocationBundle.Entity.StorageLocation_44.js') {
                                                return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 43,  '_format' => 'js',  '_route' => '_assetic_447d316_43',);
                                            }

                                            // _assetic_447d316_44
                                            if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.StorageLocationBundle.Entity.StorageLocationCategory_45.js') {
                                                return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 44,  '_format' => 'js',  '_route' => '_assetic_447d316_44',);
                                            }

                                            // _assetic_447d316_45
                                            if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.StorageLocationBundle.Entity.StorageLocationImage_46.js') {
                                                return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 45,  '_format' => 'js',  '_route' => '_assetic_447d316_45',);
                                            }

                                        }

                                    }

                                }

                                // _assetic_447d316_46
                                if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.SystemPreferenceBundle.Entity.SystemPreference_47.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 46,  '_format' => 'js',  '_route' => '_assetic_447d316_46',);
                                }

                            }

                            if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.TipOfTheDayBundle.Entity.TipOfTheDay')) {
                                // _assetic_447d316_47
                                if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.TipOfTheDayBundle.Entity.TipOfTheDay_48.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 47,  '_format' => 'js',  '_route' => '_assetic_447d316_47',);
                                }

                                // _assetic_447d316_48
                                if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.TipOfTheDayBundle.Entity.TipOfTheDayHistory_49.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 48,  '_format' => 'js',  '_route' => '_assetic_447d316_48',);
                                }

                            }

                            if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.U')) {
                                // _assetic_447d316_49
                                if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.UnitBundle.Entity.Unit_50.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 49,  '_format' => 'js',  '_route' => '_assetic_447d316_49',);
                                }

                                if (0 === strpos($pathinfo, '/js/compiled/models_part_1_PartKeepr.UploadedFileBundle.Entity.')) {
                                    // _assetic_447d316_50
                                    if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.UploadedFileBundle.Entity.TempUploadedFile_51.js') {
                                        return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 50,  '_format' => 'js',  '_route' => '_assetic_447d316_50',);
                                    }

                                    // _assetic_447d316_51
                                    if ($pathinfo === '/js/compiled/models_part_1_PartKeepr.UploadedFileBundle.Entity.UploadedFile_52.js') {
                                        return array (  '_controller' => 'assetic.controller:render',  'name' => '447d316',  'pos' => 51,  '_format' => 'js',  '_route' => '_assetic_447d316_51',);
                                    }

                                }

                            }

                        }

                    }

                }

                if (0 === strpos($pathinfo, '/js/compiled/main2')) {
                    // _assetic_2579ed4
                    if ($pathinfo === '/js/compiled/main2.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_2579ed4',);
                    }

                    if (0 === strpos($pathinfo, '/js/compiled/main2_')) {
                        // _assetic_2579ed4_0
                        if ($pathinfo === '/js/compiled/main2_i18n_1.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_2579ed4_0',);
                        }

                        // _assetic_2579ed4_1
                        if ($pathinfo === '/js/compiled/main2_CurrencyStore_2.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_2579ed4_1',);
                        }

                        // _assetic_2579ed4_2
                        if ($pathinfo === '/js/compiled/main2_ReflectionFieldTreeModel_3.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_2579ed4_2',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_Entity')) {
                            // _assetic_2579ed4_3
                            if ($pathinfo === '/js/compiled/main2_EntityQueryPanel_4.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 3,  '_format' => 'js',  '_route' => '_assetic_2579ed4_3',);
                            }

                            // _assetic_2579ed4_4
                            if ($pathinfo === '/js/compiled/main2_EntityPicker_5.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 4,  '_format' => 'js',  '_route' => '_assetic_2579ed4_4',);
                            }

                        }

                        // _assetic_2579ed4_5
                        if ($pathinfo === '/js/compiled/main2_PresetComboBox_6.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 5,  '_format' => 'js',  '_route' => '_assetic_2579ed4_5',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_Grid')) {
                            if (0 === strpos($pathinfo, '/js/compiled/main2_GridExporter')) {
                                // _assetic_2579ed4_6
                                if ($pathinfo === '/js/compiled/main2_GridExporter_7.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 6,  '_format' => 'js',  '_route' => '_assetic_2579ed4_6',);
                                }

                                // _assetic_2579ed4_7
                                if ($pathinfo === '/js/compiled/main2_GridExporterButton_8.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 7,  '_format' => 'js',  '_route' => '_assetic_2579ed4_7',);
                                }

                            }

                            // _assetic_2579ed4_8
                            if ($pathinfo === '/js/compiled/main2_GridImporterButton_9.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 8,  '_format' => 'js',  '_route' => '_assetic_2579ed4_8',);
                            }

                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_Import')) {
                            if (0 === strpos($pathinfo, '/js/compiled/main2_Importer')) {
                                // _assetic_2579ed4_9
                                if ($pathinfo === '/js/compiled/main2_Importer_10.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 9,  '_format' => 'js',  '_route' => '_assetic_2579ed4_9',);
                                }

                                // _assetic_2579ed4_10
                                if ($pathinfo === '/js/compiled/main2_ImporterEntityConfiguration_11.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 10,  '_format' => 'js',  '_route' => '_assetic_2579ed4_10',);
                                }

                                // _assetic_2579ed4_11
                                if ($pathinfo === '/js/compiled/main2_ImporterOneToManyConfiguration_12.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 11,  '_format' => 'js',  '_route' => '_assetic_2579ed4_11',);
                                }

                                // _assetic_2579ed4_12
                                if ($pathinfo === '/js/compiled/main2_ImporterManyToOneConfiguration_13.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 12,  '_format' => 'js',  '_route' => '_assetic_2579ed4_12',);
                                }

                                // _assetic_2579ed4_13
                                if ($pathinfo === '/js/compiled/main2_ImporterFieldConfiguration_14.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 13,  '_format' => 'js',  '_route' => '_assetic_2579ed4_13',);
                                }

                            }

                            // _assetic_2579ed4_14
                            if ($pathinfo === '/js/compiled/main2_ImportFieldMatcherGrid_15.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 14,  '_format' => 'js',  '_route' => '_assetic_2579ed4_14',);
                            }

                        }

                        // _assetic_2579ed4_15
                        if ($pathinfo === '/js/compiled/main2_OperatorStore_16.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 15,  '_format' => 'js',  '_route' => '_assetic_2579ed4_15',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_FilterExpression')) {
                            // _assetic_2579ed4_16
                            if ($pathinfo === '/js/compiled/main2_FilterExpression_17.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 16,  '_format' => 'js',  '_route' => '_assetic_2579ed4_16',);
                            }

                            // _assetic_2579ed4_17
                            if ($pathinfo === '/js/compiled/main2_FilterExpressionWindow_18.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 17,  '_format' => 'js',  '_route' => '_assetic_2579ed4_17',);
                            }

                        }

                        // _assetic_2579ed4_18
                        if ($pathinfo === '/js/compiled/main2_ModelTreeMaker_19.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 18,  '_format' => 'js',  '_route' => '_assetic_2579ed4_18',);
                        }

                        // _assetic_2579ed4_19
                        if ($pathinfo === '/js/compiled/main2_Blob_20.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 19,  '_format' => 'js',  '_route' => '_assetic_2579ed4_19',);
                        }

                        // _assetic_2579ed4_20
                        if ($pathinfo === '/js/compiled/main2_FileSaver_21.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 20,  '_format' => 'js',  '_route' => '_assetic_2579ed4_20',);
                        }

                        // _assetic_2579ed4_21
                        if ($pathinfo === '/js/compiled/main2_PagingToolbar_22.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 21,  '_format' => 'js',  '_route' => '_assetic_2579ed4_21',);
                        }

                        // _assetic_2579ed4_22
                        if ($pathinfo === '/js/compiled/main2_Exporter_23.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 22,  '_format' => 'js',  '_route' => '_assetic_2579ed4_22',);
                        }

                        // _assetic_2579ed4_23
                        if ($pathinfo === '/js/compiled/main2_Filter_24.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 23,  '_format' => 'js',  '_route' => '_assetic_2579ed4_23',);
                        }

                        // _assetic_2579ed4_24
                        if ($pathinfo === '/js/compiled/main2_LoginManager_25.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 24,  '_format' => 'js',  '_route' => '_assetic_2579ed4_24',);
                        }

                        // _assetic_2579ed4_25
                        if ($pathinfo === '/js/compiled/main2_Ext.grid.feature.Summary-selectorFix_26.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 25,  '_format' => 'js',  '_route' => '_assetic_2579ed4_25',);
                        }

                        // _assetic_2579ed4_26
                        if ($pathinfo === '/js/compiled/main2_AuthenticationProvider_27.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 26,  '_format' => 'js',  '_route' => '_assetic_2579ed4_26',);
                        }

                        // _assetic_2579ed4_27
                        if ($pathinfo === '/js/compiled/main2_HTTPBasicAuthenticationProvider_28.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 27,  '_format' => 'js',  '_route' => '_assetic_2579ed4_27',);
                        }

                        // _assetic_2579ed4_28
                        if ($pathinfo === '/js/compiled/main2_WSSEAuthenticationProvider_29.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 28,  '_format' => 'js',  '_route' => '_assetic_2579ed4_28',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_TipOfTheDay')) {
                            // _assetic_2579ed4_29
                            if ($pathinfo === '/js/compiled/main2_TipOfTheDayStore_30.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 29,  '_format' => 'js',  '_route' => '_assetic_2579ed4_29',);
                            }

                            // _assetic_2579ed4_30
                            if ($pathinfo === '/js/compiled/main2_TipOfTheDayHistoryStore_31.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 30,  '_format' => 'js',  '_route' => '_assetic_2579ed4_30',);
                            }

                        }

                        // _assetic_2579ed4_31
                        if ($pathinfo === '/js/compiled/main2_SystemPreferenceStore_32.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 31,  '_format' => 'js',  '_route' => '_assetic_2579ed4_31',);
                        }

                        // _assetic_2579ed4_32
                        if ($pathinfo === '/js/compiled/main2_UserProvidersStore_33.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 32,  '_format' => 'js',  '_route' => '_assetic_2579ed4_32',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_ProjectReport')) {
                            // _assetic_2579ed4_33
                            if ($pathinfo === '/js/compiled/main2_ProjectReport_34.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 33,  '_format' => 'js',  '_route' => '_assetic_2579ed4_33',);
                            }

                            // _assetic_2579ed4_34
                            if ($pathinfo === '/js/compiled/main2_ProjectReportList_35.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 34,  '_format' => 'js',  '_route' => '_assetic_2579ed4_34',);
                            }

                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_S')) {
                            // _assetic_2579ed4_35
                            if ($pathinfo === '/js/compiled/main2_SystemInformationRecord_36.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 35,  '_format' => 'js',  '_route' => '_assetic_2579ed4_35',);
                            }

                            // _assetic_2579ed4_36
                            if ($pathinfo === '/js/compiled/main2_StatisticSample_37.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 36,  '_format' => 'js',  '_route' => '_assetic_2579ed4_36',);
                            }

                        }

                        // _assetic_2579ed4_37
                        if ($pathinfo === '/js/compiled/main2_isaac_38.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 37,  '_format' => 'js',  '_route' => '_assetic_2579ed4_37',);
                        }

                        // _assetic_2579ed4_38
                        if ($pathinfo === '/js/compiled/main2_bcrypt_39.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 38,  '_format' => 'js',  '_route' => '_assetic_2579ed4_38',);
                        }

                        // _assetic_2579ed4_39
                        if ($pathinfo === '/js/compiled/main2_core_40.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 39,  '_format' => 'js',  '_route' => '_assetic_2579ed4_39',);
                        }

                        // _assetic_2579ed4_40
                        if ($pathinfo === '/js/compiled/main2_x64-core_41.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 40,  '_format' => 'js',  '_route' => '_assetic_2579ed4_40',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_sha')) {
                            // _assetic_2579ed4_41
                            if ($pathinfo === '/js/compiled/main2_sha512_42.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 41,  '_format' => 'js',  '_route' => '_assetic_2579ed4_41',);
                            }

                            // _assetic_2579ed4_42
                            if ($pathinfo === '/js/compiled/main2_sha1_43.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 42,  '_format' => 'js',  '_route' => '_assetic_2579ed4_42',);
                            }

                        }

                        // _assetic_2579ed4_43
                        if ($pathinfo === '/js/compiled/main2_enc-base64_44.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 43,  '_format' => 'js',  '_route' => '_assetic_2579ed4_43',);
                        }

                        // _assetic_2579ed4_44
                        if ($pathinfo === '/js/compiled/main2_Ext.data.Model-EXTJS-15037_45.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 44,  '_format' => 'js',  '_route' => '_assetic_2579ed4_44',);
                        }

                        // _assetic_2579ed4_45
                        if ($pathinfo === '/js/compiled/main2_JsonWithAssociationsWriter_46.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 45,  '_format' => 'js',  '_route' => '_assetic_2579ed4_45',);
                        }

                        // _assetic_2579ed4_46
                        if ($pathinfo === '/js/compiled/main2_PartKeepr_47.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 46,  '_format' => 'js',  '_route' => '_assetic_2579ed4_46',);
                        }

                        // _assetic_2579ed4_47
                        if ($pathinfo === '/js/compiled/main2_AppliedFiltersToolbar_48.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 47,  '_format' => 'js',  '_route' => '_assetic_2579ed4_47',);
                        }

                        // _assetic_2579ed4_48
                        if ($pathinfo === '/js/compiled/main2_FilterPlugin_49.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 48,  '_format' => 'js',  '_route' => '_assetic_2579ed4_48',);
                        }

                        // _assetic_2579ed4_49
                        if ($pathinfo === '/js/compiled/main2_compat_50.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 49,  '_format' => 'js',  '_route' => '_assetic_2579ed4_49',);
                        }

                        // _assetic_2579ed4_50
                        if ($pathinfo === '/js/compiled/main2_NumericField_51.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 50,  '_format' => 'js',  '_route' => '_assetic_2579ed4_50',);
                        }

                        // _assetic_2579ed4_51
                        if ($pathinfo === '/js/compiled/main2_TreePicker_52.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 51,  '_format' => 'js',  '_route' => '_assetic_2579ed4_51',);
                        }

                        // _assetic_2579ed4_52
                        if ($pathinfo === '/js/compiled/main2_CurrencyNumberField_53.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 52,  '_format' => 'js',  '_route' => '_assetic_2579ed4_52',);
                        }

                        // _assetic_2579ed4_53
                        if ($pathinfo === '/js/compiled/main2_SearchField_54.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 53,  '_format' => 'js',  '_route' => '_assetic_2579ed4_53',);
                        }

                        // _assetic_2579ed4_54
                        if ($pathinfo === '/js/compiled/main2_ClearableComboBox_55.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 54,  '_format' => 'js',  '_route' => '_assetic_2579ed4_54',);
                        }

                        // _assetic_2579ed4_55
                        if ($pathinfo === '/js/compiled/main2_ServiceCall_56.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 55,  '_format' => 'js',  '_route' => '_assetic_2579ed4_55',);
                        }

                        // _assetic_2579ed4_56
                        if ($pathinfo === '/js/compiled/main2_locale_57.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 56,  '_format' => 'js',  '_route' => '_assetic_2579ed4_56',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_FieldSelect')) {
                            // _assetic_2579ed4_57
                            if ($pathinfo === '/js/compiled/main2_FieldSelectorWindow_58.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 57,  '_format' => 'js',  '_route' => '_assetic_2579ed4_57',);
                            }

                            // _assetic_2579ed4_58
                            if ($pathinfo === '/js/compiled/main2_FieldSelectTrigger_59.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 58,  '_format' => 'js',  '_route' => '_assetic_2579ed4_58',);
                            }

                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_Ext.')) {
                            if (0 === strpos($pathinfo, '/js/compiled/main2_Ext.grid.plugin.')) {
                                // _assetic_2579ed4_59
                                if ($pathinfo === '/js/compiled/main2_Ext.grid.plugin.CellEditing-associationSupport_60.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 59,  '_format' => 'js',  '_route' => '_assetic_2579ed4_59',);
                                }

                                // _assetic_2579ed4_60
                                if ($pathinfo === '/js/compiled/main2_Ext.grid.plugin.Editing-associationSupport_61.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 60,  '_format' => 'js',  '_route' => '_assetic_2579ed4_60',);
                                }

                            }

                            // _assetic_2579ed4_61
                            if ($pathinfo === '/js/compiled/main2_Ext.form.field.ComboBox-associationSupport_62.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 61,  '_format' => 'js',  '_route' => '_assetic_2579ed4_61',);
                            }

                        }

                        // _assetic_2579ed4_62
                        if ($pathinfo === '/js/compiled/main2_HydraException_63.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 62,  '_format' => 'js',  '_route' => '_assetic_2579ed4_62',);
                        }

                        // _assetic_2579ed4_63
                        if ($pathinfo === '/js/compiled/main2_ExceptionWindow_64.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 63,  '_format' => 'js',  '_route' => '_assetic_2579ed4_63',);
                        }

                        // _assetic_2579ed4_64
                        if ($pathinfo === '/js/compiled/main2_FileUploadDialog_65.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 64,  '_format' => 'js',  '_route' => '_assetic_2579ed4_64',);
                        }

                        // _assetic_2579ed4_65
                        if ($pathinfo === '/js/compiled/main2_RememberChoiceMessageBox_66.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 65,  '_format' => 'js',  '_route' => '_assetic_2579ed4_65',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_Hydra')) {
                            // _assetic_2579ed4_66
                            if ($pathinfo === '/js/compiled/main2_HydraProxy_67.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 66,  '_format' => 'js',  '_route' => '_assetic_2579ed4_66',);
                            }

                            // _assetic_2579ed4_67
                            if ($pathinfo === '/js/compiled/main2_HydraReader_68.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 67,  '_format' => 'js',  '_route' => '_assetic_2579ed4_67',);
                            }

                            // _assetic_2579ed4_68
                            if ($pathinfo === '/js/compiled/main2_HydraTreeReader_69.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 68,  '_format' => 'js',  '_route' => '_assetic_2579ed4_68',);
                            }

                        }

                        // _assetic_2579ed4_69
                        if ($pathinfo === '/js/compiled/main2_PartCategoryStore_70.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 69,  '_format' => 'js',  '_route' => '_assetic_2579ed4_69',);
                        }

                        // _assetic_2579ed4_70
                        if ($pathinfo === '/js/compiled/main2_FootprintCategoryStore_71.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 70,  '_format' => 'js',  '_route' => '_assetic_2579ed4_70',);
                        }

                        // _assetic_2579ed4_71
                        if ($pathinfo === '/js/compiled/main2_StorageLocationCategoryStore_72.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 71,  '_format' => 'js',  '_route' => '_assetic_2579ed4_71',);
                        }

                        // _assetic_2579ed4_72
                        if ($pathinfo === '/js/compiled/main2_BarcodeScannerActionsStore_73.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 72,  '_format' => 'js',  '_route' => '_assetic_2579ed4_72',);
                        }

                        // _assetic_2579ed4_73
                        if ($pathinfo === '/js/compiled/main2_UserPreferenceStore_74.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 73,  '_format' => 'js',  '_route' => '_assetic_2579ed4_73',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_Ext.')) {
                            // _assetic_2579ed4_74
                            if ($pathinfo === '/js/compiled/main2_Ext.tree.View-missingMethods_75.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 74,  '_format' => 'js',  '_route' => '_assetic_2579ed4_74',);
                            }

                            // _assetic_2579ed4_75
                            if ($pathinfo === '/js/compiled/main2_Ext.form.Basic-AssociationSupport_76.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 75,  '_format' => 'js',  '_route' => '_assetic_2579ed4_75',);
                            }

                            // _assetic_2579ed4_76
                            if ($pathinfo === '/js/compiled/main2_Ext.ux.TreePicker-setValueWithObject_77.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 76,  '_format' => 'js',  '_route' => '_assetic_2579ed4_76',);
                            }

                        }

                        // _assetic_2579ed4_77
                        if ($pathinfo === '/js/compiled/main2_OperatorComboBox_78.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 77,  '_format' => 'js',  '_route' => '_assetic_2579ed4_77',);
                        }

                        // _assetic_2579ed4_78
                        if ($pathinfo === '/js/compiled/main2_BaseAction_79.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 78,  '_format' => 'js',  '_route' => '_assetic_2579ed4_78',);
                        }

                        // _assetic_2579ed4_79
                        if ($pathinfo === '/js/compiled/main2_LogoutAction_80.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 79,  '_format' => 'js',  '_route' => '_assetic_2579ed4_79',);
                        }

                        // _assetic_2579ed4_80
                        if ($pathinfo === '/js/compiled/main2_Statusbar_81.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 80,  '_format' => 'js',  '_route' => '_assetic_2579ed4_80',);
                        }

                        // _assetic_2579ed4_81
                        if ($pathinfo === '/js/compiled/main2_LoginDialog_82.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 81,  '_format' => 'js',  '_route' => '_assetic_2579ed4_81',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_Part')) {
                            if (0 === strpos($pathinfo, '/js/compiled/main2_PartI')) {
                                // _assetic_2579ed4_82
                                if ($pathinfo === '/js/compiled/main2_PartImageDisplay_83.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 82,  '_format' => 'js',  '_route' => '_assetic_2579ed4_82',);
                                }

                                // _assetic_2579ed4_83
                                if ($pathinfo === '/js/compiled/main2_PartInfoGrid_84.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 83,  '_format' => 'js',  '_route' => '_assetic_2579ed4_83',);
                                }

                            }

                            // _assetic_2579ed4_84
                            if ($pathinfo === '/js/compiled/main2_PartsManager_85.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 84,  '_format' => 'js',  '_route' => '_assetic_2579ed4_84',);
                            }

                            // _assetic_2579ed4_85
                            if ($pathinfo === '/js/compiled/main2_PartEditorWindow_86.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 85,  '_format' => 'js',  '_route' => '_assetic_2579ed4_85',);
                            }

                            // _assetic_2579ed4_86
                            if ($pathinfo === '/js/compiled/main2_PartDisplay_87.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 86,  '_format' => 'js',  '_route' => '_assetic_2579ed4_86',);
                            }

                            // _assetic_2579ed4_87
                            if ($pathinfo === '/js/compiled/main2_PartStockWindow_88.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 87,  '_format' => 'js',  '_route' => '_assetic_2579ed4_87',);
                            }

                            // _assetic_2579ed4_88
                            if ($pathinfo === '/js/compiled/main2_PartFilterPanel_89.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 88,  '_format' => 'js',  '_route' => '_assetic_2579ed4_88',);
                            }

                        }

                        // _assetic_2579ed4_89
                        if ($pathinfo === '/js/compiled/main2_MetaPartEditorWindow_90.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 89,  '_format' => 'js',  '_route' => '_assetic_2579ed4_89',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_PartParameterSearch')) {
                            // _assetic_2579ed4_90
                            if ($pathinfo === '/js/compiled/main2_PartParameterSearch_91.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 90,  '_format' => 'js',  '_route' => '_assetic_2579ed4_90',);
                            }

                            // _assetic_2579ed4_91
                            if ($pathinfo === '/js/compiled/main2_PartParameterSearchWindow_92.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 91,  '_format' => 'js',  '_route' => '_assetic_2579ed4_91',);
                            }

                        }

                        // _assetic_2579ed4_92
                        if ($pathinfo === '/js/compiled/main2_MenuBar_93.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 92,  '_format' => 'js',  '_route' => '_assetic_2579ed4_92',);
                        }

                        // _assetic_2579ed4_93
                        if ($pathinfo === '/js/compiled/main2_BaseGrid_94.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 93,  '_format' => 'js',  '_route' => '_assetic_2579ed4_93',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_Part')) {
                            // _assetic_2579ed4_94
                            if ($pathinfo === '/js/compiled/main2_PartParameterGrid_95.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 94,  '_format' => 'js',  '_route' => '_assetic_2579ed4_94',);
                            }

                            // _assetic_2579ed4_95
                            if ($pathinfo === '/js/compiled/main2_PartDistributorGrid_96.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 95,  '_format' => 'js',  '_route' => '_assetic_2579ed4_95',);
                            }

                            // _assetic_2579ed4_96
                            if ($pathinfo === '/js/compiled/main2_PartManufacturerGrid_97.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 96,  '_format' => 'js',  '_route' => '_assetic_2579ed4_96',);
                            }

                        }

                        // _assetic_2579ed4_97
                        if ($pathinfo === '/js/compiled/main2_AbstractStockHistoryGrid_98.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 97,  '_format' => 'js',  '_route' => '_assetic_2579ed4_97',);
                        }

                        // _assetic_2579ed4_98
                        if ($pathinfo === '/js/compiled/main2_PartStockHistory_99.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 98,  '_format' => 'js',  '_route' => '_assetic_2579ed4_98',);
                        }

                        // _assetic_2579ed4_99
                        if ($pathinfo === '/js/compiled/main2_StockHistoryGrid_100.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 99,  '_format' => 'js',  '_route' => '_assetic_2579ed4_99',);
                        }

                        // _assetic_2579ed4_100
                        if ($pathinfo === '/js/compiled/main2_UserPreferenceGrid_101.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 100,  '_format' => 'js',  '_route' => '_assetic_2579ed4_100',);
                        }

                        // _assetic_2579ed4_101
                        if ($pathinfo === '/js/compiled/main2_AttachmentGrid_102.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 101,  '_format' => 'js',  '_route' => '_assetic_2579ed4_101',);
                        }

                        // _assetic_2579ed4_102
                        if ($pathinfo === '/js/compiled/main2_PartAttachmentGrid_103.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 102,  '_format' => 'js',  '_route' => '_assetic_2579ed4_102',);
                        }

                        // _assetic_2579ed4_103
                        if ($pathinfo === '/js/compiled/main2_FootprintAttachmentGrid_104.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 103,  '_format' => 'js',  '_route' => '_assetic_2579ed4_103',);
                        }

                        // _assetic_2579ed4_104
                        if ($pathinfo === '/js/compiled/main2_ProjectAttachmentGrid_105.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 104,  '_format' => 'js',  '_route' => '_assetic_2579ed4_104',);
                        }

                        // _assetic_2579ed4_105
                        if ($pathinfo === '/js/compiled/main2_EditorGrid_106.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 105,  '_format' => 'js',  '_route' => '_assetic_2579ed4_105',);
                        }

                        // _assetic_2579ed4_106
                        if ($pathinfo === '/js/compiled/main2_DistributorGrid_107.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 106,  '_format' => 'js',  '_route' => '_assetic_2579ed4_106',);
                        }

                        // _assetic_2579ed4_107
                        if ($pathinfo === '/js/compiled/main2_PartsGrid_108.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 107,  '_format' => 'js',  '_route' => '_assetic_2579ed4_107',);
                        }

                        // _assetic_2579ed4_108
                        if ($pathinfo === '/js/compiled/main2_ManufacturerGrid_109.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 108,  '_format' => 'js',  '_route' => '_assetic_2579ed4_108',);
                        }

                        // _assetic_2579ed4_109
                        if ($pathinfo === '/js/compiled/main2_PartMeasurementUnitGrid_110.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 109,  '_format' => 'js',  '_route' => '_assetic_2579ed4_109',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_U')) {
                            // _assetic_2579ed4_110
                            if ($pathinfo === '/js/compiled/main2_UnitGrid_111.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 110,  '_format' => 'js',  '_route' => '_assetic_2579ed4_110',);
                            }

                            // _assetic_2579ed4_111
                            if ($pathinfo === '/js/compiled/main2_UserGrid_112.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 111,  '_format' => 'js',  '_route' => '_assetic_2579ed4_111',);
                            }

                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_S')) {
                            // _assetic_2579ed4_112
                            if ($pathinfo === '/js/compiled/main2_SystemNoticeGrid_113.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 112,  '_format' => 'js',  '_route' => '_assetic_2579ed4_112',);
                            }

                            // _assetic_2579ed4_113
                            if ($pathinfo === '/js/compiled/main2_StorageLocationGrid_114.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 113,  '_format' => 'js',  '_route' => '_assetic_2579ed4_113',);
                            }

                        }

                        // _assetic_2579ed4_114
                        if ($pathinfo === '/js/compiled/main2_ProjectGrid_115.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 114,  '_format' => 'js',  '_route' => '_assetic_2579ed4_114',);
                        }

                        // _assetic_2579ed4_115
                        if ($pathinfo === '/js/compiled/main2_MessageLog_116.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 115,  '_format' => 'js',  '_route' => '_assetic_2579ed4_115',);
                        }

                        // _assetic_2579ed4_116
                        if ($pathinfo === '/js/compiled/main2_ProjectPartGrid_117.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 116,  '_format' => 'js',  '_route' => '_assetic_2579ed4_116',);
                        }

                        // _assetic_2579ed4_117
                        if ($pathinfo === '/js/compiled/main2_SystemInformationGrid_118.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 117,  '_format' => 'js',  '_route' => '_assetic_2579ed4_117',);
                        }

                        // _assetic_2579ed4_118
                        if ($pathinfo === '/js/compiled/main2_GridMenuPlugin_119.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 118,  '_format' => 'js',  '_route' => '_assetic_2579ed4_118',);
                        }

                        // _assetic_2579ed4_119
                        if ($pathinfo === '/js/compiled/main2_TimeDisplay_120.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 119,  '_format' => 'js',  '_route' => '_assetic_2579ed4_119',);
                        }

                        // _assetic_2579ed4_120
                        if ($pathinfo === '/js/compiled/main2_Menu_121.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 120,  '_format' => 'js',  '_route' => '_assetic_2579ed4_120',);
                        }

                        // _assetic_2579ed4_121
                        if ($pathinfo === '/js/compiled/main2_UrlTextField_122.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 121,  '_format' => 'js',  '_route' => '_assetic_2579ed4_121',);
                        }

                        // _assetic_2579ed4_122
                        if ($pathinfo === '/js/compiled/main2_DisplayPreferencesPanel_123.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 122,  '_format' => 'js',  '_route' => '_assetic_2579ed4_122',);
                        }

                        // _assetic_2579ed4_123
                        if ($pathinfo === '/js/compiled/main2_UserPasswordChangePanel_124.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 123,  '_format' => 'js',  '_route' => '_assetic_2579ed4_123',);
                        }

                        // _assetic_2579ed4_124
                        if ($pathinfo === '/js/compiled/main2_StockPreferences_125.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 124,  '_format' => 'js',  '_route' => '_assetic_2579ed4_124',);
                        }

                        // _assetic_2579ed4_125
                        if ($pathinfo === '/js/compiled/main2_FormattingPreferences_126.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 125,  '_format' => 'js',  '_route' => '_assetic_2579ed4_125',);
                        }

                        // _assetic_2579ed4_126
                        if ($pathinfo === '/js/compiled/main2_TipOfTheDayPreferences_127.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 126,  '_format' => 'js',  '_route' => '_assetic_2579ed4_126',);
                        }

                        // _assetic_2579ed4_127
                        if ($pathinfo === '/js/compiled/main2_UserPreferences_128.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 127,  '_format' => 'js',  '_route' => '_assetic_2579ed4_127',);
                        }

                        // _assetic_2579ed4_128
                        if ($pathinfo === '/js/compiled/main2_RemotePartComboBox_129.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 128,  '_format' => 'js',  '_route' => '_assetic_2579ed4_128',);
                        }

                        // _assetic_2579ed4_129
                        if ($pathinfo === '/js/compiled/main2_FadingButton_130.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 129,  '_format' => 'js',  '_route' => '_assetic_2579ed4_129',);
                        }

                        // _assetic_2579ed4_130
                        if ($pathinfo === '/js/compiled/main2_SystemNoticeButton_131.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 130,  '_format' => 'js',  '_route' => '_assetic_2579ed4_130',);
                        }

                        // _assetic_2579ed4_131
                        if ($pathinfo === '/js/compiled/main2_ConnectionButton_132.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 131,  '_format' => 'js',  '_route' => '_assetic_2579ed4_131',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_SiUnit')) {
                            // _assetic_2579ed4_132
                            if ($pathinfo === '/js/compiled/main2_SiUnitList_133.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 132,  '_format' => 'js',  '_route' => '_assetic_2579ed4_132',);
                            }

                            // _assetic_2579ed4_133
                            if ($pathinfo === '/js/compiled/main2_SiUnitField_134.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 133,  '_format' => 'js',  '_route' => '_assetic_2579ed4_133',);
                            }

                        }

                        // _assetic_2579ed4_134
                        if ($pathinfo === '/js/compiled/main2_CategoryComboBox_135.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 134,  '_format' => 'js',  '_route' => '_assetic_2579ed4_134',);
                        }

                        // _assetic_2579ed4_135
                        if ($pathinfo === '/js/compiled/main2_PartParameterComboBox_136.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 135,  '_format' => 'js',  '_route' => '_assetic_2579ed4_135',);
                        }

                        // _assetic_2579ed4_136
                        if ($pathinfo === '/js/compiled/main2_RemoteImageField_137.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 136,  '_format' => 'js',  '_route' => '_assetic_2579ed4_136',);
                        }

                        // _assetic_2579ed4_137
                        if ($pathinfo === '/js/compiled/main2_WebcamPanel_138.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 137,  '_format' => 'js',  '_route' => '_assetic_2579ed4_137',);
                        }

                        // _assetic_2579ed4_138
                        if ($pathinfo === '/js/compiled/main2_ReloadableComboBox_139.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 138,  '_format' => 'js',  '_route' => '_assetic_2579ed4_138',);
                        }

                        // _assetic_2579ed4_139
                        if ($pathinfo === '/js/compiled/main2_DistributorComboBox_140.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 139,  '_format' => 'js',  '_route' => '_assetic_2579ed4_139',);
                        }

                        // _assetic_2579ed4_140
                        if ($pathinfo === '/js/compiled/main2_UserComboBox_141.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 140,  '_format' => 'js',  '_route' => '_assetic_2579ed4_140',);
                        }

                        // _assetic_2579ed4_141
                        if ($pathinfo === '/js/compiled/main2_FootprintComboBox_142.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 141,  '_format' => 'js',  '_route' => '_assetic_2579ed4_141',);
                        }

                        // _assetic_2579ed4_142
                        if ($pathinfo === '/js/compiled/main2_ManufacturerComboBox_143.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 142,  '_format' => 'js',  '_route' => '_assetic_2579ed4_142',);
                        }

                        // _assetic_2579ed4_143
                        if ($pathinfo === '/js/compiled/main2_UnitComboBox_144.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 143,  '_format' => 'js',  '_route' => '_assetic_2579ed4_143',);
                        }

                        // _assetic_2579ed4_144
                        if ($pathinfo === '/js/compiled/main2_PartUnitComboBox_145.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 144,  '_format' => 'js',  '_route' => '_assetic_2579ed4_144',);
                        }

                        // _assetic_2579ed4_145
                        if ($pathinfo === '/js/compiled/main2_StorageLocationComboBox_146.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 145,  '_format' => 'js',  '_route' => '_assetic_2579ed4_145',);
                        }

                        // _assetic_2579ed4_146
                        if ($pathinfo === '/js/compiled/main2_ResistorCalculator_147.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 146,  '_format' => 'js',  '_route' => '_assetic_2579ed4_146',);
                        }

                        // _assetic_2579ed4_147
                        if ($pathinfo === '/js/compiled/main2_SiUnitCombo_148.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 147,  '_format' => 'js',  '_route' => '_assetic_2579ed4_147',);
                        }

                        // _assetic_2579ed4_148
                        if ($pathinfo === '/js/compiled/main2_CharPickerMenu_149.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 148,  '_format' => 'js',  '_route' => '_assetic_2579ed4_148',);
                        }

                        // _assetic_2579ed4_149
                        if ($pathinfo === '/js/compiled/main2_Editor_150.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 149,  '_format' => 'js',  '_route' => '_assetic_2579ed4_149',);
                        }

                        // _assetic_2579ed4_150
                        if ($pathinfo === '/js/compiled/main2_DistributorEditor_151.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 150,  '_format' => 'js',  '_route' => '_assetic_2579ed4_150',);
                        }

                        // _assetic_2579ed4_151
                        if ($pathinfo === '/js/compiled/main2_PartEditor_152.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 151,  '_format' => 'js',  '_route' => '_assetic_2579ed4_151',);
                        }

                        // _assetic_2579ed4_152
                        if ($pathinfo === '/js/compiled/main2_ManufacturerEditor_153.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 152,  '_format' => 'js',  '_route' => '_assetic_2579ed4_152',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_Part')) {
                            // _assetic_2579ed4_153
                            if ($pathinfo === '/js/compiled/main2_PartParameterValueEditor_154.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 153,  '_format' => 'js',  '_route' => '_assetic_2579ed4_153',);
                            }

                            // _assetic_2579ed4_154
                            if ($pathinfo === '/js/compiled/main2_PartMeasurementUnitEditor_155.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 154,  '_format' => 'js',  '_route' => '_assetic_2579ed4_154',);
                            }

                        }

                        // _assetic_2579ed4_155
                        if ($pathinfo === '/js/compiled/main2_UnitEditor_156.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 155,  '_format' => 'js',  '_route' => '_assetic_2579ed4_155',);
                        }

                        // _assetic_2579ed4_156
                        if ($pathinfo === '/js/compiled/main2_FootprintEditor_157.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 156,  '_format' => 'js',  '_route' => '_assetic_2579ed4_156',);
                        }

                        // _assetic_2579ed4_157
                        if ($pathinfo === '/js/compiled/main2_UserEditor_158.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 157,  '_format' => 'js',  '_route' => '_assetic_2579ed4_157',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_S')) {
                            // _assetic_2579ed4_158
                            if ($pathinfo === '/js/compiled/main2_SystemNoticeEditor_159.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 158,  '_format' => 'js',  '_route' => '_assetic_2579ed4_158',);
                            }

                            // _assetic_2579ed4_159
                            if ($pathinfo === '/js/compiled/main2_StorageLocationEditor_160.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 159,  '_format' => 'js',  '_route' => '_assetic_2579ed4_159',);
                            }

                        }

                        // _assetic_2579ed4_160
                        if ($pathinfo === '/js/compiled/main2_MetaPartEditor_161.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 160,  '_format' => 'js',  '_route' => '_assetic_2579ed4_160',);
                        }

                        // _assetic_2579ed4_161
                        if ($pathinfo === '/js/compiled/main2_ProjectEditor_162.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 161,  '_format' => 'js',  '_route' => '_assetic_2579ed4_161',);
                        }

                        // _assetic_2579ed4_162
                        if ($pathinfo === '/js/compiled/main2_EditorComponent_163.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 162,  '_format' => 'js',  '_route' => '_assetic_2579ed4_162',);
                        }

                        // _assetic_2579ed4_163
                        if ($pathinfo === '/js/compiled/main2_DistributorEditorComponent_164.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 163,  '_format' => 'js',  '_route' => '_assetic_2579ed4_163',);
                        }

                        // _assetic_2579ed4_164
                        if ($pathinfo === '/js/compiled/main2_ManufacturerEditorComponent_165.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 164,  '_format' => 'js',  '_route' => '_assetic_2579ed4_164',);
                        }

                        // _assetic_2579ed4_165
                        if ($pathinfo === '/js/compiled/main2_PartMeasurementUnitEditorComponent_166.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 165,  '_format' => 'js',  '_route' => '_assetic_2579ed4_165',);
                        }

                        // _assetic_2579ed4_166
                        if ($pathinfo === '/js/compiled/main2_UnitEditorComponent_167.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 166,  '_format' => 'js',  '_route' => '_assetic_2579ed4_166',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_Footprint')) {
                            // _assetic_2579ed4_167
                            if ($pathinfo === '/js/compiled/main2_FootprintEditorComponent_168.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 167,  '_format' => 'js',  '_route' => '_assetic_2579ed4_167',);
                            }

                            // _assetic_2579ed4_168
                            if ($pathinfo === '/js/compiled/main2_FootprintNavigation_169.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 168,  '_format' => 'js',  '_route' => '_assetic_2579ed4_168',);
                            }

                            // _assetic_2579ed4_169
                            if ($pathinfo === '/js/compiled/main2_FootprintGrid_170.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 169,  '_format' => 'js',  '_route' => '_assetic_2579ed4_169',);
                            }

                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_BatchJob')) {
                            if (0 === strpos($pathinfo, '/js/compiled/main2_BatchJobEditor')) {
                                // _assetic_2579ed4_170
                                if ($pathinfo === '/js/compiled/main2_BatchJobEditor_171.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 170,  '_format' => 'js',  '_route' => '_assetic_2579ed4_170',);
                                }

                                // _assetic_2579ed4_171
                                if ($pathinfo === '/js/compiled/main2_BatchJobEditorComponent_172.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 171,  '_format' => 'js',  '_route' => '_assetic_2579ed4_171',);
                                }

                            }

                            // _assetic_2579ed4_172
                            if ($pathinfo === '/js/compiled/main2_BatchJobGrid_173.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 172,  '_format' => 'js',  '_route' => '_assetic_2579ed4_172',);
                            }

                            if (0 === strpos($pathinfo, '/js/compiled/main2_BatchJobUpdateExpression')) {
                                // _assetic_2579ed4_173
                                if ($pathinfo === '/js/compiled/main2_BatchJobUpdateExpression_174.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 173,  '_format' => 'js',  '_route' => '_assetic_2579ed4_173',);
                                }

                                // _assetic_2579ed4_174
                                if ($pathinfo === '/js/compiled/main2_BatchJobUpdateExpressionWindow_175.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 174,  '_format' => 'js',  '_route' => '_assetic_2579ed4_174',);
                                }

                            }

                            // _assetic_2579ed4_175
                            if ($pathinfo === '/js/compiled/main2_BatchJobExecutionWindow_176.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 175,  '_format' => 'js',  '_route' => '_assetic_2579ed4_175',);
                            }

                        }

                        // _assetic_2579ed4_176
                        if ($pathinfo === '/js/compiled/main2_UserEditorComponent_177.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 176,  '_format' => 'js',  '_route' => '_assetic_2579ed4_176',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_S')) {
                            // _assetic_2579ed4_177
                            if ($pathinfo === '/js/compiled/main2_SystemNoticeEditorComponent_178.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 177,  '_format' => 'js',  '_route' => '_assetic_2579ed4_177',);
                            }

                            // _assetic_2579ed4_178
                            if ($pathinfo === '/js/compiled/main2_StorageLocationEditorComponent_179.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 178,  '_format' => 'js',  '_route' => '_assetic_2579ed4_178',);
                            }

                        }

                        // _assetic_2579ed4_179
                        if ($pathinfo === '/js/compiled/main2_ProjectEditorComponent_180.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 179,  '_format' => 'js',  '_route' => '_assetic_2579ed4_179',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_StorageLocation')) {
                            if (0 === strpos($pathinfo, '/js/compiled/main2_StorageLocationMultiAdd')) {
                                // _assetic_2579ed4_180
                                if ($pathinfo === '/js/compiled/main2_StorageLocationMultiAddWindow_181.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 180,  '_format' => 'js',  '_route' => '_assetic_2579ed4_180',);
                                }

                                // _assetic_2579ed4_181
                                if ($pathinfo === '/js/compiled/main2_StorageLocationMultiAddDialog_182.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 181,  '_format' => 'js',  '_route' => '_assetic_2579ed4_181',);
                                }

                            }

                            // _assetic_2579ed4_182
                            if ($pathinfo === '/js/compiled/main2_StorageLocationNavigation_183.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 182,  '_format' => 'js',  '_route' => '_assetic_2579ed4_182',);
                            }

                        }

                        // _assetic_2579ed4_183
                        if ($pathinfo === '/js/compiled/main2_ProjectReport_184.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 183,  '_format' => 'js',  '_route' => '_assetic_2579ed4_183',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_S')) {
                            if (0 === strpos($pathinfo, '/js/compiled/main2_StatisticsChart')) {
                                // _assetic_2579ed4_184
                                if ($pathinfo === '/js/compiled/main2_StatisticsChart_185.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 184,  '_format' => 'js',  '_route' => '_assetic_2579ed4_184',);
                                }

                                // _assetic_2579ed4_185
                                if ($pathinfo === '/js/compiled/main2_StatisticsChartPanel_186.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 185,  '_format' => 'js',  '_route' => '_assetic_2579ed4_185',);
                                }

                            }

                            // _assetic_2579ed4_186
                            if ($pathinfo === '/js/compiled/main2_SummaryStatisticsPanel_187.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 186,  '_format' => 'js',  '_route' => '_assetic_2579ed4_186',);
                            }

                            // _assetic_2579ed4_187
                            if ($pathinfo === '/js/compiled/main2_SystemNoticeStore_188.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 187,  '_format' => 'js',  '_route' => '_assetic_2579ed4_187',);
                            }

                        }

                        // _assetic_2579ed4_188
                        if ($pathinfo === '/js/compiled/main2_TipOfTheDayWindow_189.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 188,  '_format' => 'js',  '_route' => '_assetic_2579ed4_188',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_Category')) {
                            // _assetic_2579ed4_189
                            if ($pathinfo === '/js/compiled/main2_CategoryTree_190.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 189,  '_format' => 'js',  '_route' => '_assetic_2579ed4_189',);
                            }

                            // _assetic_2579ed4_190
                            if ($pathinfo === '/js/compiled/main2_CategoryEditorTree_191.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 190,  '_format' => 'js',  '_route' => '_assetic_2579ed4_190',);
                            }

                        }

                        // _assetic_2579ed4_191
                        if ($pathinfo === '/js/compiled/main2_StorageLocationTree_192.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 191,  '_format' => 'js',  '_route' => '_assetic_2579ed4_191',);
                        }

                        // _assetic_2579ed4_192
                        if ($pathinfo === '/js/compiled/main2_PartCategoryTree_193.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 192,  '_format' => 'js',  '_route' => '_assetic_2579ed4_192',);
                        }

                        // _assetic_2579ed4_193
                        if ($pathinfo === '/js/compiled/main2_FootprintTree_194.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 193,  '_format' => 'js',  '_route' => '_assetic_2579ed4_193',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_C')) {
                            if (0 === strpos($pathinfo, '/js/compiled/main2_CategoryEditor')) {
                                // _assetic_2579ed4_194
                                if ($pathinfo === '/js/compiled/main2_CategoryEditorWindow_195.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 194,  '_format' => 'js',  '_route' => '_assetic_2579ed4_194',);
                                }

                                // _assetic_2579ed4_195
                                if ($pathinfo === '/js/compiled/main2_CategoryEditorForm_196.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 195,  '_format' => 'js',  '_route' => '_assetic_2579ed4_195',);
                                }

                            }

                            // _assetic_2579ed4_196
                            if ($pathinfo === '/js/compiled/main2_CharPicker_197.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 196,  '_format' => 'js',  '_route' => '_assetic_2579ed4_196',);
                            }

                        }

                        // _assetic_2579ed4_197
                        if ($pathinfo === '/js/compiled/main2_StorageLocationPicker_198.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 197,  '_format' => 'js',  '_route' => '_assetic_2579ed4_197',);
                        }

                        // _assetic_2579ed4_198
                        if ($pathinfo === '/js/compiled/main2_Panel_199.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 198,  '_format' => 'js',  '_route' => '_assetic_2579ed4_198',);
                        }

                        // _assetic_2579ed4_199
                        if ($pathinfo === '/js/compiled/main2_Tree_200.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 199,  '_format' => 'js',  '_route' => '_assetic_2579ed4_199',);
                        }

                        // _assetic_2579ed4_200
                        if ($pathinfo === '/js/compiled/main2_PreferenceEditor_201.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 200,  '_format' => 'js',  '_route' => '_assetic_2579ed4_200',);
                        }

                        // _assetic_2579ed4_201
                        if ($pathinfo === '/js/compiled/main2_FulltextSearch_202.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 201,  '_format' => 'js',  '_route' => '_assetic_2579ed4_201',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_RequiredPart')) {
                            // _assetic_2579ed4_202
                            if ($pathinfo === '/js/compiled/main2_RequiredPartFields_203.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 202,  '_format' => 'js',  '_route' => '_assetic_2579ed4_202',);
                            }

                            // _assetic_2579ed4_203
                            if ($pathinfo === '/js/compiled/main2_RequiredPartManufacturerFields_204.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 203,  '_format' => 'js',  '_route' => '_assetic_2579ed4_203',);
                            }

                            // _assetic_2579ed4_204
                            if ($pathinfo === '/js/compiled/main2_RequiredPartDistributorFields_205.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 204,  '_format' => 'js',  '_route' => '_assetic_2579ed4_204',);
                            }

                        }

                        // _assetic_2579ed4_205
                        if ($pathinfo === '/js/compiled/main2_BarcodeScannerConfiguration_206.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 205,  '_format' => 'js',  '_route' => '_assetic_2579ed4_205',);
                        }

                        // _assetic_2579ed4_206
                        if ($pathinfo === '/js/compiled/main2_ActionsConfiguration_207.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 206,  '_format' => 'js',  '_route' => '_assetic_2579ed4_206',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_ProjectRun')) {
                            // _assetic_2579ed4_207
                            if ($pathinfo === '/js/compiled/main2_ProjectRunEditor_208.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 207,  '_format' => 'js',  '_route' => '_assetic_2579ed4_207',);
                            }

                            // _assetic_2579ed4_208
                            if ($pathinfo === '/js/compiled/main2_ProjectRunGrid_209.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 208,  '_format' => 'js',  '_route' => '_assetic_2579ed4_208',);
                            }

                            // _assetic_2579ed4_209
                            if ($pathinfo === '/js/compiled/main2_ProjectRunEditorComponent_210.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 209,  '_format' => 'js',  '_route' => '_assetic_2579ed4_209',);
                            }

                        }

                        // _assetic_2579ed4_210
                        if ($pathinfo === '/js/compiled/main2_Manager_211.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 210,  '_format' => 'js',  '_route' => '_assetic_2579ed4_210',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_A')) {
                            if (0 === strpos($pathinfo, '/js/compiled/main2_Action')) {
                                // _assetic_2579ed4_211
                                if ($pathinfo === '/js/compiled/main2_Action_212.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 211,  '_format' => 'js',  '_route' => '_assetic_2579ed4_211',);
                                }

                                // _assetic_2579ed4_212
                                if ($pathinfo === '/js/compiled/main2_ActionsComboBox_213.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 212,  '_format' => 'js',  '_route' => '_assetic_2579ed4_212',);
                                }

                            }

                            if (0 === strpos($pathinfo, '/js/compiled/main2_Add')) {
                                if (0 === strpos($pathinfo, '/js/compiled/main2_AddRemoveStock')) {
                                    // _assetic_2579ed4_213
                                    if ($pathinfo === '/js/compiled/main2_AddRemoveStock_214.js') {
                                        return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 213,  '_format' => 'js',  '_route' => '_assetic_2579ed4_213',);
                                    }

                                    // _assetic_2579ed4_214
                                    if ($pathinfo === '/js/compiled/main2_AddRemoveStockWindow_215.js') {
                                        return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 214,  '_format' => 'js',  '_route' => '_assetic_2579ed4_214',);
                                    }

                                }

                                // _assetic_2579ed4_215
                                if ($pathinfo === '/js/compiled/main2_AddPart_216.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 215,  '_format' => 'js',  '_route' => '_assetic_2579ed4_215',);
                                }

                            }

                        }

                        // _assetic_2579ed4_216
                        if ($pathinfo === '/js/compiled/main2_SearchPart_217.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 216,  '_format' => 'js',  '_route' => '_assetic_2579ed4_216',);
                        }

                        // _assetic_2579ed4_217
                        if ($pathinfo === '/js/compiled/main2_FieldSelector_218.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 217,  '_format' => 'js',  '_route' => '_assetic_2579ed4_217',);
                        }

                        // _assetic_2579ed4_218
                        if ($pathinfo === '/js/compiled/main2_Message_219.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 218,  '_format' => 'js',  '_route' => '_assetic_2579ed4_218',);
                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_Ext.ux.Wizard')) {
                            if (0 === strpos($pathinfo, '/js/compiled/main2_Ext.ux.Wizard.')) {
                                // _assetic_2579ed4_219
                                if ($pathinfo === '/js/compiled/main2_Ext.ux.Wizard.Card_220.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 219,  '_format' => 'js',  '_route' => '_assetic_2579ed4_219',);
                                }

                                // _assetic_2579ed4_220
                                if ($pathinfo === '/js/compiled/main2_Ext.ux.Wizard.Header_221.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 220,  '_format' => 'js',  '_route' => '_assetic_2579ed4_220',);
                                }

                            }

                            // _assetic_2579ed4_221
                            if ($pathinfo === '/js/compiled/main2_Ext.ux.Wizard_222.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 221,  '_format' => 'js',  '_route' => '_assetic_2579ed4_221',);
                            }

                            // _assetic_2579ed4_222
                            if ($pathinfo === '/js/compiled/main2_Ext.ux.Wizard.CardLayout_223.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 222,  '_format' => 'js',  '_route' => '_assetic_2579ed4_222',);
                            }

                        }

                        if (0 === strpos($pathinfo, '/js/compiled/main2_Search')) {
                            // _assetic_2579ed4_223
                            if ($pathinfo === '/js/compiled/main2_SearchPanel_224.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 223,  '_format' => 'js',  '_route' => '_assetic_2579ed4_223',);
                            }

                            // _assetic_2579ed4_224
                            if ($pathinfo === '/js/compiled/main2_SearchWindow_225.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 224,  '_format' => 'js',  '_route' => '_assetic_2579ed4_224',);
                            }

                        }

                        // _assetic_2579ed4_225
                        if ($pathinfo === '/js/compiled/main2_DataApplicator_226.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 225,  '_format' => 'js',  '_route' => '_assetic_2579ed4_225',);
                        }

                        // _assetic_2579ed4_226
                        if ($pathinfo === '/js/compiled/main2_php.default.min_227.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '2579ed4',  'pos' => 226,  '_format' => 'js',  '_route' => '_assetic_2579ed4_226',);
                        }

                    }

                }

            }

        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/api')) {
            if (0 === strpos($pathinfo, '/api/users')) {
                // partkeepr_auth_default_getsalt
                if ($pathinfo === '/api/users/getSalt') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_partkeepr_auth_default_getsalt;
                    }

                    return array (  'method' => 'post',  '_format' => 'json',  '_controller' => 'PartKeepr\\AuthBundle\\Controller\\DefaultController::getSaltAction',  '_route' => 'partkeepr_auth_default_getsalt',);
                }
                not_partkeepr_auth_default_getsalt:

                // partkeepr_auth_default_logout
                if ($pathinfo === '/api/users/logout') {
                    return array (  'method' => 'GET',  '_format' => 'json',  '_controller' => 'PartKeepr\\AuthBundle\\Controller\\DefaultController::logoutAction',  '_route' => 'partkeepr_auth_default_logout',);
                }

            }

            if (0 === strpos($pathinfo, '/api/octopart')) {
                // partkeepr_octopart_default_index
                if (0 === strpos($pathinfo, '/api/octopart/get') && preg_match('#^/api/octopart/get/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_partkeepr_octopart_default_index;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'partkeepr_octopart_default_index')), array (  'method' => 'GET',  '_format' => 'json',  '_controller' => 'PartKeepr\\OctoPartBundle\\Controller\\DefaultController::indexAction',));
                }
                not_partkeepr_octopart_default_index:

                // partkeepr_octopart_default_getpartsbyquery
                if (rtrim($pathinfo, '/') === '/api/octopart/query') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_partkeepr_octopart_default_getpartsbyquery;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'partkeepr_octopart_default_getpartsbyquery');
                    }

                    return array (  'method' => 'GET',  '_format' => 'json',  '_controller' => 'PartKeepr\\OctoPartBundle\\Controller\\DefaultController::getPartsByQueryAction',  '_route' => 'partkeepr_octopart_default_getpartsbyquery',);
                }
                not_partkeepr_octopart_default_getpartsbyquery:

            }

            if (0 === strpos($pathinfo, '/api/system_')) {
                // partkeepr_core_default_getsystemstatus
                if ($pathinfo === '/api/system_status') {
                    return array (  'method' => 'get',  '_format' => 'json',  '_controller' => 'PartKeepr\\CoreBundle\\Controller\\DefaultController::getSystemStatusAction',  '_route' => 'partkeepr_core_default_getsystemstatus',);
                }

                // partkeepr_core_default_getsysteminformation
                if ($pathinfo === '/api/system_information') {
                    return array (  'method' => 'get',  '_format' => 'json',  '_controller' => 'PartKeepr\\CoreBundle\\Controller\\DefaultController::getSystemInformationAction',  '_route' => 'partkeepr_core_default_getsysteminformation',);
                }

            }

            // partkeepr_core_default_getdiskfreespace
            if ($pathinfo === '/api/disk_space') {
                return array (  'method' => 'get',  '_format' => 'json',  '_controller' => 'PartKeepr\\CoreBundle\\Controller\\DefaultController::getDiskFreeSpaceAction',  '_route' => 'partkeepr_core_default_getdiskfreespace',);
            }

            // partkeepr_core_default_getcurrencies
            if ($pathinfo === '/api/currencies') {
                return array (  'method' => 'get',  '_format' => 'json',  '_controller' => 'PartKeepr\\CoreBundle\\Controller\\DefaultController::getCurrenciesAction',  '_route' => 'partkeepr_core_default_getcurrencies',);
            }

            // partkeepr_project_projectreport_getprojectreport
            if ($pathinfo === '/api/project_reports') {
                return array (  'method' => 'get',  '_format' => 'json',  '_controller' => 'PartKeepr\\ProjectBundle\\Controller\\ProjectReportController::getProjectReportAction',  '_route' => 'partkeepr_project_projectreport_getprojectreport',);
            }

            if (0 === strpos($pathinfo, '/api/statistics')) {
                // partkeepr_statistic_statistic_getcurrentstatistic
                if ($pathinfo === '/api/statistics/current') {
                    return array (  'method' => 'get',  '_format' => 'json',  '_controller' => 'PartKeepr\\StatisticBundle\\Controller\\StatisticController::getCurrentStatisticAction',  '_route' => 'partkeepr_statistic_statistic_getcurrentstatistic',);
                }

                // partkeepr_statistic_statistic_getsampledstatistic
                if ($pathinfo === '/api/statistics/sampled') {
                    return array (  'method' => 'get',  '_format' => 'json',  '_controller' => 'PartKeepr\\StatisticBundle\\Controller\\StatisticController::getSampledStatisticAction',  '_route' => 'partkeepr_statistic_statistic_getsampledstatistic',);
                }

                // partkeepr_statistic_statistic_getstatisticrange
                if ($pathinfo === '/api/statistics/range') {
                    return array (  'method' => 'get',  '_format' => 'json',  '_controller' => 'PartKeepr\\StatisticBundle\\Controller\\StatisticController::getStatisticRangeAction',  '_route' => 'partkeepr_statistic_statistic_getstatisticrange',);
                }

            }

            // partkeepr_cronlogger_cronrunner_runcrons
            if ($pathinfo === '/api/cron/run') {
                return array (  '_controller' => 'PartKeepr\\CronLoggerBundle\\Controller\\CronRunnerController::runCronsAction',  '_route' => 'partkeepr_cronlogger_cronrunner_runcrons',);
            }

            if (0 === strpos($pathinfo, '/api/parts')) {
                // partkeepr_part_part_massremovestock
                if ($pathinfo === '/api/parts/massRemoveStock') {
                    return array (  'method' => 'get',  '_format' => 'json',  '_controller' => 'PartKeepr\\PartBundle\\Controller\\PartController::massRemoveStockAction',  '_route' => 'partkeepr_part_part_massremovestock',);
                }

                if (0 === strpos($pathinfo, '/api/parts/getPartParameter')) {
                    // partkeepr_part_part_getparameternames
                    if ($pathinfo === '/api/parts/getPartParameterNames') {
                        return array (  'method' => 'get',  '_format' => 'json',  '_controller' => 'PartKeepr\\PartBundle\\Controller\\PartController::getParameterNamesAction',  '_route' => 'partkeepr_part_part_getparameternames',);
                    }

                    // partkeepr_part_part_getparametervalues
                    if ($pathinfo === '/api/parts/getPartParameterValues') {
                        return array (  'method' => 'get',  '_format' => 'json',  '_controller' => 'PartKeepr\\PartBundle\\Controller\\PartController::getParameterValuesAction',  '_route' => 'partkeepr_part_part_getparametervalues',);
                    }

                }

            }

        }

        // partkeepr_frontend_index_index
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'partkeepr_frontend_index_index');
            }

            return array (  '_controller' => 'PartKeepr\\FrontendBundle\\Controller\\IndexController::indexAction',  '_route' => 'partkeepr_frontend_index_index',);
        }

        if (0 === strpos($pathinfo, '/get')) {
            // partkeepr_import_import_getsource
            if (rtrim($pathinfo, '/') === '/getSource') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'partkeepr_import_import_getsource');
                }

                return array (  '_controller' => 'PartKeepr\\ImportBundle\\Controller\\ImportController::getSourceAction',  '_route' => 'partkeepr_import_import_getsource',);
            }

            // partkeepr_import_import_getpreview
            if ($pathinfo === '/getPreview/') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_partkeepr_import_import_getpreview;
                }

                return array (  '_controller' => 'PartKeepr\\ImportBundle\\Controller\\ImportController::getPreviewAction',  '_route' => 'partkeepr_import_import_getpreview',);
            }
            not_partkeepr_import_import_getpreview:

        }

        // partkeepr_import_import_import
        if ($pathinfo === '/executeImport/') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_partkeepr_import_import_import;
            }

            return array (  '_controller' => 'PartKeepr\\ImportBundle\\Controller\\ImportController::importAction',  '_route' => 'partkeepr_import_import_import',);
        }
        not_partkeepr_import_import_import:

        // partkeepr_export_export_export
        if ($pathinfo === '/api/export') {
            return array (  'method' => 'post',  '_format' => 'json',  '_controller' => 'PartKeepr\\ExportBundle\\Controller\\ExportController::exportAction',  '_route' => 'partkeepr_export_export_export',);
        }

        if (0 === strpos($pathinfo, '/setup')) {
            // partkeepr_setup_setup_inttestconnectivity
            if ($pathinfo === '/setup/_int_test_connectivity') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\SetupController::intTestConnectivityAction',  '_route' => 'partkeepr_setup_setup_inttestconnectivity',);
            }

            // partkeepr_setup_setup_testconnectivity
            if ($pathinfo === '/setup/testConnectivity') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\SetupController::testConnectivityAction',  '_route' => 'partkeepr_setup_setup_testconnectivity',);
            }

            // partkeepr_setup_setup_saveconfig
            if ($pathinfo === '/setup/saveConfig') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\SetupController::saveConfigAction',  '_route' => 'partkeepr_setup_setup_saveconfig',);
            }

            // partkeepr_setup_setup_webservertest
            if ($pathinfo === '/setup/webserverTest') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\SetupController::webserverTestAction',  '_route' => 'partkeepr_setup_setup_webservertest',);
            }

            // partkeepr_setup_setup_generateauthkey
            if ($pathinfo === '/setup/generateAuthKey') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\SetupController::generateAuthKeyAction',  '_route' => 'partkeepr_setup_setup_generateauthkey',);
            }

            // partkeepr_setup_cachewarmupsetup_intcachewarmup
            if ($pathinfo === '/setup/_int_cache_warmup') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\CacheWarmupSetupController::intCacheWarmupAction',  '_route' => 'partkeepr_setup_cachewarmupsetup_intcachewarmup',);
            }

            // partkeepr_setup_cachewarmupsetup_cachewarmup
            if ($pathinfo === '/setup/warmupCache') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\CacheWarmupSetupController::cacheWarmupAction',  '_route' => 'partkeepr_setup_cachewarmupsetup_cachewarmup',);
            }

            // partkeepr_setup_schemasetup_intsetupschema
            if ($pathinfo === '/setup/_int_setup_schema') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\SchemaSetupController::intSetupSchemaAction',  '_route' => 'partkeepr_setup_schemasetup_intsetupschema',);
            }

            // partkeepr_setup_schemasetup_setupschema
            if ($pathinfo === '/setup/schemaSetup') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\SchemaSetupController::setupSchemaAction',  '_route' => 'partkeepr_setup_schemasetup_setupschema',);
            }

            // partkeepr_setup_adminusersetup_createuser
            if ($pathinfo === '/setup/createUser') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\AdminUserSetupController::createUserAction',  '_route' => 'partkeepr_setup_adminusersetup_createuser',);
            }

            if (0 === strpos($pathinfo, '/setup/_int_')) {
                // partkeepr_setup_adminusersetup_intcreateuser
                if ($pathinfo === '/setup/_int_create_user') {
                    return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\AdminUserSetupController::intCreateUserAction',  '_route' => 'partkeepr_setup_adminusersetup_intcreateuser',);
                }

                // partkeepr_setup_schemamigrationsetup_intmigrateschema
                if ($pathinfo === '/setup/_int_migrate_schema') {
                    return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\SchemaMigrationSetupController::intMigrateSchemaAction',  '_route' => 'partkeepr_setup_schemamigrationsetup_intmigrateschema',);
                }

            }

            // partkeepr_setup_schemamigrationsetup_migrateschema
            if ($pathinfo === '/setup/schemaMigration') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\SchemaMigrationSetupController::migrateSchemaAction',  '_route' => 'partkeepr_setup_schemamigrationsetup_migrateschema',);
            }

            // partkeepr_setup_partunitsetup_createpartunits
            if ($pathinfo === '/setup/createPartUnits') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\PartUnitSetupController::createPartUnitsAction',  '_route' => 'partkeepr_setup_partunitsetup_createpartunits',);
            }

            if (0 === strpos($pathinfo, '/setup/_int_create_')) {
                // partkeepr_setup_partunitsetup_intcreatepartunits
                if ($pathinfo === '/setup/_int_create_part_units') {
                    return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\PartUnitSetupController::intCreatePartUnitsAction',  '_route' => 'partkeepr_setup_partunitsetup_intcreatepartunits',);
                }

                // partkeepr_setup_footprintsetup_intcreatefootprints
                if ($pathinfo === '/setup/_int_create_footprints') {
                    return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\FootprintSetupController::intCreateFootprints',  '_route' => 'partkeepr_setup_footprintsetup_intcreatefootprints',);
                }

            }

            // partkeepr_setup_footprintsetup_createfootprints
            if ($pathinfo === '/setup/createFootprints') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\FootprintSetupController::createFootprintsAction',  '_route' => 'partkeepr_setup_footprintsetup_createfootprints',);
            }

            // partkeepr_setup_siprefixsetup_intcreatesiprefixes
            if ($pathinfo === '/setup/_int_create_si_prefixes') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\SiPrefixSetupController::intCreateSiPrefixes',  '_route' => 'partkeepr_setup_siprefixsetup_intcreatesiprefixes',);
            }

            // partkeepr_setup_siprefixsetup_createsiprefixes
            if ($pathinfo === '/setup/createSiPrefixes') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\SiPrefixSetupController::createSiPrefixesAction',  '_route' => 'partkeepr_setup_siprefixsetup_createsiprefixes',);
            }

            // partkeepr_setup_unitsetup_intcreateunits
            if ($pathinfo === '/setup/_int_create_units') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\UnitSetupController::intCreateUnitsAction',  '_route' => 'partkeepr_setup_unitsetup_intcreateunits',);
            }

            // partkeepr_setup_unitsetup_createunit
            if ($pathinfo === '/setup/createUnits') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\UnitSetupController::createUnitAction',  '_route' => 'partkeepr_setup_unitsetup_createunit',);
            }

            // partkeepr_setup_manufacturersetup_intcreatemanufacturers
            if ($pathinfo === '/setup/_int_create_manufacturers') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\ManufacturerSetupController::intCreateManufacturersAction',  '_route' => 'partkeepr_setup_manufacturersetup_intcreatemanufacturers',);
            }

            // partkeepr_setup_manufacturersetup_createmanufacturers
            if ($pathinfo === '/setup/createManufacturers') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\ManufacturerSetupController::createManufacturersAction',  '_route' => 'partkeepr_setup_manufacturersetup_createmanufacturers',);
            }

            // partkeepr_setup_existingconfigparser_parseexistingconfig
            if ($pathinfo === '/setup/parseExistingConfig') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\ExistingConfigParserController::parseExistingConfigAction',  '_route' => 'partkeepr_setup_existingconfigparser_parseexistingconfig',);
            }

            // partkeepr_setup_existingusersetup_testexistingusers
            if ($pathinfo === '/setup/testExistingUsers') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\ExistingUserSetupController::testExistingUsersAction',  '_route' => 'partkeepr_setup_existingusersetup_testexistingusers',);
            }

            // partkeepr_setup_existingusersetup_inttestexistingusers
            if ($pathinfo === '/setup/_int_test_existing_users') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\ExistingUserSetupController::intTestExistingUsersAction',  '_route' => 'partkeepr_setup_existingusersetup_inttestexistingusers',);
            }

            // partkeepr_setup_filemigration_migratefiles
            if ($pathinfo === '/setup/migrateFiles') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\FileMigrationController::migrateFilesAction',  '_route' => 'partkeepr_setup_filemigration_migratefiles',);
            }

            // partkeepr_setup_filemigration_intmigratefiles
            if ($pathinfo === '/setup/_int_migrate_files_action') {
                return array (  '_controller' => 'PartKeepr\\SetupBundle\\Controller\\FileMigrationController::intMigrateFilesAction',  '_route' => 'partkeepr_setup_filemigration_intmigratefiles',);
            }

        }

        if (0 === strpos($pathinfo, '/api')) {
            // api_jsonld_entrypoint
            if (rtrim($pathinfo, '/') === '/api') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'api_jsonld_entrypoint');
                }

                return array (  '_controller' => 'api.jsonld.action.entrypoint',  '_route' => 'api_jsonld_entrypoint',);
            }

            // api_jsonld_context
            if (0 === strpos($pathinfo, '/api/contexts') && preg_match('#^/api/contexts/(?P<shortName>.+)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_jsonld_context')), array (  '_controller' => 'api.jsonld.action.context',));
            }

            // api_hydra_vocab
            if ($pathinfo === '/api/apidoc') {
                return array (  '_controller' => 'api.hydra.action.documentation',  '_route' => 'api_hydra_vocab',);
            }

            if (0 === strpos($pathinfo, '/api/distributors')) {
                // api_distributors_get_collection
                if ($pathinfo === '/api/distributors') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_api_distributors_get_collection;
                    }

                    return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'Distributor',  '_route' => 'api_distributors_get_collection',);
                }
                not_api_distributors_get_collection:

                // api_distributors_post_collection
                if ($pathinfo === '/api/distributors') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_api_distributors_post_collection;
                    }

                    return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'Distributor',  '_route' => 'api_distributors_post_collection',);
                }
                not_api_distributors_post_collection:

                // api_distributors_get_item
                if (preg_match('#^/api/distributors/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_api_distributors_get_item;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_distributors_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'Distributor',));
                }
                not_api_distributors_get_item:

                // api_distributors_put_item
                if (preg_match('#^/api/distributors/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_api_distributors_put_item;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_distributors_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'Distributor',));
                }
                not_api_distributors_put_item:

                // api_distributors_delete_item
                if (preg_match('#^/api/distributors/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_api_distributors_delete_item;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_distributors_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'Distributor',));
                }
                not_api_distributors_delete_item:

            }

            if (0 === strpos($pathinfo, '/api/footprints')) {
                // api_footprints_get_collection
                if ($pathinfo === '/api/footprints') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_api_footprints_get_collection;
                    }

                    return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'Footprint',  '_route' => 'api_footprints_get_collection',);
                }
                not_api_footprints_get_collection:

                // api_footprints_post_collection
                if ($pathinfo === '/api/footprints') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_api_footprints_post_collection;
                    }

                    return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'Footprint',  '_route' => 'api_footprints_post_collection',);
                }
                not_api_footprints_post_collection:

                // api_footprints_get_item
                if (preg_match('#^/api/footprints/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_api_footprints_get_item;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_footprints_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'Footprint',));
                }
                not_api_footprints_get_item:

                // api_footprints_put_item
                if (preg_match('#^/api/footprints/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_api_footprints_put_item;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_footprints_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'Footprint',));
                }
                not_api_footprints_put_item:

                // api_footprints_delete_item
                if (preg_match('#^/api/footprints/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_api_footprints_delete_item;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_footprints_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'Footprint',));
                }
                not_api_footprints_delete_item:

            }

            if (0 === strpos($pathinfo, '/api/batch_job')) {
                if (0 === strpos($pathinfo, '/api/batch_jobs')) {
                    // api_batch_jobs_get_collection
                    if ($pathinfo === '/api/batch_jobs') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_batch_jobs_get_collection;
                        }

                        return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'BatchJob',  '_route' => 'api_batch_jobs_get_collection',);
                    }
                    not_api_batch_jobs_get_collection:

                    // api_batch_jobs_post_collection
                    if ($pathinfo === '/api/batch_jobs') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_api_batch_jobs_post_collection;
                        }

                        return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'BatchJob',  '_route' => 'api_batch_jobs_post_collection',);
                    }
                    not_api_batch_jobs_post_collection:

                    // api_batch_jobs_get_item
                    if (preg_match('#^/api/batch_jobs/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_batch_jobs_get_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_batch_jobs_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'BatchJob',));
                    }
                    not_api_batch_jobs_get_item:

                    // api_batch_jobs_put_item
                    if (preg_match('#^/api/batch_jobs/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'PUT') {
                            $allow[] = 'PUT';
                            goto not_api_batch_jobs_put_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_batch_jobs_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'BatchJob',));
                    }
                    not_api_batch_jobs_put_item:

                    // api_batch_jobs_delete_item
                    if (preg_match('#^/api/batch_jobs/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'DELETE') {
                            $allow[] = 'DELETE';
                            goto not_api_batch_jobs_delete_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_batch_jobs_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'BatchJob',));
                    }
                    not_api_batch_jobs_delete_item:

                    // BatchJobExecute
                    if (preg_match('#^/api/batch_jobs/(?P<id>[^/]++)/execute$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'PUT') {
                            $allow[] = 'PUT';
                            goto not_BatchJobExecute;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'BatchJobExecute')), array (  '_controller' => 'partkeepr.batchjob.execute',  '_resource' => 'BatchJob',));
                    }
                    not_BatchJobExecute:

                }

                if (0 === strpos($pathinfo, '/api/batch_job_')) {
                    if (0 === strpos($pathinfo, '/api/batch_job_query_fields')) {
                        // api_batch_job_query_fields_get_collection
                        if ($pathinfo === '/api/batch_job_query_fields') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_batch_job_query_fields_get_collection;
                            }

                            return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'BatchJobQueryField',  '_route' => 'api_batch_job_query_fields_get_collection',);
                        }
                        not_api_batch_job_query_fields_get_collection:

                        // api_batch_job_query_fields_post_collection
                        if ($pathinfo === '/api/batch_job_query_fields') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_api_batch_job_query_fields_post_collection;
                            }

                            return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'BatchJobQueryField',  '_route' => 'api_batch_job_query_fields_post_collection',);
                        }
                        not_api_batch_job_query_fields_post_collection:

                        // api_batch_job_query_fields_get_item
                        if (preg_match('#^/api/batch_job_query_fields/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_batch_job_query_fields_get_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_batch_job_query_fields_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'BatchJobQueryField',));
                        }
                        not_api_batch_job_query_fields_get_item:

                        // api_batch_job_query_fields_put_item
                        if (preg_match('#^/api/batch_job_query_fields/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_api_batch_job_query_fields_put_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_batch_job_query_fields_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'BatchJobQueryField',));
                        }
                        not_api_batch_job_query_fields_put_item:

                        // api_batch_job_query_fields_delete_item
                        if (preg_match('#^/api/batch_job_query_fields/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_api_batch_job_query_fields_delete_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_batch_job_query_fields_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'BatchJobQueryField',));
                        }
                        not_api_batch_job_query_fields_delete_item:

                    }

                    if (0 === strpos($pathinfo, '/api/batch_job_update_fields')) {
                        // api_batch_job_update_fields_get_collection
                        if ($pathinfo === '/api/batch_job_update_fields') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_batch_job_update_fields_get_collection;
                            }

                            return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'BatchJobUpdateField',  '_route' => 'api_batch_job_update_fields_get_collection',);
                        }
                        not_api_batch_job_update_fields_get_collection:

                        // api_batch_job_update_fields_post_collection
                        if ($pathinfo === '/api/batch_job_update_fields') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_api_batch_job_update_fields_post_collection;
                            }

                            return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'BatchJobUpdateField',  '_route' => 'api_batch_job_update_fields_post_collection',);
                        }
                        not_api_batch_job_update_fields_post_collection:

                        // api_batch_job_update_fields_get_item
                        if (preg_match('#^/api/batch_job_update_fields/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_batch_job_update_fields_get_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_batch_job_update_fields_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'BatchJobUpdateField',));
                        }
                        not_api_batch_job_update_fields_get_item:

                        // api_batch_job_update_fields_put_item
                        if (preg_match('#^/api/batch_job_update_fields/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_api_batch_job_update_fields_put_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_batch_job_update_fields_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'BatchJobUpdateField',));
                        }
                        not_api_batch_job_update_fields_put_item:

                        // api_batch_job_update_fields_delete_item
                        if (preg_match('#^/api/batch_job_update_fields/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_api_batch_job_update_fields_delete_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_batch_job_update_fields_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'BatchJobUpdateField',));
                        }
                        not_api_batch_job_update_fields_delete_item:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/api/footprint_')) {
                if (0 === strpos($pathinfo, '/api/footprint_images')) {
                    // api_footprint_images_get_collection
                    if ($pathinfo === '/api/footprint_images') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_footprint_images_get_collection;
                        }

                        return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'FootprintImage',  '_route' => 'api_footprint_images_get_collection',);
                    }
                    not_api_footprint_images_get_collection:

                    // api_footprint_images_post_collection
                    if ($pathinfo === '/api/footprint_images') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_api_footprint_images_post_collection;
                        }

                        return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'FootprintImage',  '_route' => 'api_footprint_images_post_collection',);
                    }
                    not_api_footprint_images_post_collection:

                    // api_footprint_images_get_item
                    if (preg_match('#^/api/footprint_images/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_footprint_images_get_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_footprint_images_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'FootprintImage',));
                    }
                    not_api_footprint_images_get_item:

                    // FootprintImageGetImage
                    if (preg_match('#^/api/footprint_images/(?P<id>[^/]++)/getImage$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_FootprintImageGetImage;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'FootprintImageGetImage')), array (  '_controller' => 'PartKeepr\\FootprintBundle\\Controller\\FootprintImageController::getImageAction',  '_resource' => 'FootprintImage',));
                    }
                    not_FootprintImageGetImage:

                }

                if (0 === strpos($pathinfo, '/api/footprint_attachments')) {
                    // api_footprint_attachments_get_collection
                    if ($pathinfo === '/api/footprint_attachments') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_footprint_attachments_get_collection;
                        }

                        return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'FootprintAttachment',  '_route' => 'api_footprint_attachments_get_collection',);
                    }
                    not_api_footprint_attachments_get_collection:

                    // api_footprint_attachments_post_collection
                    if ($pathinfo === '/api/footprint_attachments') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_api_footprint_attachments_post_collection;
                        }

                        return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'FootprintAttachment',  '_route' => 'api_footprint_attachments_post_collection',);
                    }
                    not_api_footprint_attachments_post_collection:

                    // api_footprint_attachments_get_item
                    if (preg_match('#^/api/footprint_attachments/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_footprint_attachments_get_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_footprint_attachments_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'FootprintAttachment',));
                    }
                    not_api_footprint_attachments_get_item:

                    // FootprintAttachmentGet
                    if (preg_match('#^/api/footprint_attachments/(?P<id>[^/]++)/getFile$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_FootprintAttachmentGet;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'FootprintAttachmentGet')), array (  '_controller' => 'PartKeepr\\FootprintBundle\\Controller\\FootprintAttachmentController::getFileAction',  '_resource' => 'FootprintAttachment',));
                    }
                    not_FootprintAttachmentGet:

                    // FootprintAttachmentMimeTypeIcon
                    if (preg_match('#^/api/footprint_attachments/(?P<id>[^/]++)/getMimeTypeIcon$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_FootprintAttachmentMimeTypeIcon;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'FootprintAttachmentMimeTypeIcon')), array (  '_controller' => 'PartKeepr\\FootprintBundle\\Controller\\FootprintAttachmentController::getMimeTypeIconAction',  '_resource' => 'FootprintAttachment',));
                    }
                    not_FootprintAttachmentMimeTypeIcon:

                }

                if (0 === strpos($pathinfo, '/api/footprint_categories')) {
                    // api_footprint_categories_get_collection
                    if ($pathinfo === '/api/footprint_categories') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_footprint_categories_get_collection;
                        }

                        return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'FootprintCategory',  '_route' => 'api_footprint_categories_get_collection',);
                    }
                    not_api_footprint_categories_get_collection:

                    // PartKeeprFootprintCategoryGetRootNode
                    if ($pathinfo === '/api/footprint_categories/getExtJSRootNode') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_PartKeeprFootprintCategoryGetRootNode;
                        }

                        return array (  '_controller' => 'partkeepr.category.get_root_node',  '_resource' => 'FootprintCategory',  '_route' => 'PartKeeprFootprintCategoryGetRootNode',);
                    }
                    not_PartKeeprFootprintCategoryGetRootNode:

                    // api_footprint_categories_post_collection
                    if ($pathinfo === '/api/footprint_categories') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_api_footprint_categories_post_collection;
                        }

                        return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'FootprintCategory',  '_route' => 'api_footprint_categories_post_collection',);
                    }
                    not_api_footprint_categories_post_collection:

                    // api_footprint_categories_get_item
                    if (preg_match('#^/api/footprint_categories/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_footprint_categories_get_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_footprint_categories_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'FootprintCategory',));
                    }
                    not_api_footprint_categories_get_item:

                    // api_footprint_categories_put_item
                    if (preg_match('#^/api/footprint_categories/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'PUT') {
                            $allow[] = 'PUT';
                            goto not_api_footprint_categories_put_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_footprint_categories_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'FootprintCategory',));
                    }
                    not_api_footprint_categories_put_item:

                    // api_footprint_categories_delete_item
                    if (preg_match('#^/api/footprint_categories/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'DELETE') {
                            $allow[] = 'DELETE';
                            goto not_api_footprint_categories_delete_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_footprint_categories_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'FootprintCategory',));
                    }
                    not_api_footprint_categories_delete_item:

                    // FootprintCategoryMove
                    if (preg_match('#^/api/footprint_categories/(?P<id>[^/]++)/move$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'PUT') {
                            $allow[] = 'PUT';
                            goto not_FootprintCategoryMove;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'FootprintCategoryMove')), array (  '_controller' => 'partkeepr.category.move',  '_resource' => 'FootprintCategory',));
                    }
                    not_FootprintCategoryMove:

                }

            }

            if (0 === strpos($pathinfo, '/api/import_presets')) {
                // api_import_presets_get_collection
                if ($pathinfo === '/api/import_presets') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_api_import_presets_get_collection;
                    }

                    return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'ImportPreset',  '_route' => 'api_import_presets_get_collection',);
                }
                not_api_import_presets_get_collection:

                // api_import_presets_post_collection
                if ($pathinfo === '/api/import_presets') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_api_import_presets_post_collection;
                    }

                    return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'ImportPreset',  '_route' => 'api_import_presets_post_collection',);
                }
                not_api_import_presets_post_collection:

                // api_import_presets_get_item
                if (preg_match('#^/api/import_presets/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_api_import_presets_get_item;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_import_presets_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'ImportPreset',));
                }
                not_api_import_presets_get_item:

                // api_import_presets_put_item
                if (preg_match('#^/api/import_presets/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_api_import_presets_put_item;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_import_presets_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'ImportPreset',));
                }
                not_api_import_presets_put_item:

                // api_import_presets_delete_item
                if (preg_match('#^/api/import_presets/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_api_import_presets_delete_item;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_import_presets_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'ImportPreset',));
                }
                not_api_import_presets_delete_item:

            }

            if (0 === strpos($pathinfo, '/api/part')) {
                if (0 === strpos($pathinfo, '/api/parts')) {
                    // PartsGet
                    if ($pathinfo === '/api/parts') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_PartsGet;
                        }

                        return array (  '_controller' => 'partkeepr.parts.collection_get',  '_resource' => 'Part',  '_route' => 'PartsGet',);
                    }
                    not_PartsGet:

                    // PartPost
                    if ($pathinfo === '/api/parts') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_PartPost;
                        }

                        return array (  '_controller' => 'partkeepr.part.post',  '_resource' => 'Part',  '_route' => 'PartPost',);
                    }
                    not_PartPost:

                    // api_parts_get_item
                    if (preg_match('#^/api/parts/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_parts_get_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_parts_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'Part',));
                    }
                    not_api_parts_get_item:

                    // PartPut
                    if (preg_match('#^/api/parts/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'PUT') {
                            $allow[] = 'PUT';
                            goto not_PartPut;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'PartPut')), array (  '_controller' => 'partkeepr.part.put',  '_resource' => 'Part',));
                    }
                    not_PartPut:

                    // api_parts_delete_item
                    if (preg_match('#^/api/parts/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'DELETE') {
                            $allow[] = 'DELETE';
                            goto not_api_parts_delete_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_parts_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'Part',));
                    }
                    not_api_parts_delete_item:

                    // PartAddStock
                    if (preg_match('#^/api/parts/(?P<id>[^/]++)/addStock$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'PUT') {
                            $allow[] = 'PUT';
                            goto not_PartAddStock;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'PartAddStock')), array (  '_controller' => 'partkeepr.part.add_stock',  '_resource' => 'Part',));
                    }
                    not_PartAddStock:

                    // PartRemoveStock
                    if (preg_match('#^/api/parts/(?P<id>[^/]++)/removeStock$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'PUT') {
                            $allow[] = 'PUT';
                            goto not_PartRemoveStock;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'PartRemoveStock')), array (  '_controller' => 'partkeepr.part.remove_stock',  '_resource' => 'Part',));
                    }
                    not_PartRemoveStock:

                    // PartSetStock
                    if (preg_match('#^/api/parts/(?P<id>[^/]++)/setStock$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'PUT') {
                            $allow[] = 'PUT';
                            goto not_PartSetStock;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'PartSetStock')), array (  '_controller' => 'partkeepr.part.set_stock',  '_resource' => 'Part',));
                    }
                    not_PartSetStock:

                }

                if (0 === strpos($pathinfo, '/api/part_')) {
                    if (0 === strpos($pathinfo, '/api/part_attachments')) {
                        // api_part_attachments_get_collection
                        if ($pathinfo === '/api/part_attachments') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_part_attachments_get_collection;
                            }

                            return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'PartAttachment',  '_route' => 'api_part_attachments_get_collection',);
                        }
                        not_api_part_attachments_get_collection:

                        // api_part_attachments_post_collection
                        if ($pathinfo === '/api/part_attachments') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_api_part_attachments_post_collection;
                            }

                            return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'PartAttachment',  '_route' => 'api_part_attachments_post_collection',);
                        }
                        not_api_part_attachments_post_collection:

                        // api_part_attachments_get_item
                        if (preg_match('#^/api/part_attachments/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_part_attachments_get_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_part_attachments_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'PartAttachment',));
                        }
                        not_api_part_attachments_get_item:

                        // PartAttachmentGet
                        if (preg_match('#^/api/part_attachments/(?P<id>[^/]++)/getFile$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_PartAttachmentGet;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'PartAttachmentGet')), array (  '_controller' => 'PartKeepr\\PartBundle\\Controller\\PartAttachmentController::getFileAction',  '_resource' => 'PartAttachment',));
                        }
                        not_PartAttachmentGet:

                        // PartAttachmentMimeTypeIcon
                        if (preg_match('#^/api/part_attachments/(?P<id>[^/]++)/getMimeTypeIcon$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_PartAttachmentMimeTypeIcon;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'PartAttachmentMimeTypeIcon')), array (  '_controller' => 'PartKeepr\\PartBundle\\Controller\\PartAttachmentController::getMimeTypeIconAction',  '_resource' => 'PartAttachment',));
                        }
                        not_PartAttachmentMimeTypeIcon:

                        // PartAttachmentGetImage
                        if (preg_match('#^/api/part_attachments/(?P<id>[^/]++)/getImage$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_PartAttachmentGetImage;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'PartAttachmentGetImage')), array (  '_controller' => 'PartKeepr\\PartBundle\\Controller\\PartAttachmentController::getImageAction',  '_resource' => 'PartAttachment',));
                        }
                        not_PartAttachmentGetImage:

                    }

                    if (0 === strpos($pathinfo, '/api/part_categories')) {
                        // api_part_categories_get_collection
                        if ($pathinfo === '/api/part_categories') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_part_categories_get_collection;
                            }

                            return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'PartCategory',  '_route' => 'api_part_categories_get_collection',);
                        }
                        not_api_part_categories_get_collection:

                        // PartKeeprPartCategoryGetRootNode
                        if ($pathinfo === '/api/part_categories/getExtJSRootNode') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_PartKeeprPartCategoryGetRootNode;
                            }

                            return array (  '_controller' => 'partkeepr.category.get_root_node',  '_resource' => 'PartCategory',  '_route' => 'PartKeeprPartCategoryGetRootNode',);
                        }
                        not_PartKeeprPartCategoryGetRootNode:

                        // api_part_categories_post_collection
                        if ($pathinfo === '/api/part_categories') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_api_part_categories_post_collection;
                            }

                            return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'PartCategory',  '_route' => 'api_part_categories_post_collection',);
                        }
                        not_api_part_categories_post_collection:

                        // api_part_categories_get_item
                        if (preg_match('#^/api/part_categories/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_part_categories_get_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_part_categories_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'PartCategory',));
                        }
                        not_api_part_categories_get_item:

                        // api_part_categories_put_item
                        if (preg_match('#^/api/part_categories/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_api_part_categories_put_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_part_categories_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'PartCategory',));
                        }
                        not_api_part_categories_put_item:

                        // api_part_categories_delete_item
                        if (preg_match('#^/api/part_categories/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_api_part_categories_delete_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_part_categories_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'PartCategory',));
                        }
                        not_api_part_categories_delete_item:

                        // PartKeeprPartCategoryMove
                        if (preg_match('#^/api/part_categories/(?P<id>[^/]++)/move$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_PartKeeprPartCategoryMove;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'PartKeeprPartCategoryMove')), array (  '_controller' => 'partkeepr.category.move',  '_resource' => 'PartCategory',));
                        }
                        not_PartKeeprPartCategoryMove:

                    }

                    if (0 === strpos($pathinfo, '/api/part_distributors')) {
                        // api_part_distributors_get_collection
                        if ($pathinfo === '/api/part_distributors') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_part_distributors_get_collection;
                            }

                            return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'PartDistributor',  '_route' => 'api_part_distributors_get_collection',);
                        }
                        not_api_part_distributors_get_collection:

                        // api_part_distributors_post_collection
                        if ($pathinfo === '/api/part_distributors') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_api_part_distributors_post_collection;
                            }

                            return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'PartDistributor',  '_route' => 'api_part_distributors_post_collection',);
                        }
                        not_api_part_distributors_post_collection:

                        // api_part_distributors_get_item
                        if (preg_match('#^/api/part_distributors/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_part_distributors_get_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_part_distributors_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'PartDistributor',));
                        }
                        not_api_part_distributors_get_item:

                        // api_part_distributors_put_item
                        if (preg_match('#^/api/part_distributors/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_api_part_distributors_put_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_part_distributors_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'PartDistributor',));
                        }
                        not_api_part_distributors_put_item:

                        // api_part_distributors_delete_item
                        if (preg_match('#^/api/part_distributors/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_api_part_distributors_delete_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_part_distributors_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'PartDistributor',));
                        }
                        not_api_part_distributors_delete_item:

                    }

                    if (0 === strpos($pathinfo, '/api/part_manufacturers')) {
                        // api_part_manufacturers_get_collection
                        if ($pathinfo === '/api/part_manufacturers') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_part_manufacturers_get_collection;
                            }

                            return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'PartManufacturer',  '_route' => 'api_part_manufacturers_get_collection',);
                        }
                        not_api_part_manufacturers_get_collection:

                        // api_part_manufacturers_post_collection
                        if ($pathinfo === '/api/part_manufacturers') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_api_part_manufacturers_post_collection;
                            }

                            return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'PartManufacturer',  '_route' => 'api_part_manufacturers_post_collection',);
                        }
                        not_api_part_manufacturers_post_collection:

                        // api_part_manufacturers_get_item
                        if (preg_match('#^/api/part_manufacturers/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_part_manufacturers_get_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_part_manufacturers_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'PartManufacturer',));
                        }
                        not_api_part_manufacturers_get_item:

                        // api_part_manufacturers_put_item
                        if (preg_match('#^/api/part_manufacturers/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_api_part_manufacturers_put_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_part_manufacturers_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'PartManufacturer',));
                        }
                        not_api_part_manufacturers_put_item:

                        // api_part_manufacturers_delete_item
                        if (preg_match('#^/api/part_manufacturers/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_api_part_manufacturers_delete_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_part_manufacturers_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'PartManufacturer',));
                        }
                        not_api_part_manufacturers_delete_item:

                    }

                    if (0 === strpos($pathinfo, '/api/part_parameters')) {
                        // api_part_parameters_get_collection
                        if ($pathinfo === '/api/part_parameters') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_part_parameters_get_collection;
                            }

                            return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'PartParameter',  '_route' => 'api_part_parameters_get_collection',);
                        }
                        not_api_part_parameters_get_collection:

                        // api_part_parameters_post_collection
                        if ($pathinfo === '/api/part_parameters') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_api_part_parameters_post_collection;
                            }

                            return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'PartParameter',  '_route' => 'api_part_parameters_post_collection',);
                        }
                        not_api_part_parameters_post_collection:

                        // api_part_parameters_get_item
                        if (preg_match('#^/api/part_parameters/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_part_parameters_get_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_part_parameters_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'PartParameter',));
                        }
                        not_api_part_parameters_get_item:

                        // api_part_parameters_put_item
                        if (preg_match('#^/api/part_parameters/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_api_part_parameters_put_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_part_parameters_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'PartParameter',));
                        }
                        not_api_part_parameters_put_item:

                        // api_part_parameters_delete_item
                        if (preg_match('#^/api/part_parameters/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_api_part_parameters_delete_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_part_parameters_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'PartParameter',));
                        }
                        not_api_part_parameters_delete_item:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/api/m')) {
                if (0 === strpos($pathinfo, '/api/meta_part_parameter_criterias')) {
                    // api_meta_part_parameter_criterias_get_collection
                    if ($pathinfo === '/api/meta_part_parameter_criterias') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_meta_part_parameter_criterias_get_collection;
                        }

                        return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'MetaPartParameterCriteria',  '_route' => 'api_meta_part_parameter_criterias_get_collection',);
                    }
                    not_api_meta_part_parameter_criterias_get_collection:

                    // api_meta_part_parameter_criterias_post_collection
                    if ($pathinfo === '/api/meta_part_parameter_criterias') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_api_meta_part_parameter_criterias_post_collection;
                        }

                        return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'MetaPartParameterCriteria',  '_route' => 'api_meta_part_parameter_criterias_post_collection',);
                    }
                    not_api_meta_part_parameter_criterias_post_collection:

                    // api_meta_part_parameter_criterias_get_item
                    if (preg_match('#^/api/meta_part_parameter_criterias/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_meta_part_parameter_criterias_get_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_meta_part_parameter_criterias_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'MetaPartParameterCriteria',));
                    }
                    not_api_meta_part_parameter_criterias_get_item:

                    // api_meta_part_parameter_criterias_put_item
                    if (preg_match('#^/api/meta_part_parameter_criterias/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'PUT') {
                            $allow[] = 'PUT';
                            goto not_api_meta_part_parameter_criterias_put_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_meta_part_parameter_criterias_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'MetaPartParameterCriteria',));
                    }
                    not_api_meta_part_parameter_criterias_put_item:

                    // api_meta_part_parameter_criterias_delete_item
                    if (preg_match('#^/api/meta_part_parameter_criterias/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'DELETE') {
                            $allow[] = 'DELETE';
                            goto not_api_meta_part_parameter_criterias_delete_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_meta_part_parameter_criterias_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'MetaPartParameterCriteria',));
                    }
                    not_api_meta_part_parameter_criterias_delete_item:

                }

                if (0 === strpos($pathinfo, '/api/manufacturer')) {
                    if (0 === strpos($pathinfo, '/api/manufacturers')) {
                        // api_manufacturers_get_collection
                        if ($pathinfo === '/api/manufacturers') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_manufacturers_get_collection;
                            }

                            return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'Manufacturer',  '_route' => 'api_manufacturers_get_collection',);
                        }
                        not_api_manufacturers_get_collection:

                        // api_manufacturers_post_collection
                        if ($pathinfo === '/api/manufacturers') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_api_manufacturers_post_collection;
                            }

                            return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'Manufacturer',  '_route' => 'api_manufacturers_post_collection',);
                        }
                        not_api_manufacturers_post_collection:

                        // api_manufacturers_get_item
                        if (preg_match('#^/api/manufacturers/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_manufacturers_get_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_manufacturers_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'Manufacturer',));
                        }
                        not_api_manufacturers_get_item:

                        // api_manufacturers_put_item
                        if (preg_match('#^/api/manufacturers/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_api_manufacturers_put_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_manufacturers_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'Manufacturer',));
                        }
                        not_api_manufacturers_put_item:

                        // api_manufacturers_delete_item
                        if (preg_match('#^/api/manufacturers/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_api_manufacturers_delete_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_manufacturers_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'Manufacturer',));
                        }
                        not_api_manufacturers_delete_item:

                    }

                    if (0 === strpos($pathinfo, '/api/manufacturer_i_c_logos')) {
                        // api_manufacturer_i_c_logos_get_collection
                        if ($pathinfo === '/api/manufacturer_i_c_logos') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_manufacturer_i_c_logos_get_collection;
                            }

                            return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'ManufacturerICLogo',  '_route' => 'api_manufacturer_i_c_logos_get_collection',);
                        }
                        not_api_manufacturer_i_c_logos_get_collection:

                        // api_manufacturer_i_c_logos_post_collection
                        if ($pathinfo === '/api/manufacturer_i_c_logos') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_api_manufacturer_i_c_logos_post_collection;
                            }

                            return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'ManufacturerICLogo',  '_route' => 'api_manufacturer_i_c_logos_post_collection',);
                        }
                        not_api_manufacturer_i_c_logos_post_collection:

                        // api_manufacturer_i_c_logos_get_item
                        if (preg_match('#^/api/manufacturer_i_c_logos/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_manufacturer_i_c_logos_get_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_manufacturer_i_c_logos_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'ManufacturerICLogo',));
                        }
                        not_api_manufacturer_i_c_logos_get_item:

                        // api_manufacturer_i_c_logos_put_item
                        if (preg_match('#^/api/manufacturer_i_c_logos/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_api_manufacturer_i_c_logos_put_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_manufacturer_i_c_logos_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'ManufacturerICLogo',));
                        }
                        not_api_manufacturer_i_c_logos_put_item:

                        // ManufacturerIcLogoGetImage
                        if (preg_match('#^/api/manufacturer_i_c_logos/(?P<id>[^/]++)/getImage$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_ManufacturerIcLogoGetImage;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'ManufacturerIcLogoGetImage')), array (  '_controller' => 'PartKeepr\\ManufacturerBundle\\Controller\\ManufacturerIcLogoController::getImageAction',  '_resource' => 'ManufacturerICLogo',));
                        }
                        not_ManufacturerIcLogoGetImage:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/api/part_measurement_units')) {
                // api_part_measurement_units_get_collection
                if ($pathinfo === '/api/part_measurement_units') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_api_part_measurement_units_get_collection;
                    }

                    return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'PartMeasurementUnit',  '_route' => 'api_part_measurement_units_get_collection',);
                }
                not_api_part_measurement_units_get_collection:

                // api_part_measurement_units_post_collection
                if ($pathinfo === '/api/part_measurement_units') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_api_part_measurement_units_post_collection;
                    }

                    return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'PartMeasurementUnit',  '_route' => 'api_part_measurement_units_post_collection',);
                }
                not_api_part_measurement_units_post_collection:

                // PartMeasurementUnitSetDefault
                if (preg_match('#^/api/part_measurement_units/(?P<id>[^/]++)/setDefault$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_PartMeasurementUnitSetDefault;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'PartMeasurementUnitSetDefault')), array (  '_controller' => 'partkeepr.part_measurement_unit.set_default',  '_resource' => 'PartMeasurementUnit',));
                }
                not_PartMeasurementUnitSetDefault:

                // api_part_measurement_units_get_item
                if (preg_match('#^/api/part_measurement_units/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_api_part_measurement_units_get_item;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_part_measurement_units_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'PartMeasurementUnit',));
                }
                not_api_part_measurement_units_get_item:

                // api_part_measurement_units_put_item
                if (preg_match('#^/api/part_measurement_units/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_api_part_measurement_units_put_item;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_part_measurement_units_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'PartMeasurementUnit',));
                }
                not_api_part_measurement_units_put_item:

            }

            if (0 === strpos($pathinfo, '/api/units')) {
                // api_units_get_collection
                if ($pathinfo === '/api/units') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_api_units_get_collection;
                    }

                    return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'Unit',  '_route' => 'api_units_get_collection',);
                }
                not_api_units_get_collection:

                // api_units_post_collection
                if ($pathinfo === '/api/units') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_api_units_post_collection;
                    }

                    return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'Unit',  '_route' => 'api_units_post_collection',);
                }
                not_api_units_post_collection:

                // api_units_get_item
                if (preg_match('#^/api/units/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_api_units_get_item;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_units_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'Unit',));
                }
                not_api_units_get_item:

                // api_units_put_item
                if (preg_match('#^/api/units/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_api_units_put_item;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_units_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'Unit',));
                }
                not_api_units_put_item:

                // api_units_delete_item
                if (preg_match('#^/api/units/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_api_units_delete_item;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_units_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'Unit',));
                }
                not_api_units_delete_item:

            }

            if (0 === strpos($pathinfo, '/api/s')) {
                if (0 === strpos($pathinfo, '/api/si_prefixes')) {
                    // api_si_prefixes_get_collection
                    if ($pathinfo === '/api/si_prefixes') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_si_prefixes_get_collection;
                        }

                        return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'SiPrefix',  '_route' => 'api_si_prefixes_get_collection',);
                    }
                    not_api_si_prefixes_get_collection:

                    // api_si_prefixes_post_collection
                    if ($pathinfo === '/api/si_prefixes') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_api_si_prefixes_post_collection;
                        }

                        return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'SiPrefix',  '_route' => 'api_si_prefixes_post_collection',);
                    }
                    not_api_si_prefixes_post_collection:

                    // api_si_prefixes_get_item
                    if (preg_match('#^/api/si_prefixes/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_si_prefixes_get_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_si_prefixes_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'SiPrefix',));
                    }
                    not_api_si_prefixes_get_item:

                    // api_si_prefixes_put_item
                    if (preg_match('#^/api/si_prefixes/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'PUT') {
                            $allow[] = 'PUT';
                            goto not_api_si_prefixes_put_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_si_prefixes_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'SiPrefix',));
                    }
                    not_api_si_prefixes_put_item:

                    // api_si_prefixes_delete_item
                    if (preg_match('#^/api/si_prefixes/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'DELETE') {
                            $allow[] = 'DELETE';
                            goto not_api_si_prefixes_delete_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_si_prefixes_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'SiPrefix',));
                    }
                    not_api_si_prefixes_delete_item:

                }

                if (0 === strpos($pathinfo, '/api/sto')) {
                    if (0 === strpos($pathinfo, '/api/storage_locations')) {
                        // api_storage_locations_get_collection
                        if ($pathinfo === '/api/storage_locations') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_storage_locations_get_collection;
                            }

                            return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'StorageLocation',  '_route' => 'api_storage_locations_get_collection',);
                        }
                        not_api_storage_locations_get_collection:

                        // api_storage_locations_post_collection
                        if ($pathinfo === '/api/storage_locations') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_api_storage_locations_post_collection;
                            }

                            return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'StorageLocation',  '_route' => 'api_storage_locations_post_collection',);
                        }
                        not_api_storage_locations_post_collection:

                        // api_storage_locations_get_item
                        if (preg_match('#^/api/storage_locations/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_storage_locations_get_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_storage_locations_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'StorageLocation',));
                        }
                        not_api_storage_locations_get_item:

                        // api_storage_locations_put_item
                        if (preg_match('#^/api/storage_locations/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_api_storage_locations_put_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_storage_locations_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'StorageLocation',));
                        }
                        not_api_storage_locations_put_item:

                        // api_storage_locations_delete_item
                        if (preg_match('#^/api/storage_locations/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_api_storage_locations_delete_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_storage_locations_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'StorageLocation',));
                        }
                        not_api_storage_locations_delete_item:

                    }

                    if (0 === strpos($pathinfo, '/api/stock_entries')) {
                        // api_stock_entries_get_collection
                        if ($pathinfo === '/api/stock_entries') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_stock_entries_get_collection;
                            }

                            return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'StockEntry',  '_route' => 'api_stock_entries_get_collection',);
                        }
                        not_api_stock_entries_get_collection:

                        // api_stock_entries_post_collection
                        if ($pathinfo === '/api/stock_entries') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_api_stock_entries_post_collection;
                            }

                            return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'StockEntry',  '_route' => 'api_stock_entries_post_collection',);
                        }
                        not_api_stock_entries_post_collection:

                        // api_stock_entries_get_item
                        if (preg_match('#^/api/stock_entries/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_stock_entries_get_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_stock_entries_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'StockEntry',));
                        }
                        not_api_stock_entries_get_item:

                        // api_stock_entries_put_item
                        if (preg_match('#^/api/stock_entries/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_api_stock_entries_put_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_stock_entries_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'StockEntry',));
                        }
                        not_api_stock_entries_put_item:

                        // api_stock_entries_delete_item
                        if (preg_match('#^/api/stock_entries/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_api_stock_entries_delete_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_stock_entries_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'StockEntry',));
                        }
                        not_api_stock_entries_delete_item:

                    }

                    if (0 === strpos($pathinfo, '/api/storage_location_')) {
                        if (0 === strpos($pathinfo, '/api/storage_location_categories')) {
                            // api_storage_location_categories_get_collection
                            if ($pathinfo === '/api/storage_location_categories') {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_api_storage_location_categories_get_collection;
                                }

                                return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'StorageLocationCategory',  '_route' => 'api_storage_location_categories_get_collection',);
                            }
                            not_api_storage_location_categories_get_collection:

                            // StorageLocationCategoryGetRoot
                            if ($pathinfo === '/api/storage_location_categories/getExtJSRootNode') {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_StorageLocationCategoryGetRoot;
                                }

                                return array (  '_controller' => 'partkeepr.category.get_root_node',  '_resource' => 'StorageLocationCategory',  '_route' => 'StorageLocationCategoryGetRoot',);
                            }
                            not_StorageLocationCategoryGetRoot:

                            // api_storage_location_categories_post_collection
                            if ($pathinfo === '/api/storage_location_categories') {
                                if ($this->context->getMethod() != 'POST') {
                                    $allow[] = 'POST';
                                    goto not_api_storage_location_categories_post_collection;
                                }

                                return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'StorageLocationCategory',  '_route' => 'api_storage_location_categories_post_collection',);
                            }
                            not_api_storage_location_categories_post_collection:

                            // api_storage_location_categories_get_item
                            if (preg_match('#^/api/storage_location_categories/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_api_storage_location_categories_get_item;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_storage_location_categories_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'StorageLocationCategory',));
                            }
                            not_api_storage_location_categories_get_item:

                            // api_storage_location_categories_put_item
                            if (preg_match('#^/api/storage_location_categories/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                if ($this->context->getMethod() != 'PUT') {
                                    $allow[] = 'PUT';
                                    goto not_api_storage_location_categories_put_item;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_storage_location_categories_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'StorageLocationCategory',));
                            }
                            not_api_storage_location_categories_put_item:

                            // api_storage_location_categories_delete_item
                            if (preg_match('#^/api/storage_location_categories/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                if ($this->context->getMethod() != 'DELETE') {
                                    $allow[] = 'DELETE';
                                    goto not_api_storage_location_categories_delete_item;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_storage_location_categories_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'StorageLocationCategory',));
                            }
                            not_api_storage_location_categories_delete_item:

                            // StorageLocationCategoryMove
                            if (preg_match('#^/api/storage_location_categories/(?P<id>[^/]++)/move$#s', $pathinfo, $matches)) {
                                if ($this->context->getMethod() != 'PUT') {
                                    $allow[] = 'PUT';
                                    goto not_StorageLocationCategoryMove;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'StorageLocationCategoryMove')), array (  '_controller' => 'partkeepr.category.move',  '_resource' => 'StorageLocationCategory',));
                            }
                            not_StorageLocationCategoryMove:

                        }

                        if (0 === strpos($pathinfo, '/api/storage_location_images')) {
                            // api_storage_location_images_get_collection
                            if ($pathinfo === '/api/storage_location_images') {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_api_storage_location_images_get_collection;
                                }

                                return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'StorageLocationImage',  '_route' => 'api_storage_location_images_get_collection',);
                            }
                            not_api_storage_location_images_get_collection:

                            // api_storage_location_images_post_collection
                            if ($pathinfo === '/api/storage_location_images') {
                                if ($this->context->getMethod() != 'POST') {
                                    $allow[] = 'POST';
                                    goto not_api_storage_location_images_post_collection;
                                }

                                return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'StorageLocationImage',  '_route' => 'api_storage_location_images_post_collection',);
                            }
                            not_api_storage_location_images_post_collection:

                            // api_storage_location_images_get_item
                            if (preg_match('#^/api/storage_location_images/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_api_storage_location_images_get_item;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_storage_location_images_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'StorageLocationImage',));
                            }
                            not_api_storage_location_images_get_item:

                            // StorageLocationGetImage
                            if (preg_match('#^/api/storage_location_images/(?P<id>[^/]++)/getImage$#s', $pathinfo, $matches)) {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_StorageLocationGetImage;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'StorageLocationGetImage')), array (  '_controller' => 'PartKeepr\\StorageLocationBundle\\Controller\\StorageLocationImageController::getImageAction',  '_resource' => 'StorageLocationImage',));
                            }
                            not_StorageLocationGetImage:

                        }

                    }

                }

            }

            if (0 === strpos($pathinfo, '/api/t')) {
                if (0 === strpos($pathinfo, '/api/temp_')) {
                    if (0 === strpos($pathinfo, '/api/temp_images')) {
                        // TemporaryImageUpload
                        if ($pathinfo === '/api/temp_images/upload') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_TemporaryImageUpload;
                            }

                            return array (  '_controller' => 'PartKeepr\\ImageBundle\\Controller\\TemporaryImageController::uploadAction',  '_resource' => 'TempImage',  '_route' => 'TemporaryImageUpload',);
                        }
                        not_TemporaryImageUpload:

                        // TemporaryImageUploadWebcam
                        if ($pathinfo === '/api/temp_images/webcamUpload') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_TemporaryImageUploadWebcam;
                            }

                            return array (  '_controller' => 'PartKeepr\\ImageBundle\\Controller\\TemporaryImageController::webcamUploadAction',  '_resource' => 'TempImage',  '_route' => 'TemporaryImageUploadWebcam',);
                        }
                        not_TemporaryImageUploadWebcam:

                        // api_temp_images_get_item
                        if (preg_match('#^/api/temp_images/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_temp_images_get_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_temp_images_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'TempImage',));
                        }
                        not_api_temp_images_get_item:

                        // TemporaryImageGet
                        if (preg_match('#^/api/temp_images/(?P<id>[^/]++)/getImage$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_TemporaryImageGet;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'TemporaryImageGet')), array (  '_controller' => 'PartKeepr\\ImageBundle\\Controller\\TemporaryImageController::getImageAction',  '_resource' => 'TempImage',));
                        }
                        not_TemporaryImageGet:

                    }

                    if (0 === strpos($pathinfo, '/api/temp_uploaded_files')) {
                        // TemporaryFileUpload
                        if ($pathinfo === '/api/temp_uploaded_files/upload') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_TemporaryFileUpload;
                            }

                            return array (  '_controller' => 'PartKeepr\\UploadedFileBundle\\Controller\\TemporaryFileController::uploadAction',  '_resource' => 'TempUploadedFile',  '_route' => 'TemporaryFileUpload',);
                        }
                        not_TemporaryFileUpload:

                        // api_temp_uploaded_files_get_item
                        if (preg_match('#^/api/temp_uploaded_files/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_temp_uploaded_files_get_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_temp_uploaded_files_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'TempUploadedFile',));
                        }
                        not_api_temp_uploaded_files_get_item:

                        // TemporaryFileGet
                        if (preg_match('#^/api/temp_uploaded_files/(?P<id>[^/]++)/getFile$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_TemporaryFileGet;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'TemporaryFileGet')), array (  '_controller' => 'PartKeepr\\UploadedFileBundle\\Controller\\TemporaryFileController::getFileAction',  '_resource' => 'TempUploadedFile',));
                        }
                        not_TemporaryFileGet:

                        // TemporaryFileGetMimeTypeIcon
                        if (preg_match('#^/api/temp_uploaded_files/(?P<id>[^/]++)/getMimeTypeIcon$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_TemporaryFileGetMimeTypeIcon;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'TemporaryFileGetMimeTypeIcon')), array (  '_controller' => 'PartKeepr\\UploadedFileBundle\\Controller\\TemporaryFileController::getMimeTypeIconAction',  '_resource' => 'TempUploadedFile',));
                        }
                        not_TemporaryFileGetMimeTypeIcon:

                        // TemporaryFileUploadWebcam
                        if ($pathinfo === '/api/temp_uploaded_files/webcamUpload') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_TemporaryFileUploadWebcam;
                            }

                            return array (  '_controller' => 'PartKeepr\\UploadedFileBundle\\Controller\\TemporaryFileController::webcamUploadAction',  '_resource' => 'TempUploadedFile',  '_route' => 'TemporaryFileUploadWebcam',);
                        }
                        not_TemporaryFileUploadWebcam:

                    }

                }

                if (0 === strpos($pathinfo, '/api/tip_of_the_day')) {
                    if (0 === strpos($pathinfo, '/api/tip_of_the_days')) {
                        // api_tip_of_the_days_get_collection
                        if ($pathinfo === '/api/tip_of_the_days') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_tip_of_the_days_get_collection;
                            }

                            return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'TipOfTheDay',  '_route' => 'api_tip_of_the_days_get_collection',);
                        }
                        not_api_tip_of_the_days_get_collection:

                        // api_tip_of_the_days_post_collection
                        if ($pathinfo === '/api/tip_of_the_days') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_api_tip_of_the_days_post_collection;
                            }

                            return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'TipOfTheDay',  '_route' => 'api_tip_of_the_days_post_collection',);
                        }
                        not_api_tip_of_the_days_post_collection:

                        // TipMarkAllUnrad
                        if ($pathinfo === '/api/tip_of_the_days/markAllTipsAsUnread') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_TipMarkAllUnrad;
                            }

                            return array (  '_controller' => 'partkeepr.tip_of_the_day.mark_all_unread',  '_resource' => 'TipOfTheDay',  '_route' => 'TipMarkAllUnrad',);
                        }
                        not_TipMarkAllUnrad:

                        // api_tip_of_the_days_get_item
                        if (preg_match('#^/api/tip_of_the_days/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_tip_of_the_days_get_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_tip_of_the_days_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'TipOfTheDay',));
                        }
                        not_api_tip_of_the_days_get_item:

                        // TipMarkRead
                        if (preg_match('#^/api/tip_of_the_days/(?P<id>[^/]++)/markTipRead$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_TipMarkRead;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'TipMarkRead')), array (  '_controller' => 'partkeepr.tip_of_the_day.mark_read',  '_resource' => 'TipOfTheDay',));
                        }
                        not_TipMarkRead:

                    }

                    if (0 === strpos($pathinfo, '/api/tip_of_the_day_histories')) {
                        // TipHistoriesGet
                        if ($pathinfo === '/api/tip_of_the_day_histories') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_TipHistoriesGet;
                            }

                            return array (  '_controller' => 'partkeepr.tip_of_the_day_history.collection_get',  '_resource' => 'TipOfTheDayHistory',  '_route' => 'TipHistoriesGet',);
                        }
                        not_TipHistoriesGet:

                        // api_tip_of_the_day_histories_get_item
                        if (preg_match('#^/api/tip_of_the_day_histories/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_tip_of_the_day_histories_get_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_tip_of_the_day_histories_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'TipOfTheDayHistory',));
                        }
                        not_api_tip_of_the_day_histories_get_item:

                        // api_tip_of_the_day_histories_put_item
                        if (preg_match('#^/api/tip_of_the_day_histories/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_api_tip_of_the_day_histories_put_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_tip_of_the_day_histories_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'TipOfTheDayHistory',));
                        }
                        not_api_tip_of_the_day_histories_put_item:

                        // api_tip_of_the_day_histories_delete_item
                        if (preg_match('#^/api/tip_of_the_day_histories/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_api_tip_of_the_day_histories_delete_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_tip_of_the_day_histories_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'TipOfTheDayHistory',));
                        }
                        not_api_tip_of_the_day_histories_delete_item:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/api/user')) {
                if (0 === strpos($pathinfo, '/api/user_providers')) {
                    // api_user_providers_get_collection
                    if ($pathinfo === '/api/user_providers') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_user_providers_get_collection;
                        }

                        return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'UserProvider',  '_route' => 'api_user_providers_get_collection',);
                    }
                    not_api_user_providers_get_collection:

                    // api_user_providers_post_collection
                    if ($pathinfo === '/api/user_providers') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_api_user_providers_post_collection;
                        }

                        return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'UserProvider',  '_route' => 'api_user_providers_post_collection',);
                    }
                    not_api_user_providers_post_collection:

                    // api_user_providers_get_item
                    if (preg_match('#^/api/user_providers/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_user_providers_get_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_user_providers_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'UserProvider',));
                    }
                    not_api_user_providers_get_item:

                    // api_user_providers_put_item
                    if (preg_match('#^/api/user_providers/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'PUT') {
                            $allow[] = 'PUT';
                            goto not_api_user_providers_put_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_user_providers_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'UserProvider',));
                    }
                    not_api_user_providers_put_item:

                    // api_user_providers_delete_item
                    if (preg_match('#^/api/user_providers/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'DELETE') {
                            $allow[] = 'DELETE';
                            goto not_api_user_providers_delete_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_user_providers_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'UserProvider',));
                    }
                    not_api_user_providers_delete_item:

                }

                if (0 === strpos($pathinfo, '/api/users')) {
                    // api_users_get_collection
                    if ($pathinfo === '/api/users') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_users_get_collection;
                        }

                        return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'User',  '_route' => 'api_users_get_collection',);
                    }
                    not_api_users_get_collection:

                    // PartKeeprUserPost
                    if ($pathinfo === '/api/users') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_PartKeeprUserPost;
                        }

                        return array (  '_controller' => 'partkeepr.user.post',  '_resource' => 'User',  '_route' => 'PartKeeprUserPost',);
                    }
                    not_PartKeeprUserPost:

                    // PartKeeprAuthGetProviders
                    if ($pathinfo === '/api/users/get_user_providers') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_PartKeeprAuthGetProviders;
                        }

                        return array (  '_controller' => 'partkeepr.auth.get_providers',  '_resource' => 'User',  '_route' => 'PartKeeprAuthGetProviders',);
                    }
                    not_PartKeeprAuthGetProviders:

                    // api_users_get_item
                    if (preg_match('#^/api/users/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_users_get_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_users_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'User',));
                    }
                    not_api_users_get_item:

                    // PartKeeprUserPut
                    if (preg_match('#^/api/users/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'PUT') {
                            $allow[] = 'PUT';
                            goto not_PartKeeprUserPut;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'PartKeeprUserPut')), array (  '_controller' => 'partkeepr.user.put',  '_resource' => 'User',));
                    }
                    not_PartKeeprUserPut:

                    // PartKeeprUserDelete
                    if (preg_match('#^/api/users/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'DELETE') {
                            $allow[] = 'DELETE';
                            goto not_PartKeeprUserDelete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'PartKeeprUserDelete')), array (  '_controller' => 'partkeepr.user.delete',  '_resource' => 'User',));
                    }
                    not_PartKeeprUserDelete:

                }

                if (0 === strpos($pathinfo, '/api/user_preferences')) {
                    // PartKeeprUserPreferenceGet
                    if ($pathinfo === '/api/user_preferences') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_PartKeeprUserPreferenceGet;
                        }

                        return array (  '_controller' => 'partkeepr.user_preference.get_preferences',  '_resource' => 'User',  '_route' => 'PartKeeprUserPreferenceGet',);
                    }
                    not_PartKeeprUserPreferenceGet:

                    // PartKeeprUserPreferenceSet
                    if ($pathinfo === '/api/user_preferences') {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_PartKeeprUserPreferenceSet;
                        }

                        return array (  '_controller' => 'partkeepr.user_preference.set_preference',  '_resource' => 'User',  '_route' => 'PartKeeprUserPreferenceSet',);
                    }
                    not_PartKeeprUserPreferenceSet:

                    // PartKeeprUserPreferenceDelete
                    if ($pathinfo === '/api/user_preferences') {
                        if ($this->context->getMethod() != 'DELETE') {
                            $allow[] = 'DELETE';
                            goto not_PartKeeprUserPreferenceDelete;
                        }

                        return array (  '_controller' => 'partkeepr.user_preference.delete_preference',  '_resource' => 'User',  '_route' => 'PartKeeprUserPreferenceDelete',);
                    }
                    not_PartKeeprUserPreferenceDelete:

                }

                if (0 === strpos($pathinfo, '/api/users')) {
                    // PartKeeprAuthLogin
                    if ($pathinfo === '/api/users/login') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_PartKeeprAuthLogin;
                        }

                        return array (  '_controller' => 'partkeepr.auth.login',  '_resource' => 'User',  '_route' => 'PartKeeprAuthLogin',);
                    }
                    not_PartKeeprAuthLogin:

                    // PartKeeprAuthChangePassword
                    if (preg_match('#^/api/users/(?P<id>[^/]++)/changePassword$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'PUT') {
                            $allow[] = 'PUT';
                            goto not_PartKeeprAuthChangePassword;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'PartKeeprAuthChangePassword')), array (  '_controller' => 'partkeepr.auth.change_password',  '_resource' => 'User',));
                    }
                    not_PartKeeprAuthChangePassword:

                }

            }

            if (0 === strpos($pathinfo, '/api/f_o_s_users')) {
                // api_f_o_s_users_get_collection
                if ($pathinfo === '/api/f_o_s_users') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_api_f_o_s_users_get_collection;
                    }

                    return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'FOSUser',  '_route' => 'api_f_o_s_users_get_collection',);
                }
                not_api_f_o_s_users_get_collection:

                // api_f_o_s_users_post_collection
                if ($pathinfo === '/api/f_o_s_users') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_api_f_o_s_users_post_collection;
                    }

                    return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'FOSUser',  '_route' => 'api_f_o_s_users_post_collection',);
                }
                not_api_f_o_s_users_post_collection:

                // api_f_o_s_users_get_item
                if (preg_match('#^/api/f_o_s_users/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_api_f_o_s_users_get_item;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_f_o_s_users_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'FOSUser',));
                }
                not_api_f_o_s_users_get_item:

                // api_f_o_s_users_put_item
                if (preg_match('#^/api/f_o_s_users/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_api_f_o_s_users_put_item;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_f_o_s_users_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'FOSUser',));
                }
                not_api_f_o_s_users_put_item:

                // api_f_o_s_users_delete_item
                if (preg_match('#^/api/f_o_s_users/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_api_f_o_s_users_delete_item;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_f_o_s_users_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'FOSUser',));
                }
                not_api_f_o_s_users_delete_item:

            }

            if (0 === strpos($pathinfo, '/api/project')) {
                if (0 === strpos($pathinfo, '/api/projects')) {
                    // api_projects_get_collection
                    if ($pathinfo === '/api/projects') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_projects_get_collection;
                        }

                        return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'Project',  '_route' => 'api_projects_get_collection',);
                    }
                    not_api_projects_get_collection:

                    // api_projects_post_collection
                    if ($pathinfo === '/api/projects') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_api_projects_post_collection;
                        }

                        return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'Project',  '_route' => 'api_projects_post_collection',);
                    }
                    not_api_projects_post_collection:

                    // api_projects_get_item
                    if (preg_match('#^/api/projects/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_projects_get_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_projects_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'Project',));
                    }
                    not_api_projects_get_item:

                    // api_projects_put_item
                    if (preg_match('#^/api/projects/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'PUT') {
                            $allow[] = 'PUT';
                            goto not_api_projects_put_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_projects_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'Project',));
                    }
                    not_api_projects_put_item:

                    // api_projects_delete_item
                    if (preg_match('#^/api/projects/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'DELETE') {
                            $allow[] = 'DELETE';
                            goto not_api_projects_delete_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_projects_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'Project',));
                    }
                    not_api_projects_delete_item:

                }

                if (0 === strpos($pathinfo, '/api/project_')) {
                    if (0 === strpos($pathinfo, '/api/project_parts')) {
                        // api_project_parts_get_collection
                        if ($pathinfo === '/api/project_parts') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_project_parts_get_collection;
                            }

                            return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'ProjectPart',  '_route' => 'api_project_parts_get_collection',);
                        }
                        not_api_project_parts_get_collection:

                        // api_project_parts_post_collection
                        if ($pathinfo === '/api/project_parts') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_api_project_parts_post_collection;
                            }

                            return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'ProjectPart',  '_route' => 'api_project_parts_post_collection',);
                        }
                        not_api_project_parts_post_collection:

                        // api_project_parts_get_item
                        if (preg_match('#^/api/project_parts/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_project_parts_get_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_project_parts_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'ProjectPart',));
                        }
                        not_api_project_parts_get_item:

                        // api_project_parts_put_item
                        if (preg_match('#^/api/project_parts/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_api_project_parts_put_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_project_parts_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'ProjectPart',));
                        }
                        not_api_project_parts_put_item:

                        // api_project_parts_delete_item
                        if (preg_match('#^/api/project_parts/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_api_project_parts_delete_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_project_parts_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'ProjectPart',));
                        }
                        not_api_project_parts_delete_item:

                    }

                    if (0 === strpos($pathinfo, '/api/project_attachments')) {
                        // api_project_attachments_get_collection
                        if ($pathinfo === '/api/project_attachments') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_project_attachments_get_collection;
                            }

                            return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'ProjectAttachment',  '_route' => 'api_project_attachments_get_collection',);
                        }
                        not_api_project_attachments_get_collection:

                        // api_project_attachments_post_collection
                        if ($pathinfo === '/api/project_attachments') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_api_project_attachments_post_collection;
                            }

                            return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'ProjectAttachment',  '_route' => 'api_project_attachments_post_collection',);
                        }
                        not_api_project_attachments_post_collection:

                        // api_project_attachments_get_item
                        if (preg_match('#^/api/project_attachments/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_api_project_attachments_get_item;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_project_attachments_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'ProjectAttachment',));
                        }
                        not_api_project_attachments_get_item:

                        // ProjectAttachmentGet
                        if (preg_match('#^/api/project_attachments/(?P<id>[^/]++)/getFile$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_ProjectAttachmentGet;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'ProjectAttachmentGet')), array (  '_controller' => 'PartKeepr\\ProjectBundle\\Controller\\ProjectAttachmentController::getFileAction',  '_resource' => 'ProjectAttachment',));
                        }
                        not_ProjectAttachmentGet:

                        // ProjectAttachmentMimeTypeIcon
                        if (preg_match('#^/api/project_attachments/(?P<id>[^/]++)/getMimeTypeIcon$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_ProjectAttachmentMimeTypeIcon;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'ProjectAttachmentMimeTypeIcon')), array (  '_controller' => 'PartKeepr\\ProjectBundle\\Controller\\ProjectAttachmentController::getMimeTypeIconAction',  '_resource' => 'ProjectAttachment',));
                        }
                        not_ProjectAttachmentMimeTypeIcon:

                        // ProjectAttachmentGetImage
                        if (preg_match('#^/api/project_attachments/(?P<id>[^/]++)/getImage$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_ProjectAttachmentGetImage;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'ProjectAttachmentGetImage')), array (  '_controller' => 'PartKeepr\\ProjectBundle\\Controller\\ProjectAttachmentController::getImageAction',  '_resource' => 'ProjectAttachment',));
                        }
                        not_ProjectAttachmentGetImage:

                    }

                    if (0 === strpos($pathinfo, '/api/project_run')) {
                        if (0 === strpos($pathinfo, '/api/project_runs')) {
                            // api_project_runs_get_collection
                            if ($pathinfo === '/api/project_runs') {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_api_project_runs_get_collection;
                                }

                                return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'ProjectRun',  '_route' => 'api_project_runs_get_collection',);
                            }
                            not_api_project_runs_get_collection:

                            // api_project_runs_post_collection
                            if ($pathinfo === '/api/project_runs') {
                                if ($this->context->getMethod() != 'POST') {
                                    $allow[] = 'POST';
                                    goto not_api_project_runs_post_collection;
                                }

                                return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'ProjectRun',  '_route' => 'api_project_runs_post_collection',);
                            }
                            not_api_project_runs_post_collection:

                            // api_project_runs_get_item
                            if (preg_match('#^/api/project_runs/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_api_project_runs_get_item;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_project_runs_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'ProjectRun',));
                            }
                            not_api_project_runs_get_item:

                            // api_project_runs_put_item
                            if (preg_match('#^/api/project_runs/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                if ($this->context->getMethod() != 'PUT') {
                                    $allow[] = 'PUT';
                                    goto not_api_project_runs_put_item;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_project_runs_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'ProjectRun',));
                            }
                            not_api_project_runs_put_item:

                            // api_project_runs_delete_item
                            if (preg_match('#^/api/project_runs/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                if ($this->context->getMethod() != 'DELETE') {
                                    $allow[] = 'DELETE';
                                    goto not_api_project_runs_delete_item;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_project_runs_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'ProjectRun',));
                            }
                            not_api_project_runs_delete_item:

                        }

                        if (0 === strpos($pathinfo, '/api/project_run_parts')) {
                            // api_project_run_parts_get_collection
                            if ($pathinfo === '/api/project_run_parts') {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_api_project_run_parts_get_collection;
                                }

                                return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'ProjectRunPart',  '_route' => 'api_project_run_parts_get_collection',);
                            }
                            not_api_project_run_parts_get_collection:

                            // api_project_run_parts_post_collection
                            if ($pathinfo === '/api/project_run_parts') {
                                if ($this->context->getMethod() != 'POST') {
                                    $allow[] = 'POST';
                                    goto not_api_project_run_parts_post_collection;
                                }

                                return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'ProjectRunPart',  '_route' => 'api_project_run_parts_post_collection',);
                            }
                            not_api_project_run_parts_post_collection:

                            // api_project_run_parts_get_item
                            if (preg_match('#^/api/project_run_parts/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_api_project_run_parts_get_item;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_project_run_parts_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'ProjectRunPart',));
                            }
                            not_api_project_run_parts_get_item:

                            // api_project_run_parts_put_item
                            if (preg_match('#^/api/project_run_parts/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                if ($this->context->getMethod() != 'PUT') {
                                    $allow[] = 'PUT';
                                    goto not_api_project_run_parts_put_item;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_project_run_parts_put_item')), array (  '_controller' => 'api.action.put_item',  '_resource' => 'ProjectRunPart',));
                            }
                            not_api_project_run_parts_put_item:

                            // api_project_run_parts_delete_item
                            if (preg_match('#^/api/project_run_parts/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                if ($this->context->getMethod() != 'DELETE') {
                                    $allow[] = 'DELETE';
                                    goto not_api_project_run_parts_delete_item;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_project_run_parts_delete_item')), array (  '_controller' => 'api.action.delete_item',  '_resource' => 'ProjectRunPart',));
                            }
                            not_api_project_run_parts_delete_item:

                        }

                    }

                }

            }

            if (0 === strpos($pathinfo, '/api/system_')) {
                if (0 === strpos($pathinfo, '/api/system_notices')) {
                    // api_system_notices_get_collection
                    if ($pathinfo === '/api/system_notices') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_system_notices_get_collection;
                        }

                        return array (  '_controller' => 'api.action.get_collection',  '_resource' => 'SystemNotice',  '_route' => 'api_system_notices_get_collection',);
                    }
                    not_api_system_notices_get_collection:

                    // api_system_notices_post_collection
                    if ($pathinfo === '/api/system_notices') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_api_system_notices_post_collection;
                        }

                        return array (  '_controller' => 'api.action.post_collection',  '_resource' => 'SystemNotice',  '_route' => 'api_system_notices_post_collection',);
                    }
                    not_api_system_notices_post_collection:

                    // api_system_notices_get_item
                    if (preg_match('#^/api/system_notices/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_api_system_notices_get_item;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_system_notices_get_item')), array (  '_controller' => 'api.action.get_item',  '_resource' => 'SystemNotice',));
                    }
                    not_api_system_notices_get_item:

                    // SystemNoticeAcknowledge
                    if (preg_match('#^/api/system_notices/(?P<id>[^/]++)/acknowledge$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'PUT') {
                            $allow[] = 'PUT';
                            goto not_SystemNoticeAcknowledge;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'SystemNoticeAcknowledge')), array (  '_controller' => 'partkeepr.system_notice.acknowledge',  '_resource' => 'SystemNotice',));
                    }
                    not_SystemNoticeAcknowledge:

                }

                if (0 === strpos($pathinfo, '/api/system_preferences')) {
                    // PartKeeprSystemPreferenceGet
                    if ($pathinfo === '/api/system_preferences') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_PartKeeprSystemPreferenceGet;
                        }

                        return array (  '_controller' => 'partkeepr.system_preference.get_preferences',  '_resource' => 'SystemPreference',  '_route' => 'PartKeeprSystemPreferenceGet',);
                    }
                    not_PartKeeprSystemPreferenceGet:

                    // PartKeeprSystemPreferenceSet
                    if ($pathinfo === '/api/system_preferences') {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_PartKeeprSystemPreferenceSet;
                        }

                        return array (  '_controller' => 'partkeepr.system_preference.set_preference',  '_resource' => 'SystemPreference',  '_route' => 'PartKeeprSystemPreferenceSet',);
                    }
                    not_PartKeeprSystemPreferenceSet:

                    // PartKeeprSystemPreferenceDelete
                    if ($pathinfo === '/api/system_preferences') {
                        if ($this->context->getMethod() != 'DELETE') {
                            $allow[] = 'DELETE';
                            goto not_PartKeeprSystemPreferenceDelete;
                        }

                        return array (  '_controller' => 'partkeepr.system_preference.delete_preference',  '_resource' => 'SystemPreference',  '_route' => 'PartKeeprSystemPreferenceDelete',);
                    }
                    not_PartKeeprSystemPreferenceDelete:

                }

            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
