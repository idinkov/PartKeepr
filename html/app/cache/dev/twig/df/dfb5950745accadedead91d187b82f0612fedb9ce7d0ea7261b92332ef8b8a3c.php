<?php

/* PartKeeprSetupBundle::parameters.php.twig */
class __TwigTemplate_5a0a0f8e09ff0c946516582edabaae15d0a96c54436a05917a726a9534d57d56 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e5448c33aa0d434117c2ac0b175d2f0fe6cdb6c5a401c649b2069c6924838731 = $this->env->getExtension("native_profiler");
        $__internal_e5448c33aa0d434117c2ac0b175d2f0fe6cdb6c5a401c649b2069c6924838731->enter($__internal_e5448c33aa0d434117c2ac0b175d2f0fe6cdb6c5a401c649b2069c6924838731_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PartKeeprSetupBundle::parameters.php.twig"));

        // line 1
        echo "<?php
";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["parameters"]) ? $context["parameters"] : null));
        foreach ($context['_seq'] as $context["name"] => $context["value"]) {
            // line 3
            echo "\$container->setParameter('";
            echo twig_escape_filter($this->env, $context["name"], "html", null, true);
            echo "', ";
            echo $context["value"];
            echo ");
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['name'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_e5448c33aa0d434117c2ac0b175d2f0fe6cdb6c5a401c649b2069c6924838731->leave($__internal_e5448c33aa0d434117c2ac0b175d2f0fe6cdb6c5a401c649b2069c6924838731_prof);

    }

    public function getTemplateName()
    {
        return "PartKeeprSetupBundle::parameters.php.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 3,  25 => 2,  22 => 1,);
    }
}
/* <?php*/
/* {% for name,value in parameters %}*/
/* $container->setParameter('{{ name }}', {{ value|raw }});*/
/* {% endfor %}*/
/* */
