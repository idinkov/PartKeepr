<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_dd34c93b2abf4a40e205f3f5f4b2a2f89e7087a4cbfde6d41e15d268a5ea0436 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0ab555b0f1952f642ebdb827589dcd4ef6feab89e016dd2a92b6ad35187a6d03 = $this->env->getExtension("native_profiler");
        $__internal_0ab555b0f1952f642ebdb827589dcd4ef6feab89e016dd2a92b6ad35187a6d03->enter($__internal_0ab555b0f1952f642ebdb827589dcd4ef6feab89e016dd2a92b6ad35187a6d03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0ab555b0f1952f642ebdb827589dcd4ef6feab89e016dd2a92b6ad35187a6d03->leave($__internal_0ab555b0f1952f642ebdb827589dcd4ef6feab89e016dd2a92b6ad35187a6d03_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_efc4d3c8343aaee0f6f71632e7d908dc87b60cb669a07694ef3f5f0cbe5e31a7 = $this->env->getExtension("native_profiler");
        $__internal_efc4d3c8343aaee0f6f71632e7d908dc87b60cb669a07694ef3f5f0cbe5e31a7->enter($__internal_efc4d3c8343aaee0f6f71632e7d908dc87b60cb669a07694ef3f5f0cbe5e31a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.confirmed", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if ((isset($context["targetUrl"]) ? $context["targetUrl"] : null)) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["targetUrl"]) ? $context["targetUrl"] : null), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_efc4d3c8343aaee0f6f71632e7d908dc87b60cb669a07694ef3f5f0cbe5e31a7->leave($__internal_efc4d3c8343aaee0f6f71632e7d908dc87b60cb669a07694ef3f5f0cbe5e31a7_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 8,  45 => 7,  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/*     <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>*/
/*     {% if targetUrl %}*/
/*     <p><a href="{{ targetUrl }}">{{ 'registration.back'|trans }}</a></p>*/
/*     {% endif %}*/
/* {% endblock fos_user_content %}*/
/* */
