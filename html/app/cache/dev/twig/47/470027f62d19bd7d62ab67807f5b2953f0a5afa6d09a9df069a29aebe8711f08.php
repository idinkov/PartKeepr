<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_367d9152eaf29adedef6fab2ea250589c69cc186944bbd42dc996fb312e8f022 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_51ae144da159b728a9f993d7e7e309544351f60a8b7ebb8dcffd5c05a02697ad = $this->env->getExtension("native_profiler");
        $__internal_51ae144da159b728a9f993d7e7e309544351f60a8b7ebb8dcffd5c05a02697ad->enter($__internal_51ae144da159b728a9f993d7e7e309544351f60a8b7ebb8dcffd5c05a02697ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : null))));
        // line 3
        echo "*/
";
        
        $__internal_51ae144da159b728a9f993d7e7e309544351f60a8b7ebb8dcffd5c05a02697ad->leave($__internal_51ae144da159b728a9f993d7e7e309544351f60a8b7ebb8dcffd5c05a02697ad_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
