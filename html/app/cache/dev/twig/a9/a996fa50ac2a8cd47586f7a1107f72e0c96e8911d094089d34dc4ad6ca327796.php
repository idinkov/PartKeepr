<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_7f1f385ba32af353111d3eefde7f01d57a2120676027621b3a09b06573e69a4c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2bd0cca76744f470d3de47e589838be45e8822f51d30df7fe81faaf7a81c371d = $this->env->getExtension("native_profiler");
        $__internal_2bd0cca76744f470d3de47e589838be45e8822f51d30df7fe81faaf7a81c371d->enter($__internal_2bd0cca76744f470d3de47e589838be45e8822f51d30df7fe81faaf7a81c371d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_2bd0cca76744f470d3de47e589838be45e8822f51d30df7fe81faaf7a81c371d->leave($__internal_2bd0cca76744f470d3de47e589838be45e8822f51d30df7fe81faaf7a81c371d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (isset($prototype)): ?>*/
/*     <?php $attr['data-prototype'] = $view->escape($view['form']->row($prototype)) ?>*/
/* <?php endif ?>*/
/* <?php echo $view['form']->widget($form, array('attr' => $attr)) ?>*/
/* */
