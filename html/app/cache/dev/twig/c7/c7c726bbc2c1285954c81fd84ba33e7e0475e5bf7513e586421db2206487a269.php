<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_fc3c23b4ca661a002c92ace29eaccf57d2a6852bf6ef72c1ec25b0735c2b0a7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2f9244ff51402c982148331a601f279d22d0d804693b2c6af7c82c76dfcfd32d = $this->env->getExtension("native_profiler");
        $__internal_2f9244ff51402c982148331a601f279d22d0d804693b2c6af7c82c76dfcfd32d->enter($__internal_2f9244ff51402c982148331a601f279d22d0d804693b2c6af7c82c76dfcfd32d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2f9244ff51402c982148331a601f279d22d0d804693b2c6af7c82c76dfcfd32d->leave($__internal_2f9244ff51402c982148331a601f279d22d0d804693b2c6af7c82c76dfcfd32d_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_dabf99619d2ad0c56dc903b28e635434af166e2b7ff97b96e8958a2a48f4c686 = $this->env->getExtension("native_profiler");
        $__internal_dabf99619d2ad0c56dc903b28e635434af166e2b7ff97b96e8958a2a48f4c686->enter($__internal_dabf99619d2ad0c56dc903b28e635434af166e2b7ff97b96e8958a2a48f4c686_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Registration:register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 4)->display($context);
        
        $__internal_dabf99619d2ad0c56dc903b28e635434af166e2b7ff97b96e8958a2a48f4c686->leave($__internal_dabf99619d2ad0c56dc903b28e635434af166e2b7ff97b96e8958a2a48f4c686_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Registration:register_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
