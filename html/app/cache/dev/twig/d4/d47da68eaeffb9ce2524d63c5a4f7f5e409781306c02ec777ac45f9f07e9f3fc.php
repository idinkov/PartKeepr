<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_e4577b91bf560bf92042b4b898d03329cb8fa631184dc91af4b4af826a971be6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_da5efa9cc37d215f5e4cb680be529e0a749685f35d2fa3aae93e73c459822fa6 = $this->env->getExtension("native_profiler");
        $__internal_da5efa9cc37d215f5e4cb680be529e0a749685f35d2fa3aae93e73c459822fa6->enter($__internal_da5efa9cc37d215f5e4cb680be529e0a749685f35d2fa3aae93e73c459822fa6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_da5efa9cc37d215f5e4cb680be529e0a749685f35d2fa3aae93e73c459822fa6->leave($__internal_da5efa9cc37d215f5e4cb680be529e0a749685f35d2fa3aae93e73c459822fa6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <textarea <?php echo $view['form']->block($form, 'widget_attributes') ?>><?php echo $view->escape($value) ?></textarea>*/
/* */
