<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_9f0f8de315f58b1ed0397c47de2a2bb1f4f6691a8fe7fecb8c1daadd1913a7e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f27f7a3e61a72cd07d6bde00e1779e117b00ed8a80a7b297e8ebac87d98da725 = $this->env->getExtension("native_profiler");
        $__internal_f27f7a3e61a72cd07d6bde00e1779e117b00ed8a80a7b297e8ebac87d98da725->enter($__internal_f27f7a3e61a72cd07d6bde00e1779e117b00ed8a80a7b297e8ebac87d98da725_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_f27f7a3e61a72cd07d6bde00e1779e117b00ed8a80a7b297e8ebac87d98da725->leave($__internal_f27f7a3e61a72cd07d6bde00e1779e117b00ed8a80a7b297e8ebac87d98da725_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="<?php echo isset($type) ? $view->escape($type) : 'text' ?>" <?php echo $view['form']->block($form, 'widget_attributes') ?><?php if (!empty($value) || is_numeric($value)): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?> />*/
/* */
