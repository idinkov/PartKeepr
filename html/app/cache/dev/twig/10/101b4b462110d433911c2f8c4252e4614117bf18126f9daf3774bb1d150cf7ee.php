<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_94cf5fb5930c733cc34d7cee340e3ebba9afab47ea4a5e8251d6b89f6718f826 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_96ad1fac1ff0824b43cae6dcc3f67eaabffe357ffc434aebba9cce8093cda167 = $this->env->getExtension("native_profiler");
        $__internal_96ad1fac1ff0824b43cae6dcc3f67eaabffe357ffc434aebba9cce8093cda167->enter($__internal_96ad1fac1ff0824b43cae6dcc3f67eaabffe357ffc434aebba9cce8093cda167_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_96ad1fac1ff0824b43cae6dcc3f67eaabffe357ffc434aebba9cce8093cda167->leave($__internal_96ad1fac1ff0824b43cae6dcc3f67eaabffe357ffc434aebba9cce8093cda167_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="radio"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     value="<?php echo $view->escape($value) ?>"*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
