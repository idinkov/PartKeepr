<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_994ea7ce62cecf97bf37acff964b23afaf138aee218f992184ed4a23719614c8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f2818a23df5f6439b413a505350bd4c8a4474faa2979d3e4a9db030cf6eb188d = $this->env->getExtension("native_profiler");
        $__internal_f2818a23df5f6439b413a505350bd4c8a4474faa2979d3e4a9db030cf6eb188d->enter($__internal_f2818a23df5f6439b413a505350bd4c8a4474faa2979d3e4a9db030cf6eb188d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_f2818a23df5f6439b413a505350bd4c8a4474faa2979d3e4a9db030cf6eb188d->leave($__internal_f2818a23df5f6439b413a505350bd4c8a4474faa2979d3e4a9db030cf6eb188d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->start($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* <?php echo $view['form']->end($form) ?>*/
/* */
