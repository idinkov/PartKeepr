<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_d88f774d799c7ca6f98911ea6f2e171e9d0c150d24e01a51863f254259de1921 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_392fa7b4eff22d34d88cfbd7881647e729dbeafa8217f058a17884589a86db82 = $this->env->getExtension("native_profiler");
        $__internal_392fa7b4eff22d34d88cfbd7881647e729dbeafa8217f058a17884589a86db82->enter($__internal_392fa7b4eff22d34d88cfbd7881647e729dbeafa8217f058a17884589a86db82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_392fa7b4eff22d34d88cfbd7881647e729dbeafa8217f058a17884589a86db82->leave($__internal_392fa7b4eff22d34d88cfbd7881647e729dbeafa8217f058a17884589a86db82_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_container_attributes') ?>*/
/* */
