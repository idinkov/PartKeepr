<?php

/* PartKeeprSetupBundle::authkey.php.twig */
class __TwigTemplate_481ebf821f52117a4b9793f9299adf541226484996beb3ba7f549eaad00d0391 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b3de195f9c53528cfed4df368472be6fd6311cd4a3d0ce0c7885f944b9ef8321 = $this->env->getExtension("native_profiler");
        $__internal_b3de195f9c53528cfed4df368472be6fd6311cd4a3d0ce0c7885f944b9ef8321->enter($__internal_b3de195f9c53528cfed4df368472be6fd6311cd4a3d0ce0c7885f944b9ef8321_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PartKeeprSetupBundle::authkey.php.twig"));

        // line 1
        echo "<?php
/**
 * Your auth key is: ";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["authkey"]) ? $context["authkey"] : null), "html", null, true);
        echo "
 *
 * Copy and paste the auth key in order to proceed with setup
 */
";
        
        $__internal_b3de195f9c53528cfed4df368472be6fd6311cd4a3d0ce0c7885f944b9ef8321->leave($__internal_b3de195f9c53528cfed4df368472be6fd6311cd4a3d0ce0c7885f944b9ef8321_prof);

    }

    public function getTemplateName()
    {
        return "PartKeeprSetupBundle::authkey.php.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 3,  22 => 1,);
    }
}
/* <?php*/
/* /***/
/*  * Your auth key is: {{ authkey }}*/
/*  **/
/*  * Copy and paste the auth key in order to proceed with setup*/
/*  *//* */
/* */
