<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_2f02610532f78623323d3ca4becb6f549c1dbb7c3a6617b43109289913a44901 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_32dd3e18ad17d03a824080b1afbb549a724a77b3f8c5060a0be6200fd5335f8b = $this->env->getExtension("native_profiler");
        $__internal_32dd3e18ad17d03a824080b1afbb549a724a77b3f8c5060a0be6200fd5335f8b->enter($__internal_32dd3e18ad17d03a824080b1afbb549a724a77b3f8c5060a0be6200fd5335f8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : null), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : null), "css", null, true);
        echo "

*/
";
        
        $__internal_32dd3e18ad17d03a824080b1afbb549a724a77b3f8c5060a0be6200fd5335f8b->leave($__internal_32dd3e18ad17d03a824080b1afbb549a724a77b3f8c5060a0be6200fd5335f8b_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
