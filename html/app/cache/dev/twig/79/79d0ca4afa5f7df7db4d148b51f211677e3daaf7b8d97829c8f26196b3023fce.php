<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_c26d27116f1319c30985d7e90031abb6a12020561e0cdb154a38c726552b250f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e4a7d22be973e0f9c6e0492ed2e8f9f533e17c48c13103a9d635e91bc64c9abe = $this->env->getExtension("native_profiler");
        $__internal_e4a7d22be973e0f9c6e0492ed2e8f9f533e17c48c13103a9d635e91bc64c9abe->enter($__internal_e4a7d22be973e0f9c6e0492ed2e8f9f533e17c48c13103a9d635e91bc64c9abe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_e4a7d22be973e0f9c6e0492ed2e8f9f533e17c48c13103a9d635e91bc64c9abe->leave($__internal_e4a7d22be973e0f9c6e0492ed2e8f9f533e17c48c13103a9d635e91bc64c9abe_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'hidden')) ?>*/
/* */
