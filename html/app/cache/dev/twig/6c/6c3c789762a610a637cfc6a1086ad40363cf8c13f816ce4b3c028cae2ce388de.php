<?php

/* FOSUserBundle:Registration:checkEmail.html.twig */
class __TwigTemplate_62da06cf87b413ef57912ca932474e236c6039e39b0f49fc33f011cfd6208640 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Registration:checkEmail.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c3bcc116c240e6a64f5189ba19f876eff9ce086d2af8e5a9d0f6293c1989785f = $this->env->getExtension("native_profiler");
        $__internal_c3bcc116c240e6a64f5189ba19f876eff9ce086d2af8e5a9d0f6293c1989785f->enter($__internal_c3bcc116c240e6a64f5189ba19f876eff9ce086d2af8e5a9d0f6293c1989785f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:checkEmail.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c3bcc116c240e6a64f5189ba19f876eff9ce086d2af8e5a9d0f6293c1989785f->leave($__internal_c3bcc116c240e6a64f5189ba19f876eff9ce086d2af8e5a9d0f6293c1989785f_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_152789a0bc51797f53e551fd15a9a09f5c2b8f7001c5ed8dd645b7e636d70527 = $this->env->getExtension("native_profiler");
        $__internal_152789a0bc51797f53e551fd15a9a09f5c2b8f7001c5ed8dd645b7e636d70527->enter($__internal_152789a0bc51797f53e551fd15a9a09f5c2b8f7001c5ed8dd645b7e636d70527_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.check_email", array("%email%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_152789a0bc51797f53e551fd15a9a09f5c2b8f7001c5ed8dd645b7e636d70527->leave($__internal_152789a0bc51797f53e551fd15a9a09f5c2b8f7001c5ed8dd645b7e636d70527_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:checkEmail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/*     <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>*/
/* {% endblock fos_user_content %}*/
/* */
