<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_a0acf9fcfad51e969bbf35432ca20f13ea7be57c22f7004bd780288a44a38417 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3df2f1f461a1d8b8b851af04654260db0b541e8c534bf33bc22f3985fabffef0 = $this->env->getExtension("native_profiler");
        $__internal_3df2f1f461a1d8b8b851af04654260db0b541e8c534bf33bc22f3985fabffef0->enter($__internal_3df2f1f461a1d8b8b851af04654260db0b541e8c534bf33bc22f3985fabffef0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3df2f1f461a1d8b8b851af04654260db0b541e8c534bf33bc22f3985fabffef0->leave($__internal_3df2f1f461a1d8b8b851af04654260db0b541e8c534bf33bc22f3985fabffef0_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ef2e1d0f339e182ed54f89b91bc0ff8e3dbcdcabf1c5262a8688796ca71b15d5 = $this->env->getExtension("native_profiler");
        $__internal_ef2e1d0f339e182ed54f89b91bc0ff8e3dbcdcabf1c5262a8688796ca71b15d5->enter($__internal_ef2e1d0f339e182ed54f89b91bc0ff8e3dbcdcabf1c5262a8688796ca71b15d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Resetting:request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_ef2e1d0f339e182ed54f89b91bc0ff8e3dbcdcabf1c5262a8688796ca71b15d5->leave($__internal_ef2e1d0f339e182ed54f89b91bc0ff8e3dbcdcabf1c5262a8688796ca71b15d5_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Resetting:request_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
