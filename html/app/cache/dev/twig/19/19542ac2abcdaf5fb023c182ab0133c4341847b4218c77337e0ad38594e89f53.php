<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_4e7d9dbacb22ae2a6c174a0e60ad403293d61145f2fb154f20b98ad993763866 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_95fe443effb7f8209a849908c600f956e216788cad5d02183a41076e212520ee = $this->env->getExtension("native_profiler");
        $__internal_95fe443effb7f8209a849908c600f956e216788cad5d02183a41076e212520ee->enter($__internal_95fe443effb7f8209a849908c600f956e216788cad5d02183a41076e212520ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_95fe443effb7f8209a849908c600f956e216788cad5d02183a41076e212520ee->leave($__internal_95fe443effb7f8209a849908c600f956e216788cad5d02183a41076e212520ee_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/* <?php foreach ($form as $child): ?>*/
/*     <?php echo $view['form']->widget($child) ?>*/
/*     <?php echo $view['form']->label($child, null, array('translation_domain' => $choice_translation_domain)) ?>*/
/* <?php endforeach ?>*/
/* </div>*/
/* */
