<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_262312612103cd096dbd94f66bfada5dd23498f05c5886d78a3383aea8871d8a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c2fc59679919886f21dc773aabfa34cecb8504ab332b0f88bbac6b6ea930d2a4 = $this->env->getExtension("native_profiler");
        $__internal_c2fc59679919886f21dc773aabfa34cecb8504ab332b0f88bbac6b6ea930d2a4->enter($__internal_c2fc59679919886f21dc773aabfa34cecb8504ab332b0f88bbac6b6ea930d2a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : null))));
        
        $__internal_c2fc59679919886f21dc773aabfa34cecb8504ab332b0f88bbac6b6ea930d2a4->leave($__internal_c2fc59679919886f21dc773aabfa34cecb8504ab332b0f88bbac6b6ea930d2a4_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
