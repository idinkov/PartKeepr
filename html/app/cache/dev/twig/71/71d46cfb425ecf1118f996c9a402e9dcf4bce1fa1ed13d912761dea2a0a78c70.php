<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_0f23738393c32cc3b044e22b9373c6e969f5612a6e3e8c77651838f44d786e1b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_96c3b6b5d5d554027aa90eb4671afd41241cd1fbd74d9516081b73bd93bbac6d = $this->env->getExtension("native_profiler");
        $__internal_96c3b6b5d5d554027aa90eb4671afd41241cd1fbd74d9516081b73bd93bbac6d->enter($__internal_96c3b6b5d5d554027aa90eb4671afd41241cd1fbd74d9516081b73bd93bbac6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) ? $context["widget"] : null), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_96c3b6b5d5d554027aa90eb4671afd41241cd1fbd74d9516081b73bd93bbac6d->leave($__internal_96c3b6b5d5d554027aa90eb4671afd41241cd1fbd74d9516081b73bd93bbac6d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo str_replace('{{ widget }}', $view['form']->block($form, 'form_widget_simple'), $money_pattern) ?>*/
/* */
