<?php

/* @Framework/Form/range_widget.html.php */
class __TwigTemplate_a60ffe845aad21fa6c433f07416daaae6097f57eac7adee218f372e392ec0943 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dde0bc6846888f7ea7d29f09fe75d63da29a1897091ee70539b1fe967da94d4a = $this->env->getExtension("native_profiler");
        $__internal_dde0bc6846888f7ea7d29f09fe75d63da29a1897091ee70539b1fe967da94d4a->enter($__internal_dde0bc6846888f7ea7d29f09fe75d63da29a1897091ee70539b1fe967da94d4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/range_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
";
        
        $__internal_dde0bc6846888f7ea7d29f09fe75d63da29a1897091ee70539b1fe967da94d4a->leave($__internal_dde0bc6846888f7ea7d29f09fe75d63da29a1897091ee70539b1fe967da94d4a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/range_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'range'));*/
/* */
