<?php

/* FOSUserBundle:Group:list.html.twig */
class __TwigTemplate_157ee521ddffb858dd158cd6f5de094bf8a59a600d254de39ed6b337c584c3c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6278197fe20aa75c50b12a40b8dc608b66648073dfb634b1ac64fe0d652c8780 = $this->env->getExtension("native_profiler");
        $__internal_6278197fe20aa75c50b12a40b8dc608b66648073dfb634b1ac64fe0d652c8780->enter($__internal_6278197fe20aa75c50b12a40b8dc608b66648073dfb634b1ac64fe0d652c8780_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6278197fe20aa75c50b12a40b8dc608b66648073dfb634b1ac64fe0d652c8780->leave($__internal_6278197fe20aa75c50b12a40b8dc608b66648073dfb634b1ac64fe0d652c8780_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_c60ec227b6d2dafbff77367b345f598c45dab9f8057f0b4e7b4a47b20139b595 = $this->env->getExtension("native_profiler");
        $__internal_c60ec227b6d2dafbff77367b345f598c45dab9f8057f0b4e7b4a47b20139b595->enter($__internal_c60ec227b6d2dafbff77367b345f598c45dab9f8057f0b4e7b4a47b20139b595_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:list_content.html.twig", "FOSUserBundle:Group:list.html.twig", 4)->display($context);
        
        $__internal_c60ec227b6d2dafbff77367b345f598c45dab9f8057f0b4e7b4a47b20139b595->leave($__internal_c60ec227b6d2dafbff77367b345f598c45dab9f8057f0b4e7b4a47b20139b595_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:list_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
