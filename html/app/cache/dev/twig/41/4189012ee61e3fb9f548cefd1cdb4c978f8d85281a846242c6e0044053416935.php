<?php

/* JMSTranslationBundle::base.html.twig */
class __TwigTemplate_a9eef1291b1216c598e8924c8f017ad08c2804b36f6818d310e46ecd44cfbd31 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'topjavascripts' => array($this, 'block_topjavascripts'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_81a2a5e5eab2c9ecd0e8b272aba2a07806a59a263c381591130f3de9b3416001 = $this->env->getExtension("native_profiler");
        $__internal_81a2a5e5eab2c9ecd0e8b272aba2a07806a59a263c381591130f3de9b3416001->enter($__internal_81a2a5e5eab2c9ecd0e8b272aba2a07806a59a263c381591130f3de9b3416001_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "JMSTranslationBundle::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/jmstranslation/css/bootstrap.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/jmstranslation/css/layout.css"), "html", null, true);
        echo "\" />
        <link rel=\"shortcut icon\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        ";
        // line 9
        $this->displayBlock('topjavascripts', $context, $blocks);
        // line 10
        echo "    </head>
    <body>
        <div class=\"topbar\">
            <div class=\"topbar-inner\">
                <div class=\"container\">
                    <h3><a href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("jms_translation_index");
        echo "\" class=\"logo\">JMSTranslationBundle UI</a></h3>
                    
                </div>
            </div>
        </div>
        <div class=\"container\">
            ";
        // line 21
        $this->displayBlock('body', $context, $blocks);
        // line 22
        echo "        </div>

        ";
        // line 24
        $this->displayBlock('javascripts', $context, $blocks);
        // line 27
        echo "    </body>
</html>";
        
        $__internal_81a2a5e5eab2c9ecd0e8b272aba2a07806a59a263c381591130f3de9b3416001->leave($__internal_81a2a5e5eab2c9ecd0e8b272aba2a07806a59a263c381591130f3de9b3416001_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_b20cd2420f82c1c157017de652ced2e3ccd3a5ca04f95570d0f0d3b441a4a957 = $this->env->getExtension("native_profiler");
        $__internal_b20cd2420f82c1c157017de652ced2e3ccd3a5ca04f95570d0f0d3b441a4a957->enter($__internal_b20cd2420f82c1c157017de652ced2e3ccd3a5ca04f95570d0f0d3b441a4a957_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "JMSTranslationBundle UI";
        
        $__internal_b20cd2420f82c1c157017de652ced2e3ccd3a5ca04f95570d0f0d3b441a4a957->leave($__internal_b20cd2420f82c1c157017de652ced2e3ccd3a5ca04f95570d0f0d3b441a4a957_prof);

    }

    // line 9
    public function block_topjavascripts($context, array $blocks = array())
    {
        $__internal_8e6dbea622937ef1f1d35a3192d17a3f487a5f4baf908c9b54637e18871d0b5b = $this->env->getExtension("native_profiler");
        $__internal_8e6dbea622937ef1f1d35a3192d17a3f487a5f4baf908c9b54637e18871d0b5b->enter($__internal_8e6dbea622937ef1f1d35a3192d17a3f487a5f4baf908c9b54637e18871d0b5b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "topjavascripts"));

        
        $__internal_8e6dbea622937ef1f1d35a3192d17a3f487a5f4baf908c9b54637e18871d0b5b->leave($__internal_8e6dbea622937ef1f1d35a3192d17a3f487a5f4baf908c9b54637e18871d0b5b_prof);

    }

    // line 21
    public function block_body($context, array $blocks = array())
    {
        $__internal_763668371d028fed9b6ea4d18b9ac61933a63077c1bef390768ddd30073da95f = $this->env->getExtension("native_profiler");
        $__internal_763668371d028fed9b6ea4d18b9ac61933a63077c1bef390768ddd30073da95f->enter($__internal_763668371d028fed9b6ea4d18b9ac61933a63077c1bef390768ddd30073da95f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_763668371d028fed9b6ea4d18b9ac61933a63077c1bef390768ddd30073da95f->leave($__internal_763668371d028fed9b6ea4d18b9ac61933a63077c1bef390768ddd30073da95f_prof);

    }

    // line 24
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_00ea0f76389414a7f4ba5b97dd9c17c50d1622cda4e723c72300d8550eea0159 = $this->env->getExtension("native_profiler");
        $__internal_00ea0f76389414a7f4ba5b97dd9c17c50d1622cda4e723c72300d8550eea0159->enter($__internal_00ea0f76389414a7f4ba5b97dd9c17c50d1622cda4e723c72300d8550eea0159_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 25
        echo "        <script language=\"javascript\" type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/jmstranslation/js/jquery.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_00ea0f76389414a7f4ba5b97dd9c17c50d1622cda4e723c72300d8550eea0159->leave($__internal_00ea0f76389414a7f4ba5b97dd9c17c50d1622cda4e723c72300d8550eea0159_prof);

    }

    public function getTemplateName()
    {
        return "JMSTranslationBundle::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 25,  116 => 24,  105 => 21,  94 => 9,  82 => 5,  74 => 27,  72 => 24,  68 => 22,  66 => 21,  57 => 15,  50 => 10,  48 => 9,  44 => 8,  40 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*         <title>{% block title %}JMSTranslationBundle UI{% endblock %}</title>*/
/*         <link rel="stylesheet" type="text/css" media="screen" href="{{ asset("bundles/jmstranslation/css/bootstrap.css") }}" />*/
/*         <link rel="stylesheet" type="text/css" media="screen" href="{{ asset("bundles/jmstranslation/css/layout.css") }}" />*/
/*         <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />*/
/*         {% block topjavascripts %}{% endblock %}*/
/*     </head>*/
/*     <body>*/
/*         <div class="topbar">*/
/*             <div class="topbar-inner">*/
/*                 <div class="container">*/
/*                     <h3><a href="{{ path("jms_translation_index") }}" class="logo">JMSTranslationBundle UI</a></h3>*/
/*                     */
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <div class="container">*/
/*             {% block body %}{% endblock %}*/
/*         </div>*/
/* */
/*         {% block javascripts %}*/
/*         <script language="javascript" type="text/javascript" src="{{ asset("bundles/jmstranslation/js/jquery.js") }}"></script>*/
/*         {% endblock %}*/
/*     </body>*/
/* </html>*/
