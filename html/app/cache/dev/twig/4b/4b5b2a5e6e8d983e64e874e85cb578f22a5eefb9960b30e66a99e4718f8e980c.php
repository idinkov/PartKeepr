<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_8dadf10dfe282f83f10e4294598a01b34df6f74ab035d88024c511bd3c5b6bab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4853190dd16ecb16070f9a9863d3b2e6280f16670c8903ad1217f45c37e2ab8c = $this->env->getExtension("native_profiler");
        $__internal_4853190dd16ecb16070f9a9863d3b2e6280f16670c8903ad1217f45c37e2ab8c->enter($__internal_4853190dd16ecb16070f9a9863d3b2e6280f16670c8903ad1217f45c37e2ab8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_4853190dd16ecb16070f9a9863d3b2e6280f16670c8903ad1217f45c37e2ab8c->leave($__internal_4853190dd16ecb16070f9a9863d3b2e6280f16670c8903ad1217f45c37e2ab8c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
