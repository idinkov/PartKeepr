<?php

/* SensioDistributionBundle::Configurator/layout.html.twig */
class __TwigTemplate_88f7b4b3d9d27a4f568497355e1f8ea213e040397e67447b49a7df4040bcd9f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "SensioDistributionBundle::Configurator/layout.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4cd191b87cbe2027fa774f65e1eb35f3a2b69343302720fdd8ef413618611dcf = $this->env->getExtension("native_profiler");
        $__internal_4cd191b87cbe2027fa774f65e1eb35f3a2b69343302720fdd8ef413618611dcf->enter($__internal_4cd191b87cbe2027fa774f65e1eb35f3a2b69343302720fdd8ef413618611dcf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SensioDistributionBundle::Configurator/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4cd191b87cbe2027fa774f65e1eb35f3a2b69343302720fdd8ef413618611dcf->leave($__internal_4cd191b87cbe2027fa774f65e1eb35f3a2b69343302720fdd8ef413618611dcf_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_361c986695cb347485b4b48d4b77149cef5966fef782d846d5356f4625dd8121 = $this->env->getExtension("native_profiler");
        $__internal_361c986695cb347485b4b48d4b77149cef5966fef782d846d5356f4625dd8121->enter($__internal_361c986695cb347485b4b48d4b77149cef5966fef782d846d5356f4625dd8121_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/sensiodistribution/webconfigurator/css/configurator.css"), "html", null, true);
        echo "\" />
";
        
        $__internal_361c986695cb347485b4b48d4b77149cef5966fef782d846d5356f4625dd8121->leave($__internal_361c986695cb347485b4b48d4b77149cef5966fef782d846d5356f4625dd8121_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_5e30d9237be9336d80fa5cfc68a7c711c7ce9b2cd1642ab2a06b7e743f77f65e = $this->env->getExtension("native_profiler");
        $__internal_5e30d9237be9336d80fa5cfc68a7c711c7ce9b2cd1642ab2a06b7e743f77f65e->enter($__internal_5e30d9237be9336d80fa5cfc68a7c711c7ce9b2cd1642ab2a06b7e743f77f65e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Web Configurator Bundle";
        
        $__internal_5e30d9237be9336d80fa5cfc68a7c711c7ce9b2cd1642ab2a06b7e743f77f65e->leave($__internal_5e30d9237be9336d80fa5cfc68a7c711c7ce9b2cd1642ab2a06b7e743f77f65e_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_d798f830790207b8621e20b5d690b794a566e3e28e4618b815e54525a0819d0c = $this->env->getExtension("native_profiler");
        $__internal_d798f830790207b8621e20b5d690b794a566e3e28e4618b815e54525a0819d0c->enter($__internal_d798f830790207b8621e20b5d690b794a566e3e28e4618b815e54525a0819d0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "    <div class=\"block\">
        ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 12
        echo "    </div>
    <div class=\"version\">Symfony Standard Edition v.";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["version"]) ? $context["version"] : null), "html", null, true);
        echo "</div>
";
        
        $__internal_d798f830790207b8621e20b5d690b794a566e3e28e4618b815e54525a0819d0c->leave($__internal_d798f830790207b8621e20b5d690b794a566e3e28e4618b815e54525a0819d0c_prof);

    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        $__internal_ce365864a8cb35260581311d8e4617ab187339522939cff0ea1bdc11fba4f088 = $this->env->getExtension("native_profiler");
        $__internal_ce365864a8cb35260581311d8e4617ab187339522939cff0ea1bdc11fba4f088->enter($__internal_ce365864a8cb35260581311d8e4617ab187339522939cff0ea1bdc11fba4f088_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_ce365864a8cb35260581311d8e4617ab187339522939cff0ea1bdc11fba4f088->leave($__internal_ce365864a8cb35260581311d8e4617ab187339522939cff0ea1bdc11fba4f088_prof);

    }

    public function getTemplateName()
    {
        return "SensioDistributionBundle::Configurator/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 11,  79 => 13,  76 => 12,  74 => 11,  71 => 10,  65 => 9,  53 => 7,  43 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends "TwigBundle::layout.html.twig" %}*/
/* */
/* {% block head %}*/
/*     <link rel="stylesheet" href="{{ asset('bundles/sensiodistribution/webconfigurator/css/configurator.css') }}" />*/
/* {% endblock %}*/
/* */
/* {% block title 'Web Configurator Bundle' %}*/
/* */
/* {% block body %}*/
/*     <div class="block">*/
/*         {% block content %}{% endblock %}*/
/*     </div>*/
/*     <div class="version">Symfony Standard Edition v.{{ version }}</div>*/
/* {% endblock %}*/
/* */
