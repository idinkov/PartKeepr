<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_5bf48bf7e13a047793935b13ff9f293c58842439c0a749d18f841ed7a04b719b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c4f02c1ef30b696d4243e46a99c2f3b2eadaa936bf1eeb9e714f01ae3454072c = $this->env->getExtension("native_profiler");
        $__internal_c4f02c1ef30b696d4243e46a99c2f3b2eadaa936bf1eeb9e714f01ae3454072c->enter($__internal_c4f02c1ef30b696d4243e46a99c2f3b2eadaa936bf1eeb9e714f01ae3454072c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_c4f02c1ef30b696d4243e46a99c2f3b2eadaa936bf1eeb9e714f01ae3454072c->leave($__internal_c4f02c1ef30b696d4243e46a99c2f3b2eadaa936bf1eeb9e714f01ae3454072c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'email')) ?>*/
/* */
