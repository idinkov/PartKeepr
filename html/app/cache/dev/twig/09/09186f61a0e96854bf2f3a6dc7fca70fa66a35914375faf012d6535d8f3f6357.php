<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_fb1a0402777daff19adf1ff39daccbd64aa5eb7ef8e73e1e517d62664516b909 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ffbfc3057047cb223c974b3b528ddf2a43af8160a044cdf005217f732975d5ec = $this->env->getExtension("native_profiler");
        $__internal_ffbfc3057047cb223c974b3b528ddf2a43af8160a044cdf005217f732975d5ec->enter($__internal_ffbfc3057047cb223c974b3b528ddf2a43af8160a044cdf005217f732975d5ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ffbfc3057047cb223c974b3b528ddf2a43af8160a044cdf005217f732975d5ec->leave($__internal_ffbfc3057047cb223c974b3b528ddf2a43af8160a044cdf005217f732975d5ec_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_9f4021e7631a23b1ad271eb4001a01dac9f3a560f4417cc2ea48c7c330e77ddf = $this->env->getExtension("native_profiler");
        $__internal_9f4021e7631a23b1ad271eb4001a01dac9f3a560f4417cc2ea48c7c330e77ddf->enter($__internal_9f4021e7631a23b1ad271eb4001a01dac9f3a560f4417cc2ea48c7c330e77ddf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_9f4021e7631a23b1ad271eb4001a01dac9f3a560f4417cc2ea48c7c330e77ddf->leave($__internal_9f4021e7631a23b1ad271eb4001a01dac9f3a560f4417cc2ea48c7c330e77ddf_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_367702809c77da7970e6ed63ac7079ceb8e8899cfb377b2f232247057aaf4405 = $this->env->getExtension("native_profiler");
        $__internal_367702809c77da7970e6ed63ac7079ceb8e8899cfb377b2f232247057aaf4405->enter($__internal_367702809c77da7970e6ed63ac7079ceb8e8899cfb377b2f232247057aaf4405_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_367702809c77da7970e6ed63ac7079ceb8e8899cfb377b2f232247057aaf4405->leave($__internal_367702809c77da7970e6ed63ac7079ceb8e8899cfb377b2f232247057aaf4405_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_939bec9db7902227a30632ce71a4782e3949b2633157adfd77149337f1a9cf6e = $this->env->getExtension("native_profiler");
        $__internal_939bec9db7902227a30632ce71a4782e3949b2633157adfd77149337f1a9cf6e->enter($__internal_939bec9db7902227a30632ce71a4782e3949b2633157adfd77149337f1a9cf6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : null))));
        echo "
";
        
        $__internal_939bec9db7902227a30632ce71a4782e3949b2633157adfd77149337f1a9cf6e->leave($__internal_939bec9db7902227a30632ce71a4782e3949b2633157adfd77149337f1a9cf6e_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
