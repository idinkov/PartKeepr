<?php

/* @Framework/Form/button_widget.html.php */
class __TwigTemplate_ad7a2ddd0b8e61687cde60b4fda86465985795eda8a2f5e8b6d5aa1e3ded91ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_93b5de40a2d5e5cfb70059d75690bf654b4eacc62d5ebb575aefeaed7b4fd513 = $this->env->getExtension("native_profiler");
        $__internal_93b5de40a2d5e5cfb70059d75690bf654b4eacc62d5ebb575aefeaed7b4fd513->enter($__internal_93b5de40a2d5e5cfb70059d75690bf654b4eacc62d5ebb575aefeaed7b4fd513_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        // line 1
        echo "<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
";
        
        $__internal_93b5de40a2d5e5cfb70059d75690bf654b4eacc62d5ebb575aefeaed7b4fd513->leave($__internal_93b5de40a2d5e5cfb70059d75690bf654b4eacc62d5ebb575aefeaed7b4fd513_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!$label) { $label = isset($label_format)*/
/*     ? strtr($label_format, array('%name%' => $name, '%id%' => $id))*/
/*     : $view['form']->humanize($name); } ?>*/
/* <button type="<?php echo isset($type) ? $view->escape($type) : 'button' ?>" <?php echo $view['form']->block($form, 'button_attributes') ?>><?php echo $view->escape(false !== $translation_domain ? $view['translator']->trans($label, array(), $translation_domain) : $label) ?></button>*/
/* */
