<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_4df6416357fa552bcb0b9e8601041d5fc29412dfd9023262a9a4849c3e354f73 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4744e1e1cf42744683acd113e59c7782a1a0077d102d473ca1550498ade3762d = $this->env->getExtension("native_profiler");
        $__internal_4744e1e1cf42744683acd113e59c7782a1a0077d102d473ca1550498ade3762d->enter($__internal_4744e1e1cf42744683acd113e59c7782a1a0077d102d473ca1550498ade3762d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_4744e1e1cf42744683acd113e59c7782a1a0077d102d473ca1550498ade3762d->leave($__internal_4744e1e1cf42744683acd113e59c7782a1a0077d102d473ca1550498ade3762d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?>*/
/* */
