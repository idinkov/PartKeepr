<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_6d24424d49f82fc2dbe7ce948affbd2b50d21216b9447118899cf8bfa41c92d5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a147f90e4902fb4c72135ccd98b9c8297e14322024d36713ade39ea504ddde0c = $this->env->getExtension("native_profiler");
        $__internal_a147f90e4902fb4c72135ccd98b9c8297e14322024d36713ade39ea504ddde0c->enter($__internal_a147f90e4902fb4c72135ccd98b9c8297e14322024d36713ade39ea504ddde0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : null))));
        // line 3
        echo "*/
";
        
        $__internal_a147f90e4902fb4c72135ccd98b9c8297e14322024d36713ade39ea504ddde0c->leave($__internal_a147f90e4902fb4c72135ccd98b9c8297e14322024d36713ade39ea504ddde0c_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
