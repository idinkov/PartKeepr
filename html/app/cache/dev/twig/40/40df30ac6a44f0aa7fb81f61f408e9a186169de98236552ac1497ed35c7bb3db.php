<?php

/* FOSUserBundle:Resetting:checkEmail.html.twig */
class __TwigTemplate_6b2677d8737c305b6f3603a4aa74bc5a9559abba453927d85c293062c39fc4aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Resetting:checkEmail.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8cdd51accd39639c266085290d9357de6de7dbe122c6e6e798583419ab2edd87 = $this->env->getExtension("native_profiler");
        $__internal_8cdd51accd39639c266085290d9357de6de7dbe122c6e6e798583419ab2edd87->enter($__internal_8cdd51accd39639c266085290d9357de6de7dbe122c6e6e798583419ab2edd87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:checkEmail.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8cdd51accd39639c266085290d9357de6de7dbe122c6e6e798583419ab2edd87->leave($__internal_8cdd51accd39639c266085290d9357de6de7dbe122c6e6e798583419ab2edd87_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_8373cda0faa4b209bd92ba174e271fed986ced714728d854d951388277c1acf1 = $this->env->getExtension("native_profiler");
        $__internal_8373cda0faa4b209bd92ba174e271fed986ced714728d854d951388277c1acf1->enter($__internal_8373cda0faa4b209bd92ba174e271fed986ced714728d854d951388277c1acf1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.check_email", array("%email%" => (isset($context["email"]) ? $context["email"] : null)), "FOSUserBundle"), "html", null, true);
        echo "
</p>
";
        
        $__internal_8373cda0faa4b209bd92ba174e271fed986ced714728d854d951388277c1acf1->leave($__internal_8373cda0faa4b209bd92ba174e271fed986ced714728d854d951388277c1acf1_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:checkEmail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 7,  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/* <p>*/
/* {{ 'resetting.check_email'|trans({'%email%': email}) }}*/
/* </p>*/
/* {% endblock %}*/
/* */
