<?php

/* FOSUserBundle:Group:new.html.twig */
class __TwigTemplate_38a7ab1552b93f0b17b98aef2c8b5127fef4e2d9eab826b2b2eb1d6cac8a1fef extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fa72b8518b0059b8d5824104192b1184ea7cb92dec11afdd47e6d0f36eeaefa0 = $this->env->getExtension("native_profiler");
        $__internal_fa72b8518b0059b8d5824104192b1184ea7cb92dec11afdd47e6d0f36eeaefa0->enter($__internal_fa72b8518b0059b8d5824104192b1184ea7cb92dec11afdd47e6d0f36eeaefa0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fa72b8518b0059b8d5824104192b1184ea7cb92dec11afdd47e6d0f36eeaefa0->leave($__internal_fa72b8518b0059b8d5824104192b1184ea7cb92dec11afdd47e6d0f36eeaefa0_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_1bd10de4a3f24ab8ed328b372c7009566082ff8955c54bd0d618d0e35b7e52a7 = $this->env->getExtension("native_profiler");
        $__internal_1bd10de4a3f24ab8ed328b372c7009566082ff8955c54bd0d618d0e35b7e52a7->enter($__internal_1bd10de4a3f24ab8ed328b372c7009566082ff8955c54bd0d618d0e35b7e52a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:new_content.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_1bd10de4a3f24ab8ed328b372c7009566082ff8955c54bd0d618d0e35b7e52a7->leave($__internal_1bd10de4a3f24ab8ed328b372c7009566082ff8955c54bd0d618d0e35b7e52a7_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:new_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
