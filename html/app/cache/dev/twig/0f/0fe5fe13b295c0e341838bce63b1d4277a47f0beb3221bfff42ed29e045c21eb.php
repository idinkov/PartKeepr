<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_1b7d3a3e461df008a45835b91b0cfad7ee6bc951f76ac66911d80e890b2c751b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7cd085d97e099c56bc421b992c25250963ca41f20f7d093639c8a1d7b094dd98 = $this->env->getExtension("native_profiler");
        $__internal_7cd085d97e099c56bc421b992c25250963ca41f20f7d093639c8a1d7b094dd98->enter($__internal_7cd085d97e099c56bc421b992c25250963ca41f20f7d093639c8a1d7b094dd98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_7cd085d97e099c56bc421b992c25250963ca41f20f7d093639c8a1d7b094dd98->leave($__internal_7cd085d97e099c56bc421b992c25250963ca41f20f7d093639c8a1d7b094dd98_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?> %*/
/* */
