<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_b0eaa2f2535937d1735dbd5c3a9de2e20f05b2478c4868a126e8c2e864ac9a7d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3d872f153c539961469b763576e87f9801c9ea480a07c4609dbef954c8aa580b = $this->env->getExtension("native_profiler");
        $__internal_3d872f153c539961469b763576e87f9801c9ea480a07c4609dbef954c8aa580b->enter($__internal_3d872f153c539961469b763576e87f9801c9ea480a07c4609dbef954c8aa580b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3d872f153c539961469b763576e87f9801c9ea480a07c4609dbef954c8aa580b->leave($__internal_3d872f153c539961469b763576e87f9801c9ea480a07c4609dbef954c8aa580b_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_0389c1f0d355f3aa779351c7cab1bdef0c6216295196918e84bbeb92c7a3e705 = $this->env->getExtension("native_profiler");
        $__internal_0389c1f0d355f3aa779351c7cab1bdef0c6216295196918e84bbeb92c7a3e705->enter($__internal_0389c1f0d355f3aa779351c7cab1bdef0c6216295196918e84bbeb92c7a3e705_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Resetting:reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_0389c1f0d355f3aa779351c7cab1bdef0c6216295196918e84bbeb92c7a3e705->leave($__internal_0389c1f0d355f3aa779351c7cab1bdef0c6216295196918e84bbeb92c7a3e705_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Resetting:reset_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
