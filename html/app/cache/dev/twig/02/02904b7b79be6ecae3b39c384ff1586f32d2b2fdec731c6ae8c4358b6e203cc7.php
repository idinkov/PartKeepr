<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_74388d4ad0cc6a189f4805ee6ea10a86425f1f0445bf21ad053f924cfa558075 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cd09c8796423cfc8851ff1008be3bab47d9c12216f35074eef3c56e911a08918 = $this->env->getExtension("native_profiler");
        $__internal_cd09c8796423cfc8851ff1008be3bab47d9c12216f35074eef3c56e911a08918->enter($__internal_cd09c8796423cfc8851ff1008be3bab47d9c12216f35074eef3c56e911a08918_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_cd09c8796423cfc8851ff1008be3bab47d9c12216f35074eef3c56e911a08918->leave($__internal_cd09c8796423cfc8851ff1008be3bab47d9c12216f35074eef3c56e911a08918_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/* */
