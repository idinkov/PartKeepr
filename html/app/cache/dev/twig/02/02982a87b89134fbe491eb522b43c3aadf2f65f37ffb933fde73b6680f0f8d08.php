<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_fcc8ad62bccbc80d8b321d325ea0c1bb3c8b7df05afa68ac196b18d2e2bfc3ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8ecc3273e824f070359b09030e04a697a6b4bdc6d1988010697463f5b348263c = $this->env->getExtension("native_profiler");
        $__internal_8ecc3273e824f070359b09030e04a697a6b4bdc6d1988010697463f5b348263c->enter($__internal_8ecc3273e824f070359b09030e04a697a6b4bdc6d1988010697463f5b348263c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8ecc3273e824f070359b09030e04a697a6b4bdc6d1988010697463f5b348263c->leave($__internal_8ecc3273e824f070359b09030e04a697a6b4bdc6d1988010697463f5b348263c_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f7f10d1415ed53612875ecd442c637b41a3d336d54837e20dd3189f69c4d7363 = $this->env->getExtension("native_profiler");
        $__internal_f7f10d1415ed53612875ecd442c637b41a3d336d54837e20dd3189f69c4d7363->enter($__internal_f7f10d1415ed53612875ecd442c637b41a3d336d54837e20dd3189f69c4d7363_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Profile:show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_f7f10d1415ed53612875ecd442c637b41a3d336d54837e20dd3189f69c4d7363->leave($__internal_f7f10d1415ed53612875ecd442c637b41a3d336d54837e20dd3189f69c4d7363_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Profile:show_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
