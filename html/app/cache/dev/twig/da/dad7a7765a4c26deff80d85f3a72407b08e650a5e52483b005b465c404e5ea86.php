<?php

/* WebProfilerBundle:Collector:exception.html.twig */
class __TwigTemplate_50aaaf7d0247f59573b86335d3428d4fb82970a4f4f609460a0aa733c4de436e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5ddd09e708389f8981f9df265ceebd368d72a38480f224b7de5668fd7da5fcf9 = $this->env->getExtension("native_profiler");
        $__internal_5ddd09e708389f8981f9df265ceebd368d72a38480f224b7de5668fd7da5fcf9->enter($__internal_5ddd09e708389f8981f9df265ceebd368d72a38480f224b7de5668fd7da5fcf9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5ddd09e708389f8981f9df265ceebd368d72a38480f224b7de5668fd7da5fcf9->leave($__internal_5ddd09e708389f8981f9df265ceebd368d72a38480f224b7de5668fd7da5fcf9_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_7cff2a78ade4beb2f963382e0784b962b617d161ba30758faa01e3369fdfdfb1 = $this->env->getExtension("native_profiler");
        $__internal_7cff2a78ade4beb2f963382e0784b962b617d161ba30758faa01e3369fdfdfb1->enter($__internal_7cff2a78ade4beb2f963382e0784b962b617d161ba30758faa01e3369fdfdfb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : null), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) ? $context["token"] : null))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_7cff2a78ade4beb2f963382e0784b962b617d161ba30758faa01e3369fdfdfb1->leave($__internal_7cff2a78ade4beb2f963382e0784b962b617d161ba30758faa01e3369fdfdfb1_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_66f779f608f9da3c292d064663a585f196946cd327918ad63e9625868314e22a = $this->env->getExtension("native_profiler");
        $__internal_66f779f608f9da3c292d064663a585f196946cd327918ad63e9625868314e22a->enter($__internal_66f779f608f9da3c292d064663a585f196946cd327918ad63e9625868314e22a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : null), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : null), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_66f779f608f9da3c292d064663a585f196946cd327918ad63e9625868314e22a->leave($__internal_66f779f608f9da3c292d064663a585f196946cd327918ad63e9625868314e22a_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_ac03e1733743bf69f8e03150d209e76b0e0f7cd3c2232179251aacd04412d978 = $this->env->getExtension("native_profiler");
        $__internal_ac03e1733743bf69f8e03150d209e76b0e0f7cd3c2232179251aacd04412d978->enter($__internal_ac03e1733743bf69f8e03150d209e76b0e0f7cd3c2232179251aacd04412d978_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute((isset($context["collector"]) ? $context["collector"] : null), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_exception", array("token" => (isset($context["token"]) ? $context["token"] : null))));
            echo "
        </div>
    ";
        }
        
        $__internal_ac03e1733743bf69f8e03150d209e76b0e0f7cd3c2232179251aacd04412d978->leave($__internal_ac03e1733743bf69f8e03150d209e76b0e0f7cd3c2232179251aacd04412d978_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 33,  114 => 32,  108 => 28,  106 => 27,  102 => 25,  96 => 24,  88 => 21,  82 => 17,  80 => 16,  75 => 14,  70 => 13,  64 => 12,  54 => 9,  48 => 6,  45 => 5,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     {% if collector.hasexception %}*/
/*         <style>*/
/*             {{ render(path('_profiler_exception_css', { token: token })) }}*/
/*         </style>*/
/*     {% endif %}*/
/*     {{ parent() }}*/
/* {% endblock %}*/
/* */
/* {% block menu %}*/
/*     <span class="label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}">*/
/*         <span class="icon">{{ include('@WebProfiler/Icon/exception.svg') }}</span>*/
/*         <strong>Exception</strong>*/
/*         {% if collector.hasexception %}*/
/*             <span class="count">*/
/*                 <span>1</span>*/
/*             </span>*/
/*         {% endif %}*/
/*     </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     <h2>Exceptions</h2>*/
/* */
/*     {% if not collector.hasexception %}*/
/*         <div class="empty">*/
/*             <p>No exception was thrown and caught during the request.</p>*/
/*         </div>*/
/*     {% else %}*/
/*         <div class="sf-reset">*/
/*             {{ render(path('_profiler_exception', { token: token })) }}*/
/*         </div>*/
/*     {% endif %}*/
/* {% endblock %}*/
/* */
