<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_39d5e41a215c1241b8d21c12888bc918f48bbac5091b18233d5fe127e2c6682e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e42d2fc6defb679926ac34dbd7135a8fb05bf44180db0dce6d03b7c229466db1 = $this->env->getExtension("native_profiler");
        $__internal_e42d2fc6defb679926ac34dbd7135a8fb05bf44180db0dce6d03b7c229466db1->enter($__internal_e42d2fc6defb679926ac34dbd7135a8fb05bf44180db0dce6d03b7c229466db1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_e42d2fc6defb679926ac34dbd7135a8fb05bf44180db0dce6d03b7c229466db1->leave($__internal_e42d2fc6defb679926ac34dbd7135a8fb05bf44180db0dce6d03b7c229466db1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child): ?>*/
/*     <?php if (!$child->isRendered()): ?>*/
/*         <?php echo $view['form']->row($child) ?>*/
/*     <?php endif; ?>*/
/* <?php endforeach; ?>*/
/* */
