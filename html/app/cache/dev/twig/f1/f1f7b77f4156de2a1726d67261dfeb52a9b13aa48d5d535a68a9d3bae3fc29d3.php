<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_7a2bf1b9c13a4d03331fb3a53b3736d69940b2e102463deb00d5cd7c41d81875 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_11acb7c476586e9ac436452c9da2fc1a4cc7896a67a506f39d390ccbd5350166 = $this->env->getExtension("native_profiler");
        $__internal_11acb7c476586e9ac436452c9da2fc1a4cc7896a67a506f39d390ccbd5350166->enter($__internal_11acb7c476586e9ac436452c9da2fc1a4cc7896a67a506f39d390ccbd5350166_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_11acb7c476586e9ac436452c9da2fc1a4cc7896a67a506f39d390ccbd5350166->leave($__internal_11acb7c476586e9ac436452c9da2fc1a4cc7896a67a506f39d390ccbd5350166_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($form->vars['multipart']): ?>enctype="multipart/form-data"<?php endif ?>*/
/* */
