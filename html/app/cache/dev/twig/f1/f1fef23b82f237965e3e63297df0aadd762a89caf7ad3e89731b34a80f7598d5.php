<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_9c5cd2406ff4c04afbd92b8f10f9ed9dadd1f474d25bc002af56fac7c4476805 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ab6a3c9620cbc46ae3c5e1c0a1166717572e1e503e65f209e11f86ea34684e75 = $this->env->getExtension("native_profiler");
        $__internal_ab6a3c9620cbc46ae3c5e1c0a1166717572e1e503e65f209e11f86ea34684e75->enter($__internal_ab6a3c9620cbc46ae3c5e1c0a1166717572e1e503e65f209e11f86ea34684e75_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_ab6a3c9620cbc46ae3c5e1c0a1166717572e1e503e65f209e11f86ea34684e75->leave($__internal_ab6a3c9620cbc46ae3c5e1c0a1166717572e1e503e65f209e11f86ea34684e75_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'password')) ?>*/
/* */
