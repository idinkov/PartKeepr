<?php

/* PartKeeprFootprintBundle:Default:index.html.twig */
class __TwigTemplate_e8ae4d57f09f41f3eb72f54906608d34fa055cd12cb0a426eb4f597f3da315d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_17d34047214b68242d434c18cfc04931341320a61d20140bfc423465c7616428 = $this->env->getExtension("native_profiler");
        $__internal_17d34047214b68242d434c18cfc04931341320a61d20140bfc423465c7616428->enter($__internal_17d34047214b68242d434c18cfc04931341320a61d20140bfc423465c7616428_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PartKeeprFootprintBundle:Default:index.html.twig"));

        // line 1
        echo "Hello ";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
        echo "!
";
        
        $__internal_17d34047214b68242d434c18cfc04931341320a61d20140bfc423465c7616428->leave($__internal_17d34047214b68242d434c18cfc04931341320a61d20140bfc423465c7616428_prof);

    }

    public function getTemplateName()
    {
        return "PartKeeprFootprintBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* Hello {{ name }}!*/
/* */
