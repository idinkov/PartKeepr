<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_cc0a9c1860d99b4aa9c4c0a7d47603fc377173e901bb5e1ba46d88e7cf1f1970 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_62130205af535d68369579cb4cfd305c04c584cf6a67782483ce5e12cd891b0f = $this->env->getExtension("native_profiler");
        $__internal_62130205af535d68369579cb4cfd305c04c584cf6a67782483ce5e12cd891b0f->enter($__internal_62130205af535d68369579cb4cfd305c04c584cf6a67782483ce5e12cd891b0f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_62130205af535d68369579cb4cfd305c04c584cf6a67782483ce5e12cd891b0f->leave($__internal_62130205af535d68369579cb4cfd305c04c584cf6a67782483ce5e12cd891b0f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="checkbox"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     <?php if (strlen($value) > 0): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?>*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
