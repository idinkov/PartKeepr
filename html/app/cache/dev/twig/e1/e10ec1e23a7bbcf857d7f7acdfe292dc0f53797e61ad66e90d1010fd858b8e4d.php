<?php

/* PartKeeprFrontendBundle::maintenance.html.twig */
class __TwigTemplate_0cf01ab4e141f7312b338f67767bd5b715b90d4f1947fb07a0362a634cf53449 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2bb1baf5cdb489b6b69e7b2ff035af240a775db93c4022b9dc570592a8d26a16 = $this->env->getExtension("native_profiler");
        $__internal_2bb1baf5cdb489b6b69e7b2ff035af240a775db93c4022b9dc570592a8d26a16->enter($__internal_2bb1baf5cdb489b6b69e7b2ff035af240a775db93c4022b9dc570592a8d26a16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PartKeeprFrontendBundle::maintenance.html.twig"));

        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"
        \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">
<html>
<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>
    <title>";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["maintenanceTitle"]) ? $context["maintenanceTitle"] : null), "html", null, true);
        echo "</title>
</head>
<body>
    <h1>";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["maintenanceTitle"]) ? $context["maintenanceTitle"] : null), "html", null, true);
        echo "</h1>

    <p>";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["maintenanceMessage"]) ? $context["maintenanceMessage"] : null), "html", null, true);
        echo "</p>
</body>
</html>
";
        
        $__internal_2bb1baf5cdb489b6b69e7b2ff035af240a775db93c4022b9dc570592a8d26a16->leave($__internal_2bb1baf5cdb489b6b69e7b2ff035af240a775db93c4022b9dc570592a8d26a16_prof);

    }

    public function getTemplateName()
    {
        return "PartKeeprFrontendBundle::maintenance.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 11,  35 => 9,  29 => 6,  22 => 1,);
    }
}
/* <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"*/
/*         "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">*/
/* <html>*/
/* <head>*/
/*     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>*/
/*     <title>{{ maintenanceTitle }}</title>*/
/* </head>*/
/* <body>*/
/*     <h1>{{ maintenanceTitle }}</h1>*/
/* */
/*     <p>{{ maintenanceMessage }}</p>*/
/* </body>*/
/* </html>*/
/* */
