<?php

/* TwigBundle:Exception:error.xml.twig */
class __TwigTemplate_3fc2d3c2af145a0cf3d85de07c606816140890b960da380d97a397af4093fc50 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b377b1c27d177831304460603150cb88a14f899cb4ceba6052ca853c078d0d39 = $this->env->getExtension("native_profiler");
        $__internal_b377b1c27d177831304460603150cb88a14f899cb4ceba6052ca853c078d0d39->enter($__internal_b377b1c27d177831304460603150cb88a14f899cb4ceba6052ca853c078d0d39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : null), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : null), "html", null, true);
        echo "\" />
";
        
        $__internal_b377b1c27d177831304460603150cb88a14f899cb4ceba6052ca853c078d0d39->leave($__internal_b377b1c27d177831304460603150cb88a14f899cb4ceba6052ca853c078d0d39_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  22 => 1,);
    }
}
/* <?xml version="1.0" encoding="{{ _charset }}" ?>*/
/* */
/* <error code="{{ status_code }}" message="{{ status_text }}" />*/
/* */
