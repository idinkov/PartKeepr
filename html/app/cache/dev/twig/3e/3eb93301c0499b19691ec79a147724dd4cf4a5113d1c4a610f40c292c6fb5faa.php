<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_959d0eb60a39da232eddef0f9e9d2992a46af13c985bc2ed97637c8967bd9359 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a5dd48b51d5032ec541a501697b86359e47b624d7a880c0c063019619cde9170 = $this->env->getExtension("native_profiler");
        $__internal_a5dd48b51d5032ec541a501697b86359e47b624d7a880c0c063019619cde9170->enter($__internal_a5dd48b51d5032ec541a501697b86359e47b624d7a880c0c063019619cde9170_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_a5dd48b51d5032ec541a501697b86359e47b624d7a880c0c063019619cde9170->leave($__internal_a5dd48b51d5032ec541a501697b86359e47b624d7a880c0c063019619cde9170_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td>*/
/*         <?php echo $view['form']->label($form) ?>*/
/*     </td>*/
/*     <td>*/
/*         <?php echo $view['form']->errors($form) ?>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
