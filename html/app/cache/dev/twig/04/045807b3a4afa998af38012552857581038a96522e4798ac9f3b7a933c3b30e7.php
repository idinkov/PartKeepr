<?php

/* FOSUserBundle:Resetting:passwordAlreadyRequested.html.twig */
class __TwigTemplate_30e0fcac2a331a3a808c86d05693961e8a788fa41ce339511480e3f2ad7f7481 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Resetting:passwordAlreadyRequested.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_26f8ed3edd856c98b3aa2b71ed6f4964babfa0b159e63e2e07a1ce1cea68372d = $this->env->getExtension("native_profiler");
        $__internal_26f8ed3edd856c98b3aa2b71ed6f4964babfa0b159e63e2e07a1ce1cea68372d->enter($__internal_26f8ed3edd856c98b3aa2b71ed6f4964babfa0b159e63e2e07a1ce1cea68372d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:passwordAlreadyRequested.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_26f8ed3edd856c98b3aa2b71ed6f4964babfa0b159e63e2e07a1ce1cea68372d->leave($__internal_26f8ed3edd856c98b3aa2b71ed6f4964babfa0b159e63e2e07a1ce1cea68372d_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_9ae4c92a042f20eca96364216e87763b826d78555bc27a5d414723f386b08196 = $this->env->getExtension("native_profiler");
        $__internal_9ae4c92a042f20eca96364216e87763b826d78555bc27a5d414723f386b08196->enter($__internal_9ae4c92a042f20eca96364216e87763b826d78555bc27a5d414723f386b08196_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.password_already_requested", array(), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_9ae4c92a042f20eca96364216e87763b826d78555bc27a5d414723f386b08196->leave($__internal_9ae4c92a042f20eca96364216e87763b826d78555bc27a5d414723f386b08196_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:passwordAlreadyRequested.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/* <p>{{ 'resetting.password_already_requested'|trans }}</p>*/
/* {% endblock fos_user_content %}*/
/* */
