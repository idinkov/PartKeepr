<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_1e44a0f014f13f69594f430339639d9cf7ec6d8e785d67ffeaa8926546b4e503 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_12cbd7c6ae81ee6325bcdf14d6278584cc3243c1218ed2aea2998385c6ebf749 = $this->env->getExtension("native_profiler");
        $__internal_12cbd7c6ae81ee6325bcdf14d6278584cc3243c1218ed2aea2998385c6ebf749->enter($__internal_12cbd7c6ae81ee6325bcdf14d6278584cc3243c1218ed2aea2998385c6ebf749_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_12cbd7c6ae81ee6325bcdf14d6278584cc3243c1218ed2aea2998385c6ebf749->leave($__internal_12cbd7c6ae81ee6325bcdf14d6278584cc3243c1218ed2aea2998385c6ebf749_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($widget == 'single_text'): ?>*/
/*     <?php echo $view['form']->block($form, 'form_widget_simple'); ?>*/
/* <?php else: ?>*/
/*     <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*         <?php echo $view['form']->widget($form['date']).' '.$view['form']->widget($form['time']) ?>*/
/*     </div>*/
/* <?php endif ?>*/
/* */
