<?php

/* TwigBundle:Exception:error.txt.twig */
class __TwigTemplate_7bc11696ec98855c8d121c9bd68f098b0653339831954c7292f93f1a36cb6a97 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f03ecf1ec534c75541686f376745529469dfe55a86ee48fa267bcca48c8fd2df = $this->env->getExtension("native_profiler");
        $__internal_f03ecf1ec534c75541686f376745529469dfe55a86ee48fa267bcca48c8fd2df->enter($__internal_f03ecf1ec534c75541686f376745529469dfe55a86ee48fa267bcca48c8fd2df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo (isset($context["status_code"]) ? $context["status_code"] : null);
        echo " ";
        echo (isset($context["status_text"]) ? $context["status_text"] : null);
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_f03ecf1ec534c75541686f376745529469dfe55a86ee48fa267bcca48c8fd2df->leave($__internal_f03ecf1ec534c75541686f376745529469dfe55a86ee48fa267bcca48c8fd2df_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  22 => 1,);
    }
}
/* Oops! An Error Occurred*/
/* =======================*/
/* */
/* The server returned a "{{ status_code }} {{ status_text }}".*/
/* */
/* Something is broken. Please let us know what you were doing when this error occurred.*/
/* We will fix it as soon as possible. Sorry for any inconvenience caused.*/
/* */
