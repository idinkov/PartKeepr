<?php

/* FOSUserBundle:ChangePassword:changePassword.html.twig */
class __TwigTemplate_909d0f0c312a8bcc41822d3c787c473acdf488e375632b8e9ded475cba69bc11 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:ChangePassword:changePassword.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_174adcbd0dedc9e8982b4721a6676f700638da2800b42869e7f4d0e5f9d45207 = $this->env->getExtension("native_profiler");
        $__internal_174adcbd0dedc9e8982b4721a6676f700638da2800b42869e7f4d0e5f9d45207->enter($__internal_174adcbd0dedc9e8982b4721a6676f700638da2800b42869e7f4d0e5f9d45207_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:changePassword.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_174adcbd0dedc9e8982b4721a6676f700638da2800b42869e7f4d0e5f9d45207->leave($__internal_174adcbd0dedc9e8982b4721a6676f700638da2800b42869e7f4d0e5f9d45207_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_36ce50eb4a85fea193a493d8bf6dd7f39c9e6ffc84f8e98460444974257095eb = $this->env->getExtension("native_profiler");
        $__internal_36ce50eb4a85fea193a493d8bf6dd7f39c9e6ffc84f8e98460444974257095eb->enter($__internal_36ce50eb4a85fea193a493d8bf6dd7f39c9e6ffc84f8e98460444974257095eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:ChangePassword:changePassword_content.html.twig", "FOSUserBundle:ChangePassword:changePassword.html.twig", 4)->display($context);
        
        $__internal_36ce50eb4a85fea193a493d8bf6dd7f39c9e6ffc84f8e98460444974257095eb->leave($__internal_36ce50eb4a85fea193a493d8bf6dd7f39c9e6ffc84f8e98460444974257095eb_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:changePassword.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:ChangePassword:changePassword_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
