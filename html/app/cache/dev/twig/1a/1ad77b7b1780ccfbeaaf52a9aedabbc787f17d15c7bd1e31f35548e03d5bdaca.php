<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_59744aba6ede92211efb8b64939fee3a8b60a12ccd60b373afd58eb7a00145a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a1572eccb1c64df11f8c35bacf2ba1e9a390822c72d201189e7c86f67737ba87 = $this->env->getExtension("native_profiler");
        $__internal_a1572eccb1c64df11f8c35bacf2ba1e9a390822c72d201189e7c86f67737ba87->enter($__internal_a1572eccb1c64df11f8c35bacf2ba1e9a390822c72d201189e7c86f67737ba87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : null))));
        
        $__internal_a1572eccb1c64df11f8c35bacf2ba1e9a390822c72d201189e7c86f67737ba87->leave($__internal_a1572eccb1c64df11f8c35bacf2ba1e9a390822c72d201189e7c86f67737ba87_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
