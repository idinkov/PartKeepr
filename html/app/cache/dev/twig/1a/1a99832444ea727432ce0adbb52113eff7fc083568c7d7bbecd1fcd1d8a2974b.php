<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_4171d1cb56bf5bcb1333140d0e93d7346b747beacb8b63497b498c6aad7da605 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_195ec2985d34e6f31b236bdeeba7dcbde3a023f37ced29809c0bfa8344a67856 = $this->env->getExtension("native_profiler");
        $__internal_195ec2985d34e6f31b236bdeeba7dcbde3a023f37ced29809c0bfa8344a67856->enter($__internal_195ec2985d34e6f31b236bdeeba7dcbde3a023f37ced29809c0bfa8344a67856_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_195ec2985d34e6f31b236bdeeba7dcbde3a023f37ced29809c0bfa8344a67856->leave($__internal_195ec2985d34e6f31b236bdeeba7dcbde3a023f37ced29809c0bfa8344a67856_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <table <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <tr>*/
/*         <td colspan="2">*/
/*             <?php echo $view['form']->errors($form) ?>*/
/*         </td>*/
/*     </tr>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </table>*/
/* */
