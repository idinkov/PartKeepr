<?php

/* WebProfilerBundle:Profiler:header.html.twig */
class __TwigTemplate_42f6eb5df4f71c74ff9c98ed0bfac3674ac25e85dd68c1c8dad883895886542f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ff206df36c3911fc782de374c5a18f4643e2aa48ca468fd2f436a9ac6e1a1c27 = $this->env->getExtension("native_profiler");
        $__internal_ff206df36c3911fc782de374c5a18f4643e2aa48ca468fd2f436a9ac6e1a1c27->enter($__internal_ff206df36c3911fc782de374c5a18f4643e2aa48ca468fd2f436a9ac6e1a1c27_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:header.html.twig"));

        // line 1
        echo "<div id=\"header\">
    <div class=\"container\">
        <h1>";
        // line 3
        echo twig_include($this->env, $context, "@WebProfiler/Icon/symfony.svg");
        echo " Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
";
        
        $__internal_ff206df36c3911fc782de374c5a18f4643e2aa48ca468fd2f436a9ac6e1a1c27->leave($__internal_ff206df36c3911fc782de374c5a18f4643e2aa48ca468fd2f436a9ac6e1a1c27_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 3,  22 => 1,);
    }
}
/* <div id="header">*/
/*     <div class="container">*/
/*         <h1>{{ include('@WebProfiler/Icon/symfony.svg') }} Symfony <span>Profiler</span></h1>*/
/* */
/*         <div class="search">*/
/*             <form method="get" action="https://symfony.com/search" target="_blank">*/
/*                 <div class="form-row">*/
/*                     <input name="q" id="search-id" type="search" placeholder="search on symfony.com">*/
/*                     <button type="submit" class="btn">Search</button>*/
/*                 </div>*/
/*            </form>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
