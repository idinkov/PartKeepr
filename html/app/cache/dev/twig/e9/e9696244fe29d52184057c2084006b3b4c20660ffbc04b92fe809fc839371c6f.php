<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_765a1acaded1d764ad63348a70ce7130d691e35378b0c21903e62babf8290bd1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5a41cb81241023655644f1bf640d814688df0fdbb727faffc201e610ac61ec44 = $this->env->getExtension("native_profiler");
        $__internal_5a41cb81241023655644f1bf640d814688df0fdbb727faffc201e610ac61ec44->enter($__internal_5a41cb81241023655644f1bf640d814688df0fdbb727faffc201e610ac61ec44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_5a41cb81241023655644f1bf640d814688df0fdbb727faffc201e610ac61ec44->leave($__internal_5a41cb81241023655644f1bf640d814688df0fdbb727faffc201e610ac61ec44_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($expanded): ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_expanded') ?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_collapsed') ?>*/
/* <?php endif ?>*/
/* */
