<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_0eb665204e72735e882a1ab73f9d48267cd4cebcbfe79646a74a76d50c92eeb7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_371f1e87dbc033114a93c733bcda5dc76e32f3b0220f41afcad84f2a233a3512 = $this->env->getExtension("native_profiler");
        $__internal_371f1e87dbc033114a93c733bcda5dc76e32f3b0220f41afcad84f2a233a3512->enter($__internal_371f1e87dbc033114a93c733bcda5dc76e32f3b0220f41afcad84f2a233a3512_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_371f1e87dbc033114a93c733bcda5dc76e32f3b0220f41afcad84f2a233a3512->leave($__internal_371f1e87dbc033114a93c733bcda5dc76e32f3b0220f41afcad84f2a233a3512_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child) : ?>*/
/*     <?php echo $view['form']->row($child) ?>*/
/* <?php endforeach; ?>*/
/* */
