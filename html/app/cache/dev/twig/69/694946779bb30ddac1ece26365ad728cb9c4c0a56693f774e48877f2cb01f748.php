<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_f2a7060a95373f126810bf0402cdd72d21bd5dc0c0d356bd7e537930a6734d98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f027d2803e262e1c9198f80d91d227f70f249b02e762c0ed2a6e0f9803034a9e = $this->env->getExtension("native_profiler");
        $__internal_f027d2803e262e1c9198f80d91d227f70f249b02e762c0ed2a6e0f9803034a9e->enter($__internal_f027d2803e262e1c9198f80d91d227f70f249b02e762c0ed2a6e0f9803034a9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        $this->displayBlock('body_text', $context, $blocks);
        // line 12
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_f027d2803e262e1c9198f80d91d227f70f249b02e762c0ed2a6e0f9803034a9e->leave($__internal_f027d2803e262e1c9198f80d91d227f70f249b02e762c0ed2a6e0f9803034a9e_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_82e526b3a0eeb730b95ac029ec1cf690c510110ebf9f1322c702264530cd4048 = $this->env->getExtension("native_profiler");
        $__internal_82e526b3a0eeb730b95ac029ec1cf690c510110ebf9f1322c702264530cd4048->enter($__internal_82e526b3a0eeb730b95ac029ec1cf690c510110ebf9f1322c702264530cd4048_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('translator')->trans("registration.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : null)), "FOSUserBundle");
        echo "
";
        
        $__internal_82e526b3a0eeb730b95ac029ec1cf690c510110ebf9f1322c702264530cd4048->leave($__internal_82e526b3a0eeb730b95ac029ec1cf690c510110ebf9f1322c702264530cd4048_prof);

    }

    // line 7
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_9cc5cf83ad6ca5b3a1d20b5a0f4d8fda0b1017296ab1c8ec93b552689d03a6df = $this->env->getExtension("native_profiler");
        $__internal_9cc5cf83ad6ca5b3a1d20b5a0f4d8fda0b1017296ab1c8ec93b552689d03a6df->enter($__internal_9cc5cf83ad6ca5b3a1d20b5a0f4d8fda0b1017296ab1c8ec93b552689d03a6df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 9
        echo $this->env->getExtension('translator')->trans("registration.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : null)), "FOSUserBundle");
        echo "
";
        
        $__internal_9cc5cf83ad6ca5b3a1d20b5a0f4d8fda0b1017296ab1c8ec93b552689d03a6df->leave($__internal_9cc5cf83ad6ca5b3a1d20b5a0f4d8fda0b1017296ab1c8ec93b552689d03a6df_prof);

    }

    // line 12
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_728e96d89f1eed4bfa12dba66188ef5dd53e9c5c893c26f6581b4ee4e64385ff = $this->env->getExtension("native_profiler");
        $__internal_728e96d89f1eed4bfa12dba66188ef5dd53e9c5c893c26f6581b4ee4e64385ff->enter($__internal_728e96d89f1eed4bfa12dba66188ef5dd53e9c5c893c26f6581b4ee4e64385ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_728e96d89f1eed4bfa12dba66188ef5dd53e9c5c893c26f6581b4ee4e64385ff->leave($__internal_728e96d89f1eed4bfa12dba66188ef5dd53e9c5c893c26f6581b4ee4e64385ff_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  66 => 12,  57 => 9,  51 => 7,  42 => 4,  36 => 2,  29 => 12,  27 => 7,  25 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {% block subject %}*/
/* {% autoescape false %}*/
/* {{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_text %}*/
/* {% autoescape false %}*/
/* {{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_html %}{% endblock %}*/
/* */
