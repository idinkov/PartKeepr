<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_4b5b7c49326269304ac1c4bcdccaeb32d6f8ddb5fa45c0428c1986968def9b0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_21e2be5d4dff670d7fef0518e850ba56084d088cd32a2d91e69606cc5386e94e = $this->env->getExtension("native_profiler");
        $__internal_21e2be5d4dff670d7fef0518e850ba56084d088cd32a2d91e69606cc5386e94e->enter($__internal_21e2be5d4dff670d7fef0518e850ba56084d088cd32a2d91e69606cc5386e94e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_21e2be5d4dff670d7fef0518e850ba56084d088cd32a2d91e69606cc5386e94e->leave($__internal_21e2be5d4dff670d7fef0518e850ba56084d088cd32a2d91e69606cc5386e94e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </div>*/
/* */
