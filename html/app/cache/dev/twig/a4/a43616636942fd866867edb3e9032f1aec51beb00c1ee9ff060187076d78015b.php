<?php

/* LiipFunctionalTestBundle:Default:form.html.twig */
class __TwigTemplate_f251e3cffa61ace64f1486c4a27a6ee528346897008bfd1d82abee00b905f185 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("LiipFunctionalTestBundle::layout.html.twig", "LiipFunctionalTestBundle:Default:form.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LiipFunctionalTestBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6d44380a37c80a7056871892309f1336e73a257292a451898ff82cb1e9487a8e = $this->env->getExtension("native_profiler");
        $__internal_6d44380a37c80a7056871892309f1336e73a257292a451898ff82cb1e9487a8e->enter($__internal_6d44380a37c80a7056871892309f1336e73a257292a451898ff82cb1e9487a8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "LiipFunctionalTestBundle:Default:form.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6d44380a37c80a7056871892309f1336e73a257292a451898ff82cb1e9487a8e->leave($__internal_6d44380a37c80a7056871892309f1336e73a257292a451898ff82cb1e9487a8e_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_4dacc2eb9bf01655cf65c7a67302c811703eae702815dd01ea5ab5564076dd8c = $this->env->getExtension("native_profiler");
        $__internal_4dacc2eb9bf01655cf65c7a67302c811703eae702815dd01ea5ab5564076dd8c->enter($__internal_4dacc2eb9bf01655cf65c7a67302c811703eae702815dd01ea5ab5564076dd8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form');
        echo "
";
        
        $__internal_4dacc2eb9bf01655cf65c7a67302c811703eae702815dd01ea5ab5564076dd8c->leave($__internal_4dacc2eb9bf01655cf65c7a67302c811703eae702815dd01ea5ab5564076dd8c_prof);

    }

    public function getTemplateName()
    {
        return "LiipFunctionalTestBundle:Default:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'LiipFunctionalTestBundle::layout.html.twig' %}*/
/* */
/* {% block body %}*/
/*     {{ form(form) }}*/
/* {% endblock %}*/
/* */
