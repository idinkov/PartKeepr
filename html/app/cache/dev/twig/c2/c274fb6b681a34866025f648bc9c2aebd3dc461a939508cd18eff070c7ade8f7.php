<?php

/* FOSUserBundle:Group:edit.html.twig */
class __TwigTemplate_9fbd08fe06baf826a6f33b67134cd1d62b9ed7209a7947b405405be13f39e446 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9b8a2f5cb23e46bc1749826b8132944356be85c8cdcb814fb7ce17a78905a302 = $this->env->getExtension("native_profiler");
        $__internal_9b8a2f5cb23e46bc1749826b8132944356be85c8cdcb814fb7ce17a78905a302->enter($__internal_9b8a2f5cb23e46bc1749826b8132944356be85c8cdcb814fb7ce17a78905a302_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9b8a2f5cb23e46bc1749826b8132944356be85c8cdcb814fb7ce17a78905a302->leave($__internal_9b8a2f5cb23e46bc1749826b8132944356be85c8cdcb814fb7ce17a78905a302_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_eca831d2deb0bdf6f88ed07aea4f8fc51e5acd25eeea0f2c359ed8e86ed3f124 = $this->env->getExtension("native_profiler");
        $__internal_eca831d2deb0bdf6f88ed07aea4f8fc51e5acd25eeea0f2c359ed8e86ed3f124->enter($__internal_eca831d2deb0bdf6f88ed07aea4f8fc51e5acd25eeea0f2c359ed8e86ed3f124_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:edit_content.html.twig", "FOSUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $__internal_eca831d2deb0bdf6f88ed07aea4f8fc51e5acd25eeea0f2c359ed8e86ed3f124->leave($__internal_eca831d2deb0bdf6f88ed07aea4f8fc51e5acd25eeea0f2c359ed8e86ed3f124_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:edit_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
