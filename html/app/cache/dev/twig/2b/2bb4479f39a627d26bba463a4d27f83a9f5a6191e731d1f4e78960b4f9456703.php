<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_583121133a0346c2e4072872f494f2863f5340392a26d0abb16a71cc3d5fce9a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7115939a99d550d13f0468e46eae82bdd14e722c8c970bab075f7c8271fafc3d = $this->env->getExtension("native_profiler");
        $__internal_7115939a99d550d13f0468e46eae82bdd14e722c8c970bab075f7c8271fafc3d->enter($__internal_7115939a99d550d13f0468e46eae82bdd14e722c8c970bab075f7c8271fafc3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_7115939a99d550d13f0468e46eae82bdd14e722c8c970bab075f7c8271fafc3d->leave($__internal_7115939a99d550d13f0468e46eae82bdd14e722c8c970bab075f7c8271fafc3d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (count($errors) > 0): ?>*/
/*     <ul>*/
/*         <?php foreach ($errors as $error): ?>*/
/*             <li><?php echo $error->getMessage() ?></li>*/
/*         <?php endforeach; ?>*/
/*     </ul>*/
/* <?php endif ?>*/
/* */
