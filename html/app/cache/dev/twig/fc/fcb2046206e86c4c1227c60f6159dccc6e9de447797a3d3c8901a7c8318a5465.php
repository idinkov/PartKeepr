<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_210b606e80be7261a29a1000bb89f73a0c1d198b134ca70a5dc88a84e73b8e43 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_650e1420c2c91d708172b21dcd1c3daab154284baef349eca192ced59bcc265b = $this->env->getExtension("native_profiler");
        $__internal_650e1420c2c91d708172b21dcd1c3daab154284baef349eca192ced59bcc265b->enter($__internal_650e1420c2c91d708172b21dcd1c3daab154284baef349eca192ced59bcc265b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_650e1420c2c91d708172b21dcd1c3daab154284baef349eca192ced59bcc265b->leave($__internal_650e1420c2c91d708172b21dcd1c3daab154284baef349eca192ced59bcc265b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($compound): ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_compound')?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_simple')?>*/
/* <?php endif ?>*/
/* */
