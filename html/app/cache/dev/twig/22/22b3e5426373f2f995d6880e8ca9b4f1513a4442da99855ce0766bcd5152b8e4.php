<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_9de1034b67279d7d29f9eec93bf552f2d2f8d394c11c86717a9d1d4f97527b45 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dffed993b0d9b86b6c2057dc0fcda0d87c98206778e8beb5a420f65ba9b8e9e3 = $this->env->getExtension("native_profiler");
        $__internal_dffed993b0d9b86b6c2057dc0fcda0d87c98206778e8beb5a420f65ba9b8e9e3->enter($__internal_dffed993b0d9b86b6c2057dc0fcda0d87c98206778e8beb5a420f65ba9b8e9e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_dffed993b0d9b86b6c2057dc0fcda0d87c98206778e8beb5a420f65ba9b8e9e3->leave($__internal_dffed993b0d9b86b6c2057dc0fcda0d87c98206778e8beb5a420f65ba9b8e9e3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'search')) ?>*/
/* */
