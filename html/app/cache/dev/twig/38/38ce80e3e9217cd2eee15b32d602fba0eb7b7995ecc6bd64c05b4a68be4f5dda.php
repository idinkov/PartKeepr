<?php

/* FOSUserBundle:Group:show_content.html.twig */
class __TwigTemplate_9520540bab0440f6ba84d209ee42e3a89012493baa31073a84a89d3814826d18 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c65308e8dd6eb6f8c7f6768b383ea59950e50b6abbdc923734b346b04c997121 = $this->env->getExtension("native_profiler");
        $__internal_c65308e8dd6eb6f8c7f6768b383ea59950e50b6abbdc923734b346b04c997121->enter($__internal_c65308e8dd6eb6f8c7f6768b383ea59950e50b6abbdc923734b346b04c997121_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_group_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("group.show.name", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["group"]) ? $context["group"] : null), "getName", array(), "method"), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_c65308e8dd6eb6f8c7f6768b383ea59950e50b6abbdc923734b346b04c997121->leave($__internal_c65308e8dd6eb6f8c7f6768b383ea59950e50b6abbdc923734b346b04c997121_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 4,  22 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* <div class="fos_user_group_show">*/
/*     <p>{{ 'group.show.name'|trans }}: {{ group.getName() }}</p>*/
/* </div>*/
/* */
