<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_d2c790cd6b64c54151120b6d6925551e4ac34b30b07b2b2e0037c5fc685c8ca5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fd27656004ef50dc55767eb9dace13e6296f6430534ad0a81dada390bc3951ee = $this->env->getExtension("native_profiler");
        $__internal_fd27656004ef50dc55767eb9dace13e6296f6430534ad0a81dada390bc3951ee->enter($__internal_fd27656004ef50dc55767eb9dace13e6296f6430534ad0a81dada390bc3951ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_fd27656004ef50dc55767eb9dace13e6296f6430534ad0a81dada390bc3951ee->leave($__internal_fd27656004ef50dc55767eb9dace13e6296f6430534ad0a81dada390bc3951ee_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!isset($render_rest) || $render_rest): ?>*/
/* <?php echo $view['form']->rest($form) ?>*/
/* <?php endif ?>*/
/* </form>*/
/* */
