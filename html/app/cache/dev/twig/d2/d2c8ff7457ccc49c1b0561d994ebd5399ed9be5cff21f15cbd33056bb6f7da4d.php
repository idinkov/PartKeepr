<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_629b3fca3c29e61d7f20fe250a0a6160b7b3d4bfcbf3acf73d4ad45d39a9271b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_832e6a819532a7aa270704bb011cd43ceb1cdda7a9f84254caedcf285fb8f9da = $this->env->getExtension("native_profiler");
        $__internal_832e6a819532a7aa270704bb011cd43ceb1cdda7a9f84254caedcf285fb8f9da->enter($__internal_832e6a819532a7aa270704bb011cd43ceb1cdda7a9f84254caedcf285fb8f9da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_832e6a819532a7aa270704bb011cd43ceb1cdda7a9f84254caedcf285fb8f9da->leave($__internal_832e6a819532a7aa270704bb011cd43ceb1cdda7a9f84254caedcf285fb8f9da_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'submit')) ?>*/
/* */
