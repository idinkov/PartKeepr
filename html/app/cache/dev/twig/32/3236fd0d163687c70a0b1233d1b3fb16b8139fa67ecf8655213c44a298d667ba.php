<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_3f62d2913e2b86cf403062621ab5b5046d6475ed82953a8a3f62b6d0e4a805ba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1673cb8f049570245d7d41c08e0e086436ddb446e4801661cbd3cd138c71ba4a = $this->env->getExtension("native_profiler");
        $__internal_1673cb8f049570245d7d41c08e0e086436ddb446e4801661cbd3cd138c71ba4a->enter($__internal_1673cb8f049570245d7d41c08e0e086436ddb446e4801661cbd3cd138c71ba4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_1673cb8f049570245d7d41c08e0e086436ddb446e4801661cbd3cd138c71ba4a->leave($__internal_1673cb8f049570245d7d41c08e0e086436ddb446e4801661cbd3cd138c71ba4a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr style="display: none">*/
/*     <td colspan="2">*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
