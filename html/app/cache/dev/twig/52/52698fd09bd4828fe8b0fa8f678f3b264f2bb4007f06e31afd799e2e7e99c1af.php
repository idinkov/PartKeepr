<?php

/* LiipFunctionalTestBundle:Default:user.html.twig */
class __TwigTemplate_aa8fc4ad4be13692a5d99eae9a1b6bdb5e8cd18c010284ed411b292ef06615e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("LiipFunctionalTestBundle::layout.html.twig", "LiipFunctionalTestBundle:Default:user.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "LiipFunctionalTestBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bf44444bfa25072696b2c446fd10d540f7c2d4b98e302a59c2d6155ed99b679a = $this->env->getExtension("native_profiler");
        $__internal_bf44444bfa25072696b2c446fd10d540f7c2d4b98e302a59c2d6155ed99b679a->enter($__internal_bf44444bfa25072696b2c446fd10d540f7c2d4b98e302a59c2d6155ed99b679a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "LiipFunctionalTestBundle:Default:user.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bf44444bfa25072696b2c446fd10d540f7c2d4b98e302a59c2d6155ed99b679a->leave($__internal_bf44444bfa25072696b2c446fd10d540f7c2d4b98e302a59c2d6155ed99b679a_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_e5d29511ee9aa7594a5d757183a4872b45d2df54963a4b2f8d6bc6c6c51ba100 = $this->env->getExtension("native_profiler");
        $__internal_e5d29511ee9aa7594a5d757183a4872b45d2df54963a4b2f8d6bc6c6c51ba100->enter($__internal_e5d29511ee9aa7594a5d757183a4872b45d2df54963a4b2f8d6bc6c6c51ba100_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <p>Name: ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "name", array()), "html", null, true);
        echo "</p>
    <p>Email: ";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "email", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_e5d29511ee9aa7594a5d757183a4872b45d2df54963a4b2f8d6bc6c6c51ba100->leave($__internal_e5d29511ee9aa7594a5d757183a4872b45d2df54963a4b2f8d6bc6c6c51ba100_prof);

    }

    public function getTemplateName()
    {
        return "LiipFunctionalTestBundle:Default:user.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 5,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'LiipFunctionalTestBundle::layout.html.twig' %}*/
/* */
/* {% block body %}*/
/*     <p>Name: {{ user.name }}</p>*/
/*     <p>Email: {{ user.email }}</p>*/
/* {% endblock %}*/
/* */
