<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_668e78305e090e6958b695e288514a6fd671ce5d1846beb7d05c7fec56d28586 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9668c9e5767aa86d4c60b67b895e8bb01edddc7cb7fc7bec0baa276d867574df = $this->env->getExtension("native_profiler");
        $__internal_9668c9e5767aa86d4c60b67b895e8bb01edddc7cb7fc7bec0baa276d867574df->enter($__internal_9668c9e5767aa86d4c60b67b895e8bb01edddc7cb7fc7bec0baa276d867574df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9668c9e5767aa86d4c60b67b895e8bb01edddc7cb7fc7bec0baa276d867574df->leave($__internal_9668c9e5767aa86d4c60b67b895e8bb01edddc7cb7fc7bec0baa276d867574df_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_c138ac7937afb15ab9c82471d2824bd32498a06e2f7dd4fb2bc2d9d763afea54 = $this->env->getExtension("native_profiler");
        $__internal_c138ac7937afb15ab9c82471d2824bd32498a06e2f7dd4fb2bc2d9d763afea54->enter($__internal_c138ac7937afb15ab9c82471d2824bd32498a06e2f7dd4fb2bc2d9d763afea54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Profile:edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_c138ac7937afb15ab9c82471d2824bd32498a06e2f7dd4fb2bc2d9d763afea54->leave($__internal_c138ac7937afb15ab9c82471d2824bd32498a06e2f7dd4fb2bc2d9d763afea54_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Profile:edit_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
