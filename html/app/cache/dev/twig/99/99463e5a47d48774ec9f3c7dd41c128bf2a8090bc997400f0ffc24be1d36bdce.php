<?php

/* LiipFunctionalTestBundle::layout.html.twig */
class __TwigTemplate_d33cdb6034992f4e4e408814922f2c45436bdd7427ab0006ec094340d5ddc98d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8756d7e796c02136ed2a9e69b87efbbce37d44cb5377968c071abec4650170e9 = $this->env->getExtension("native_profiler");
        $__internal_8756d7e796c02136ed2a9e69b87efbbce37d44cb5377968c071abec4650170e9->enter($__internal_8756d7e796c02136ed2a9e69b87efbbce37d44cb5377968c071abec4650170e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "LiipFunctionalTestBundle::layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
        
        <title>LiipFunctionalTestBundle</title>
    </head>
    
    <body>
        <p id=\"user\">";
        // line 12
        if ((null === $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()))) {
            // line 13
            echo "Not logged in.";
        } else {
            // line 15
            echo "Logged in as ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
            echo ".";
        }
        // line 17
        echo "</p>

        <h1>LiipFunctionalTestBundle</h1>
        
        ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 22
            echo "            <div class=\"flash-notice\">
                ";
            // line 23
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "        
        <div id=\"content\">";
        // line 27
        $this->displayBlock('body', $context, $blocks);
        echo "</div>
    </body>
</html>
";
        
        $__internal_8756d7e796c02136ed2a9e69b87efbbce37d44cb5377968c071abec4650170e9->leave($__internal_8756d7e796c02136ed2a9e69b87efbbce37d44cb5377968c071abec4650170e9_prof);

    }

    public function block_body($context, array $blocks = array())
    {
        $__internal_bd54094506d6623639944c64950e3eaf58805e95824eb8863e2c123a107960ae = $this->env->getExtension("native_profiler");
        $__internal_bd54094506d6623639944c64950e3eaf58805e95824eb8863e2c123a107960ae->enter($__internal_bd54094506d6623639944c64950e3eaf58805e95824eb8863e2c123a107960ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_bd54094506d6623639944c64950e3eaf58805e95824eb8863e2c123a107960ae->leave($__internal_bd54094506d6623639944c64950e3eaf58805e95824eb8863e2c123a107960ae_prof);

    }

    public function getTemplateName()
    {
        return "LiipFunctionalTestBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 27,  67 => 26,  58 => 23,  55 => 22,  51 => 21,  45 => 17,  40 => 15,  37 => 13,  35 => 12,  23 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/*     <head>*/
/*         <meta charset="utf-8">*/
/*         <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*         */
/*         <title>LiipFunctionalTestBundle</title>*/
/*     </head>*/
/*     */
/*     <body>*/
/*         <p id="user">*/
/*             {%- if (app.user is null) -%}*/
/*                 Not logged in.*/
/*             {%- else -%}*/
/*                 Logged in as {{ app.user.username }}.*/
/*             {%- endif -%}*/
/*         </p>*/
/* */
/*         <h1>LiipFunctionalTestBundle</h1>*/
/*         */
/*         {% for flash_message in app.session.flashbag.get('notice') %}*/
/*             <div class="flash-notice">*/
/*                 {{ flash_message }}*/
/*             </div>*/
/*         {% endfor %}*/
/*         */
/*         <div id="content">{% block body %}{% endblock %}</div>*/
/*     </body>*/
/* </html>*/
/* */
