<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_8702fdd36d1add2db2a5a44e5e875373816cbb690247b5cd605e0f61125d4b28 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_abb76b106ebe3d910fd886d369598bd4d07469f727d181ac4751d40f34e479a1 = $this->env->getExtension("native_profiler");
        $__internal_abb76b106ebe3d910fd886d369598bd4d07469f727d181ac4751d40f34e479a1->enter($__internal_abb76b106ebe3d910fd886d369598bd4d07469f727d181ac4751d40f34e479a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_abb76b106ebe3d910fd886d369598bd4d07469f727d181ac4751d40f34e479a1->leave($__internal_abb76b106ebe3d910fd886d369598bd4d07469f727d181ac4751d40f34e479a1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'url')) ?>*/
/* */
