<?php

/* PartKeeprAuthBundle:Default:index.html.twig */
class __TwigTemplate_e0193b42ff9c24a812d7aa00a15a9c2621a087f89da78ab892d3f163833fca74 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5ecfcc0f3a447f33bb7d4b3e3eeec1ef2d582aab2f238b1c6c5141d575831465 = $this->env->getExtension("native_profiler");
        $__internal_5ecfcc0f3a447f33bb7d4b3e3eeec1ef2d582aab2f238b1c6c5141d575831465->enter($__internal_5ecfcc0f3a447f33bb7d4b3e3eeec1ef2d582aab2f238b1c6c5141d575831465_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PartKeeprAuthBundle:Default:index.html.twig"));

        // line 1
        echo "Hello ";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
        echo "!
";
        
        $__internal_5ecfcc0f3a447f33bb7d4b3e3eeec1ef2d582aab2f238b1c6c5141d575831465->leave($__internal_5ecfcc0f3a447f33bb7d4b3e3eeec1ef2d582aab2f238b1c6c5141d575831465_prof);

    }

    public function getTemplateName()
    {
        return "PartKeeprAuthBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* Hello {{ name }}!*/
/* */
