<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_61f3372fe37e371cd9d40ba4f6cdbaee9d4fb71c3c6c103bedf4354dec8ce55e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ec1999fae0b70af4cc2cce20b8bd69a7cd224fb3e6fff7a7a9075d13d94662df = $this->env->getExtension("native_profiler");
        $__internal_ec1999fae0b70af4cc2cce20b8bd69a7cd224fb3e6fff7a7a9075d13d94662df->enter($__internal_ec1999fae0b70af4cc2cce20b8bd69a7cd224fb3e6fff7a7a9075d13d94662df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_ec1999fae0b70af4cc2cce20b8bd69a7cd224fb3e6fff7a7a9075d13d94662df->leave($__internal_ec1999fae0b70af4cc2cce20b8bd69a7cd224fb3e6fff7a7a9075d13d94662df_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->label($form) ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
