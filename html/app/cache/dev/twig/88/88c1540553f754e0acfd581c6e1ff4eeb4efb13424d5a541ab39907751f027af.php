<?php

/* FOSUserBundle:Group:new_content.html.twig */
class __TwigTemplate_a88af60becd315b6824512fdcf6a4992b84035cbaa0b83c1d022beb86d7ff781 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_afab8ecb3d0af8ebc65db8ab02f68cccbcf6851fb1a3deae08dad671118bfa49 = $this->env->getExtension("native_profiler");
        $__internal_afab8ecb3d0af8ebc65db8ab02f68cccbcf6851fb1a3deae08dad671118bfa49->enter($__internal_afab8ecb3d0af8ebc65db8ab02f68cccbcf6851fb1a3deae08dad671118bfa49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new_content.html.twig"));

        // line 2
        echo "
<form action=\"";
        // line 3
        echo $this->env->getExtension('routing')->getPath("fos_user_group_new");
        echo "\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'enctype');
        echo " method=\"POST\" class=\"fos_user_group_new\">
    ";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'widget');
        echo "
    <div>
        <input type=\"submit\" value=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("group.new.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
</form>
";
        
        $__internal_afab8ecb3d0af8ebc65db8ab02f68cccbcf6851fb1a3deae08dad671118bfa49->leave($__internal_afab8ecb3d0af8ebc65db8ab02f68cccbcf6851fb1a3deae08dad671118bfa49_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 6,  31 => 4,  25 => 3,  22 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* <form action="{{ path('fos_user_group_new') }}" {{ form_enctype(form) }} method="POST" class="fos_user_group_new">*/
/*     {{ form_widget(form) }}*/
/*     <div>*/
/*         <input type="submit" value="{{ 'group.new.submit'|trans }}" />*/
/*     </div>*/
/* </form>*/
/* */
