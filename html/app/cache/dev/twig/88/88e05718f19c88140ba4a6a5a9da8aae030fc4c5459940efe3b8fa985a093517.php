<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_6361a5d69035d31683205f92894b3f9b7bd9a8e3e2e312580486d17ee4a503fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e97d95b8a9486390a4bd4d5a8a892273dac7e65b028cba7c473761b5f4b6a4bd = $this->env->getExtension("native_profiler");
        $__internal_e97d95b8a9486390a4bd4d5a8a892273dac7e65b028cba7c473761b5f4b6a4bd->enter($__internal_e97d95b8a9486390a4bd4d5a8a892273dac7e65b028cba7c473761b5f4b6a4bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e97d95b8a9486390a4bd4d5a8a892273dac7e65b028cba7c473761b5f4b6a4bd->leave($__internal_e97d95b8a9486390a4bd4d5a8a892273dac7e65b028cba7c473761b5f4b6a4bd_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_e5b3290caf06ead973b9eba739d47d40c816410f7652b5600630d3fe077474db = $this->env->getExtension("native_profiler");
        $__internal_e5b3290caf06ead973b9eba739d47d40c816410f7652b5600630d3fe077474db->enter($__internal_e5b3290caf06ead973b9eba739d47d40c816410f7652b5600630d3fe077474db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_e5b3290caf06ead973b9eba739d47d40c816410f7652b5600630d3fe077474db->leave($__internal_e5b3290caf06ead973b9eba739d47d40c816410f7652b5600630d3fe077474db_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_24ab90cc18ea7b074444e728a0ca585cb759a33ffdcf471b6f1b15c279c86a76 = $this->env->getExtension("native_profiler");
        $__internal_24ab90cc18ea7b074444e728a0ca585cb759a33ffdcf471b6f1b15c279c86a76->enter($__internal_24ab90cc18ea7b074444e728a0ca585cb759a33ffdcf471b6f1b15c279c86a76_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : null), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : null), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : null), "html", null, true);
        echo ")
";
        
        $__internal_24ab90cc18ea7b074444e728a0ca585cb759a33ffdcf471b6f1b15c279c86a76->leave($__internal_24ab90cc18ea7b074444e728a0ca585cb759a33ffdcf471b6f1b15c279c86a76_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_227aaac058c537052d009810725307fcdd03da03dd47183c4cc74a1f37b4d189 = $this->env->getExtension("native_profiler");
        $__internal_227aaac058c537052d009810725307fcdd03da03dd47183c4cc74a1f37b4d189->enter($__internal_227aaac058c537052d009810725307fcdd03da03dd47183c4cc74a1f37b4d189_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_227aaac058c537052d009810725307fcdd03da03dd47183c4cc74a1f37b4d189->leave($__internal_227aaac058c537052d009810725307fcdd03da03dd47183c4cc74a1f37b4d189_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
