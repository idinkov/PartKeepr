<?php

/* ::base.html.twig */
class __TwigTemplate_4804bc6075c413c867171f46cd715a74f2b2f4abcbf0a420ce8a8e60bf1beba7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ad70cd93de26d297ea2243b9ca9a81df1c581167ec17a98886b9b21a75c24e48 = $this->env->getExtension("native_profiler");
        $__internal_ad70cd93de26d297ea2243b9ca9a81df1c581167ec17a98886b9b21a75c24e48->enter($__internal_ad70cd93de26d297ea2243b9ca9a81df1c581167ec17a98886b9b21a75c24e48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_ad70cd93de26d297ea2243b9ca9a81df1c581167ec17a98886b9b21a75c24e48->leave($__internal_ad70cd93de26d297ea2243b9ca9a81df1c581167ec17a98886b9b21a75c24e48_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_69587972df73a12191e5fff7628d93dd10d9394a9759568803924de64e436186 = $this->env->getExtension("native_profiler");
        $__internal_69587972df73a12191e5fff7628d93dd10d9394a9759568803924de64e436186->enter($__internal_69587972df73a12191e5fff7628d93dd10d9394a9759568803924de64e436186_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_69587972df73a12191e5fff7628d93dd10d9394a9759568803924de64e436186->leave($__internal_69587972df73a12191e5fff7628d93dd10d9394a9759568803924de64e436186_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_740351ba26523cc7171ddce88276a3dc9bc3cd8a625b5fdb4d2b0651ad94d7d5 = $this->env->getExtension("native_profiler");
        $__internal_740351ba26523cc7171ddce88276a3dc9bc3cd8a625b5fdb4d2b0651ad94d7d5->enter($__internal_740351ba26523cc7171ddce88276a3dc9bc3cd8a625b5fdb4d2b0651ad94d7d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_740351ba26523cc7171ddce88276a3dc9bc3cd8a625b5fdb4d2b0651ad94d7d5->leave($__internal_740351ba26523cc7171ddce88276a3dc9bc3cd8a625b5fdb4d2b0651ad94d7d5_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_627335c0dae1ac272bec0f56fca40bf24d18d2495dab7637a72a37096ca18b8c = $this->env->getExtension("native_profiler");
        $__internal_627335c0dae1ac272bec0f56fca40bf24d18d2495dab7637a72a37096ca18b8c->enter($__internal_627335c0dae1ac272bec0f56fca40bf24d18d2495dab7637a72a37096ca18b8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_627335c0dae1ac272bec0f56fca40bf24d18d2495dab7637a72a37096ca18b8c->leave($__internal_627335c0dae1ac272bec0f56fca40bf24d18d2495dab7637a72a37096ca18b8c_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_01e941549863305a63042f4527966d0866a1772d529bf02de9a8bb2f32237ca3 = $this->env->getExtension("native_profiler");
        $__internal_01e941549863305a63042f4527966d0866a1772d529bf02de9a8bb2f32237ca3->enter($__internal_01e941549863305a63042f4527966d0866a1772d529bf02de9a8bb2f32237ca3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_01e941549863305a63042f4527966d0866a1772d529bf02de9a8bb2f32237ca3->leave($__internal_01e941549863305a63042f4527966d0866a1772d529bf02de9a8bb2f32237ca3_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
