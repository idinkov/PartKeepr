<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_3ff149c1759d1da966af23e375ee9162660a294bbca4ab034dec0b4638cd91fd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2811ffe39f196b60704921d0c13656eaea5a9395a652284825415afb1658a581 = $this->env->getExtension("native_profiler");
        $__internal_2811ffe39f196b60704921d0c13656eaea5a9395a652284825415afb1658a581->enter($__internal_2811ffe39f196b60704921d0c13656eaea5a9395a652284825415afb1658a581_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_2811ffe39f196b60704921d0c13656eaea5a9395a652284825415afb1658a581->leave($__internal_2811ffe39f196b60704921d0c13656eaea5a9395a652284825415afb1658a581_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'reset')) ?>*/
/* */
