<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_eaf0d6ba01223cae89ed0f827ff6763abd8fa8619fcc9ecbf6914242a860ad40 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d69ae1ce8a3a257fce95c9764cd008f45fcdf8ef0334511d98afeea582d1ee79 = $this->env->getExtension("native_profiler");
        $__internal_d69ae1ce8a3a257fce95c9764cd008f45fcdf8ef0334511d98afeea582d1ee79->enter($__internal_d69ae1ce8a3a257fce95c9764cd008f45fcdf8ef0334511d98afeea582d1ee79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_d69ae1ce8a3a257fce95c9764cd008f45fcdf8ef0334511d98afeea582d1ee79->leave($__internal_d69ae1ce8a3a257fce95c9764cd008f45fcdf8ef0334511d98afeea582d1ee79_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td></td>*/
/*     <td>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
