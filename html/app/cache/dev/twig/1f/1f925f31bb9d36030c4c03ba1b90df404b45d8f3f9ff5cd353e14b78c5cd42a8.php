<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_2fada2e1370371719018a0bc0a27d8f4fb825189b6d4524b066053ec2bea2ef9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ec31ddafc9130270a5167322a3c0727905313fcb383da507fec72b8211120860 = $this->env->getExtension("native_profiler");
        $__internal_ec31ddafc9130270a5167322a3c0727905313fcb383da507fec72b8211120860->enter($__internal_ec31ddafc9130270a5167322a3c0727905313fcb383da507fec72b8211120860_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_ec31ddafc9130270a5167322a3c0727905313fcb383da507fec72b8211120860->leave($__internal_ec31ddafc9130270a5167322a3c0727905313fcb383da507fec72b8211120860_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'number')) ?>*/
/* */
