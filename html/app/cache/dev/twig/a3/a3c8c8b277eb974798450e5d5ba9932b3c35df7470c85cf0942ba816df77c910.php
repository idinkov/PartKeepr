<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_da1aea83519c16e6e31b0482cadbfb1d9e664f696f81269df75e67a1bc275284 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fdcb409a7f013f1ba3550e5a4902152d049274f7a285e1c4e493983470106e3f = $this->env->getExtension("native_profiler");
        $__internal_fdcb409a7f013f1ba3550e5a4902152d049274f7a285e1c4e493983470106e3f->enter($__internal_fdcb409a7f013f1ba3550e5a4902152d049274f7a285e1c4e493983470106e3f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        $this->displayBlock('body_text', $context, $blocks);
        // line 12
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_fdcb409a7f013f1ba3550e5a4902152d049274f7a285e1c4e493983470106e3f->leave($__internal_fdcb409a7f013f1ba3550e5a4902152d049274f7a285e1c4e493983470106e3f_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_cf200c55d084b2dc749df771a637f2e2e78c71265a8ff4a1b26ee472ceb0ff5d = $this->env->getExtension("native_profiler");
        $__internal_cf200c55d084b2dc749df771a637f2e2e78c71265a8ff4a1b26ee472ceb0ff5d->enter($__internal_cf200c55d084b2dc749df771a637f2e2e78c71265a8ff4a1b26ee472ceb0ff5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('translator')->trans("resetting.email.subject", array(), "FOSUserBundle");
        echo "
";
        
        $__internal_cf200c55d084b2dc749df771a637f2e2e78c71265a8ff4a1b26ee472ceb0ff5d->leave($__internal_cf200c55d084b2dc749df771a637f2e2e78c71265a8ff4a1b26ee472ceb0ff5d_prof);

    }

    // line 7
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_9cce586d9fddb20fa78df2058899e83da6f8a5717cbc33770d2e6e71570ee7ed = $this->env->getExtension("native_profiler");
        $__internal_9cce586d9fddb20fa78df2058899e83da6f8a5717cbc33770d2e6e71570ee7ed->enter($__internal_9cce586d9fddb20fa78df2058899e83da6f8a5717cbc33770d2e6e71570ee7ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 9
        echo $this->env->getExtension('translator')->trans("resetting.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : null)), "FOSUserBundle");
        echo "
";
        
        $__internal_9cce586d9fddb20fa78df2058899e83da6f8a5717cbc33770d2e6e71570ee7ed->leave($__internal_9cce586d9fddb20fa78df2058899e83da6f8a5717cbc33770d2e6e71570ee7ed_prof);

    }

    // line 12
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_40e97cc76c0d4ba20315ff4369ac030fdef0e20c967c4ad6537cc10e11a79df3 = $this->env->getExtension("native_profiler");
        $__internal_40e97cc76c0d4ba20315ff4369ac030fdef0e20c967c4ad6537cc10e11a79df3->enter($__internal_40e97cc76c0d4ba20315ff4369ac030fdef0e20c967c4ad6537cc10e11a79df3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_40e97cc76c0d4ba20315ff4369ac030fdef0e20c967c4ad6537cc10e11a79df3->leave($__internal_40e97cc76c0d4ba20315ff4369ac030fdef0e20c967c4ad6537cc10e11a79df3_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  66 => 12,  57 => 9,  51 => 7,  42 => 4,  36 => 2,  29 => 12,  27 => 7,  25 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {% block subject %}*/
/* {% autoescape false %}*/
/* {{ 'resetting.email.subject'|trans }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_text %}*/
/* {% autoescape false %}*/
/* {{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_html %}{% endblock %}*/
/* */
