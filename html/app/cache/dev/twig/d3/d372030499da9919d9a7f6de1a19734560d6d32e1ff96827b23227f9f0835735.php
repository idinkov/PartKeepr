<?php

/* FOSUserBundle:Group:show.html.twig */
class __TwigTemplate_1e6baff1c758dbb0cb0e7f683ea33775354683b8f600e9c3574d6f63d3d4b906 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Group:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3141162dede5991a923db46d100f665adbaa5eab0ebefe2c7480aff404cbff08 = $this->env->getExtension("native_profiler");
        $__internal_3141162dede5991a923db46d100f665adbaa5eab0ebefe2c7480aff404cbff08->enter($__internal_3141162dede5991a923db46d100f665adbaa5eab0ebefe2c7480aff404cbff08_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3141162dede5991a923db46d100f665adbaa5eab0ebefe2c7480aff404cbff08->leave($__internal_3141162dede5991a923db46d100f665adbaa5eab0ebefe2c7480aff404cbff08_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_59b0de38e36a92439cf9d8c9c2ca6c67f9e9ebba2898c5a651973f031e6a7ebc = $this->env->getExtension("native_profiler");
        $__internal_59b0de38e36a92439cf9d8c9c2ca6c67f9e9ebba2898c5a651973f031e6a7ebc->enter($__internal_59b0de38e36a92439cf9d8c9c2ca6c67f9e9ebba2898c5a651973f031e6a7ebc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:show_content.html.twig", "FOSUserBundle:Group:show.html.twig", 4)->display($context);
        
        $__internal_59b0de38e36a92439cf9d8c9c2ca6c67f9e9ebba2898c5a651973f031e6a7ebc->leave($__internal_59b0de38e36a92439cf9d8c9c2ca6c67f9e9ebba2898c5a651973f031e6a7ebc_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:show_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
