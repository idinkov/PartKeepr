<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_81b8cdd79fd4ce08bf5e11383a3fb67330ae13781d73420e00348daf2036d1fb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9aa45a3b8624cecf6c2d109038ab34a1da24d9ba1a27c16d497a0db5c3da61ac = $this->env->getExtension("native_profiler");
        $__internal_9aa45a3b8624cecf6c2d109038ab34a1da24d9ba1a27c16d497a0db5c3da61ac->enter($__internal_9aa45a3b8624cecf6c2d109038ab34a1da24d9ba1a27c16d497a0db5c3da61ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9aa45a3b8624cecf6c2d109038ab34a1da24d9ba1a27c16d497a0db5c3da61ac->leave($__internal_9aa45a3b8624cecf6c2d109038ab34a1da24d9ba1a27c16d497a0db5c3da61ac_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_9c92e9824e4a188bdc1239f5262d37efb39b5cf8f9bba5a6f17fbb496eebabb7 = $this->env->getExtension("native_profiler");
        $__internal_9c92e9824e4a188bdc1239f5262d37efb39b5cf8f9bba5a6f17fbb496eebabb7->enter($__internal_9c92e9824e4a188bdc1239f5262d37efb39b5cf8f9bba5a6f17fbb496eebabb7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_9c92e9824e4a188bdc1239f5262d37efb39b5cf8f9bba5a6f17fbb496eebabb7->leave($__internal_9c92e9824e4a188bdc1239f5262d37efb39b5cf8f9bba5a6f17fbb496eebabb7_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_22e11c85156d67b4ebcd3922ae5679a89ed2ef25d0da8d43ac411e00f9a6eb20 = $this->env->getExtension("native_profiler");
        $__internal_22e11c85156d67b4ebcd3922ae5679a89ed2ef25d0da8d43ac411e00f9a6eb20->enter($__internal_22e11c85156d67b4ebcd3922ae5679a89ed2ef25d0da8d43ac411e00f9a6eb20_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : null), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_22e11c85156d67b4ebcd3922ae5679a89ed2ef25d0da8d43ac411e00f9a6eb20->leave($__internal_22e11c85156d67b4ebcd3922ae5679a89ed2ef25d0da8d43ac411e00f9a6eb20_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
