<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_2fad64d26ed9daf3c70136b08e5b0818a2e98c2e026e4b6dc6126d7ed35db4a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_427155e5e68d107ec5a3ef98c39b87f289e16c53170a73e5d916e605e1bcc953 = $this->env->getExtension("native_profiler");
        $__internal_427155e5e68d107ec5a3ef98c39b87f289e16c53170a73e5d916e605e1bcc953->enter($__internal_427155e5e68d107ec5a3ef98c39b87f289e16c53170a73e5d916e605e1bcc953_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_427155e5e68d107ec5a3ef98c39b87f289e16c53170a73e5d916e605e1bcc953->leave($__internal_427155e5e68d107ec5a3ef98c39b87f289e16c53170a73e5d916e605e1bcc953_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_dbd82070936e2a5e04e3ca2315a9645dd4ec0206ae59836cb43c4002f36ca612 = $this->env->getExtension("native_profiler");
        $__internal_dbd82070936e2a5e04e3ca2315a9645dd4ec0206ae59836cb43c4002f36ca612->enter($__internal_dbd82070936e2a5e04e3ca2315a9645dd4ec0206ae59836cb43c4002f36ca612_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_dbd82070936e2a5e04e3ca2315a9645dd4ec0206ae59836cb43c4002f36ca612->leave($__internal_dbd82070936e2a5e04e3ca2315a9645dd4ec0206ae59836cb43c4002f36ca612_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
