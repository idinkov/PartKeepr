<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_4deb726c4b043335e40ba410b4c36a1051da101f99924f2f64ee84edeab6ea7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7048d804758382ea6e49281033c6ffe0667ed7cb1c8265f29210e913b545f789 = $this->env->getExtension("native_profiler");
        $__internal_7048d804758382ea6e49281033c6ffe0667ed7cb1c8265f29210e913b545f789->enter($__internal_7048d804758382ea6e49281033c6ffe0667ed7cb1c8265f29210e913b545f789_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : null), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : null), "js", null, true);
        echo "

*/
";
        
        $__internal_7048d804758382ea6e49281033c6ffe0667ed7cb1c8265f29210e913b545f789->leave($__internal_7048d804758382ea6e49281033c6ffe0667ed7cb1c8265f29210e913b545f789_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
